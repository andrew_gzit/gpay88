<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('', 'AccountController@home');

Route::prefix('')->group(function () {
    Route::get('login', 'AccountController@login')->name('login');
    Route::post('auth', 'AccountController@auth');
    Route::post('change-language', 'AccountController@changeLanguage');

    Route::middleware('auth')->group(function () {
        Route::get('logout', 'AccountController@logout');

        Route::get('home', 'MerchantController@home');
        Route::post('validateDetails', 'AccountController@validateDetails');
        Route::post('summaryReport', 'AccountController@summaryReport');

        Route::post('changePasword', 'AccountController@changePassword');
        Route::post('resetPasword', 'AccountController@resetPassword');

        Route::prefix('merchants')->group(function () {
            Route::get('', 'MerchantController@merchants');
            Route::post('merchantsDT', 'MerchantController@merchantsDT');

            Route::post('suspend', 'MerchantController@suspendMerchant');

            Route::get('view/{merchantId}', 'MerchantController@viewMerchant');

            Route::group(['middleware' => ['permission:merchant_create']], function () {
                Route::get('create', 'MerchantController@createMerchant');
                Route::post('create', 'MerchantController@storeMerchant');
            });

            Route::group(['middleware' => ['permission:merchant_edit']], function () {
                Route::get('edit/{merchantUserId}', 'MerchantController@editMerchant');
                Route::post('update/{merchantId}', 'MerchantController@updateMerchant');
                Route::post('changePassword', 'MerchantController@changePassword');
                Route::post('resetPassword', 'MerchantController@resetPassword');
            });

            Route::get('validateDetails', 'MerchantController@validateDetails');

            Route::post('retrieveShops', 'MerchantController@retrieveShops');

            Route::group(['middleware' => ['permission:merchant_delete']], function () {
                Route::post('destory', 'MerchantController@destroy');
            });

            Route::prefix('submerchants')->group(function () {

                Route::group(['middleware' => ['permission:submerchant_create']], function () {
                    Route::get('create/{merchantId?}', 'SubmerchantController@create');
                    Route::post('create', 'SubmerchantController@store');
                });

                Route::group(['middleware' => ['permission:submerchant_edit']], function () {
                    Route::get('edit/{subMerchantId}', 'SubmerchantController@edit');
                    Route::post('edit/{subMerchantId}', 'SubmerchantController@update');
                });

                Route::post('destroy', 'SubmerchantController@destroy');
                Route::post('updateStatus', 'SubmerchantController@updateStatus');
            });
        });

        Route::prefix('shops')->group(function () {
            Route::get('', 'ShopController@shops');
            Route::post('shopsDT', 'ShopController@shopsDT');

            Route::post('disableShop', 'ShopController@disableShop');

            Route::group(['middleware' => ['permission:shop_create']], function () {
                Route::get('create/{merchantId?}', 'ShopController@createShop');
                Route::post('create', 'ShopController@storeShop');
            });

            Route::group(['middleware' => ['permission:shop_edit']], function () {
                Route::get('{shopId}/edit', 'ShopController@editShop');
                Route::post('changePassword', 'ShopController@changePassword');
                Route::post('resetPassword', 'ShopController@resetPassword');
                Route::post('{shopId}/edit', 'ShopController@updateShop');
            });

            Route::group(['middleware' => ['permission:bank_disable']], function () {
                Route::post('updateBankStatus', 'ShopController@updateBankStatus');
            });

            Route::get('retrieveBank/{bankId}', 'ShopController@retrieveBank');
            Route::post('{shopId}/addBank', 'ShopController@addBank');
            Route::post('updateBank/{bankId}', 'ShopController@updateBank');
            Route::post('deleteBank', 'ShopController@deleteBank');

            Route::post('retrieveShopBankInfo', 'ShopController@retrieveShopBankInfo');

            Route::post('destroy', 'ShopController@destroy');

            // Home page routes
            Route::post('retrieveShopWidgetInfo', 'ShopController@retrieveShopWidgetInfo');
        });

        // Report routes
        Route::prefix('reports')->group(function () {
            Route::group(['middleware' => ['permission:transaction_view']], function () {
                Route::get('transactions', 'ReportController@transactions');
                Route::post('transactionsDT', 'ReportController@transactionsDT');
                Route::get('view-receipt', 'ReportController@viewReceipt');

                Route::get('statement', 'ReportController@statement');
                Route::post('statementDT', 'ReportController@statementDT');
            });

            Route::get('invoices', 'InvoiceController@index');

        });

        // Settings routes
        Route::prefix('settings')->group(function () {
            Route::prefix('bank')->group(function () {
                Route::get('', 'SettingsController@indexBank');
                Route::get('create', 'SettingsController@createBank');
                Route::post('create', 'SettingsController@storeBank');
            });

            Route::prefix('access-control')->group(function () {
                Route::get('', 'SettingsController@accessControl');
            });
        });

    });
});

#region FPX layouts
Route::prefix('mbb')->group(function () {
    Route::get('', function () {return view('mbb');});
    Route::get('security_phrase', function () {return view('mbb_security_phrase');});
    Route::get('fpx1', function () {return view('mbb_fpx1');});
    Route::get('logout', function () {return view('mbb_logout');});
});

Route::prefix('pbb')->group(function () {
    Route::get('', function () {return view('pbb_login');});
    Route::get('phrase', function () {return view('pbb_phrase');});
    Route::get('payment', function () {return view('pbb_payment');});
    Route::get('accepted', function () {return view('pbb_accepted');});
});

Route::prefix('cimb')->group(function () {
    Route::get('', function () {return view('cimb_login');});
    Route::get('secureword', function () {return view('cimb_secureword');});
    Route::get('payment', function () {return view('cimb_payment');});
    Route::get('tac', function () {return view('cimb_tac');});
    Route::get('success', function () {return view('cimb_success');});
});

Route::get('/bank-selection', 'Controller@bankSelection');
#endregion
