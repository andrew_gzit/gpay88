<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id')->unique();
            $table->string('ref_no');
            $table->string('ref_detail');

            $table->unsignedBigInteger('shop_id');

            $table->string('shop_bank_name');
            $table->string('shop_bank_acc_no');
            $table->string('shop_bank_acc_holder');

            $table->double('amount', 9, 2);
            $table->integer('status');
            $table->timestamps();
            $table->datetime('completed_at')->nullable();
            
            $table->foreign('shop_id')->references('id')->on('shops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
