<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftdeleteUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('sub_merchants', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('shops', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('sub_merchants', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('shops', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
