<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubMerchantShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_merchant_shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sub_merchant_user_id');
            $table->unsignedBigInteger('shop_user_id');
            $table->timestamps();

            $table->foreign('sub_merchant_user_id')->references('id')->on('users');
            $table->foreign('shop_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_merchant_shops');
    }
}
