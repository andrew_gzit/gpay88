<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Super Admin',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Admin',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Merchant',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Sub Merchant',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Shop',
                'guard_name' => 'web'
            ]
        ]);
    }
}
