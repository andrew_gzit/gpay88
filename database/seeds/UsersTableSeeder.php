<?php

use App\Enum;
use App\Shop;
use App\ShopBank;
use App\User;
use App\Utility;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->user_id = Utility::generateUniqueId();
        $user->username = 'gpay88sa';
        $user->password = bcrypt('Little*2019');
        $user->name = 'SuperAdmin';
        $user->phone_no = '-';
        $user->email = 'sa@gzpay.com';
        $user->save();

        $user->assignRole('Super Admin');

        // Merchant
        $merchantUser = new User();
        $merchantUser->user_id = Utility::generateUniqueId();
        $merchantUser->username = 'twmerchant';
        $merchantUser->password = bcrypt('qwer1234');
        $merchantUser->name = 'TW17888 MERCHANT';
        $merchantUser->phone_no = '-';
        $merchantUser->email = 'merchant@tw17888.com';
        $merchantUser->save();

        $merchantUser->assignRole(Enum::ROLE_MERCHANT);
        $merchantUser->assignPermission(Enum::ROLE_MERCHANT);

        // Shop
        $shopUser = new User();
        $shopUser->user_id = Utility::generateUniqueId();
        $shopUser->username = 'twshop';
        $shopUser->password = bcrypt('qwer1234');
        $shopUser->name = 'TW17888';
        $shopUser->phone_no = '-';
        $shopUser->email = 'shop@tw17888.com';
        $shopUser->save();

        $shopUser->assignRole(Enum::ROLE_SHOP);
        $shopUser->assignPermission(Enum::ROLE_SHOP);

        $shop = new Shop();
        $shop->user_id = $shopUser->id;
        $shop->merchant_id = $merchantUser->id;
        $shop->shop_name = 'TW17888';
        $shop->api_code = 'GP882057745';
        $shop->api_secret = 't0VCKXb2Env0t9jlFcMu';
        $shop->callback_url = 'http://www.tw17888.com/payment-callback';
        $shop->response_url = 'http://www.tw17888.com/payment-backend';
        $shop->status = Enum::STATUS_ACTIVE;
        $shop->save();

        // Shop Bank CIMB
        $sb = new ShopBank();
        $sb->shop_id = 1;
        $sb->bank_id = 1;
        $sb->bank_acc_no = 8603809354;
        $sb->bank_acc_holder = 'COMFORT HOUSEHOLD COLLECTION';

        $sb->min_threshold = 1;
        $sb->max_threshold = 1000;
        $sb->accumulated_threshold = 10000;
        $sb->save();

        // Shop Bank HLBB
        $sb = new ShopBank();
        $sb->shop_id = 1;
        $sb->bank_id = 2;
        $sb->bank_acc_no = 33701015487;
        $sb->bank_acc_holder = 'CHEONG WYSOON';

        $sb->min_threshold = 1;
        $sb->max_threshold = 1000;
        $sb->accumulated_threshold = 10000;
        $sb->save();

        // Shop Bank PBB
        $sb = new ShopBank();
        $sb->shop_id = 1;
        $sb->bank_id = 4;
        $sb->bank_acc_no = 3215678705;
        $sb->bank_acc_holder = 'COMFORT HOUSEHOLD COLLECTION';

        $sb->min_threshold = 1;
        $sb->max_threshold = 1000;
        $sb->accumulated_threshold = 10000;
        $sb->save();

    }
}
