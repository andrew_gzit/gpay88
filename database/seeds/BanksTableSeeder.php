<?php

use Illuminate\Database\Seeder;

  class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            [
                'abbr' => 'CIMB',
                'name' => 'CIMB Bank Berhad'
            ],
            [
                'abbr' => 'HLBB',
                'name' => 'Hong Leong Bank Berhad'
            ],
            [
                'abbr' => 'MBB',
                'name' => 'Malayan Banking Berhad'
            ],
            [
                'abbr' => 'PBB',
                'name' => 'Public Bank Berhad'
            ],
            [
                'abbr' => 'RHB',
                'name' => 'RHB Bank Berhad'
            ]
        ]);
    }
}
