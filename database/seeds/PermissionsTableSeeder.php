<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'shop_view',
                'guard_name' => 'web',
            ],
            [
                'name' => 'shop_create',
                'guard_name' => 'web',
            ],
            [
                'name' => 'shop_edit',
                'guard_name' => 'web',
            ],
            [
                'name' => 'shop_disable',
                'guard_name' => 'web',
            ],
            [
                'name' => 'shop_delete',
                'guard_name' => 'web',
            ],

            [
                'name' => 'merchant_view',
                'guard_name' => 'web',
            ],

            [
                'name' => 'merchant_create',
                'guard_name' => 'web',
            ],
            [
                'name' => 'merchant_edit',
                'guard_name' => 'web',
            ],
            [
                'name' => 'merchant_disable',
                'guard_name' => 'web',
            ],
            [
                'name' => 'merchant_delete',
                'guard_name' => 'web',
            ],

            [
                'name' => 'bank_view',
                'guard_name' => 'web',
            ],
            [
                'name' => 'bank_create',
                'guard_name' => 'web',
            ],
            [
                'name' => 'bank_edit',
                'guard_name' => 'web',
            ],
            [
                'name' => 'bank_delete',
                'guard_name' => 'web',
            ],
            [
                'name' => 'bank_disable',
                'guard_name' => 'web',
            ],

            [
                'name' => 'transaction_view',
                'guard_name' => 'web'
            ],

            [
                'name' => 'merchant_view',
                'guard_name' => 'web',
            ],

            [
                'name' => 'submerchant_view',
                'guard_name' => 'web',
            ],
            [
                'name' => 'submerchant_create',
                'guard_name' => 'web',
            ],
            [
                'name' => 'submerchant_edit',
                'guard_name' => 'web',
            ],
            [
                'name' => 'submerchant_disable',
                'guard_name' => 'web',
            ],
            [
                'name' => 'submerchant_delete',
                'guard_name' => 'web',
            ],
        ]);
    }
}
