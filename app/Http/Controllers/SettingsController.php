<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;
use App\ShopBank;
use App\Enum;
use App\Traits\ShopTrait;

class SettingsController extends Controller
{

    use ShopTrait;

    #region bank settings

    public function indexBank()
    {

        $user = Auth::user();

        $shops = $this->shopQuery()->get();

        $shopBanks = ShopBank::whereIn('shop_id', $shops->pluck('id'))
        ->join('banks', 'banks.id', 'bank_id')
        ->join('shops', 'shops.id', 'shop_banks.shop_id')
        ->orderBy('shop_id')
        ->select(['banks.*', 'shop_banks.*', 'shops.shop_name'])
        ->get();

        return view('merchants.settings.index-bank', compact('shopBanks'));
    }

    #endregion

    #region access control

    public function accessControl()
    {
        return view();
    }

    #endregion
}
