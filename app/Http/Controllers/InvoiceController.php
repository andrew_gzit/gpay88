<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Jobs\GenerateInvoice;
use App\Invoice;

use PDF;

class InvoiceController extends Controller
{
    use ResponseTrait;

    public function index()
    {

       // $invoicePdf = PDF::loadView('pdf.invoice');
        //return $invoicePdf->stream('invoice.pdf');
        $invoice = new Invoice();

        GenerateInvoice::dispatchNow($invoice);

        return view('merchants.invoice.index');
    }
}
