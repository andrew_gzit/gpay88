<?php

namespace App\Http\Controllers;

use App\Bank;
use App\DataTable;
use App\DataTableFilter;
use App\Enum;
use App\Shop;
use App\Traits\ShopTrait;
use App\Transaction;
use App\Utility;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    use ShopTrait;

    public function transactions()
    {
        $merchants = $this->merchantQuery()->get();
        $banks = Bank::get();

        return view('merchants.reports.transactions', compact('merchants', 'banks'));
    }

    private function defineTransactionQuery($status = null)
    {
        $query = null;

      

        if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {
            $query = Transaction::join('shops', 'shops.id', 'transactions.shop_id')
                ->join('users', 'shops.user_id', 'users.id');
        } else if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {
            $query = Transaction::join('shops', 'shops.id', 'transactions.shop_id')
                ->join('users', 'shops.user_id', 'users.id')
                ->where('shops.merchant_id', Auth::id());
        } else if (Auth::user()->hasRole(Enum::ROLE_SHOP)) {
            $query = Transaction::where('shops.user_id', Auth::id())
                ->join('shops', 'shops.id', 'transactions.shop_id')
                ->join('users', 'shops.user_id', 'users.id');
        } else if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {
            $query = Transaction::join('shops', 'shops.id', 'transactions.shop_id')
                ->join('users', 'shops.user_id', 'users.id')
                ->where('shops.merchant_id', Auth::user()->getMerchant()->id);
        }

        if($status == null){
            $query = $query->where(function ($query) {
                $query->where('transactions.status', Enum::TX_SUCCESS['key'])
                    ->orWhere('transactions.status', Enum::TX_REJECTED['key']);
            });
        }

        $query = $query->select(['transactions.*', 'shops.shop_name', 'users.user_id AS uuid']);

        return $query;
    }

    public function transactionsDT(Request $request)
    {
        try {

            // Define column names for ordering use
            $columns = [
                'transaction_id',
                'shop_name',
                'ref_no',
                'shop_bank_acc_no',
                'created_at',
                'ref_detail',
                'amount',
                'status',
            ];

            // Setup DataTable configurations
            $dt = new DataTable();
            $dt->limit = $request->length;
            $dt->start = $request->start;
            $dt->order = $columns[$request->input('order.0.column')];
            $dt->dir = $request->input('order.0.dir');

            if (!empty($request->input('search.value'))) {
                $dt->search = $request->input('search.value');
                $dt->searchables = ['transaction_id', 'shop_name', 'ref_no',
                    'ref_detail', 'transactions.created_at', 'amount'];
            }

            $dt->filters = collect();

            $filter = $request->filter;

            if (!empty($filter['startDate'])) {
                if (empty($filter['endDate'])) {
                    $filter['endDate'] = Carbon::now()->format('d/m/Y H:i:s A');
                }

                $sd = $filter['startDate'];
                $ed = $filter['endDate'];

                $dtFilter = new DataTableFilter();
                $dtFilter->type = DataTableFilter::DATE;
                $dtFilter->date_type = DataTableFilter::DATE_RANGE;
                $dtFilter->key = "transactions.created_at";
                $dtFilter->start_date = Carbon::createFromFormat('d/m/Y H:i:s A', $sd);
                $dtFilter->end_date = Carbon::createFromFormat('d/m/Y H:i:s A', $ed);

                $dt->filters->push($dtFilter);
            }

            if (!empty($filter['shop'])) {
                $dtFilter = new DataTableFilter();
                $dtFilter->type = DataTableFilter::STR;
                $dtFilter->key = 'users.user_id';
                $dtFilter->value = $filter['shop'];

                $dt->filters->push($dtFilter);
            }

            if ($filter['bank'] !== '-' && $filter['bank'] !== '' && $filter['bank'] !== null) {
                $dtFilter = new DataTableFilter();
                $dtFilter->type = DataTableFilter::STR;
                $dtFilter->key = 'transactions.shop_bank_name';
                $dtFilter->value = $filter['bank'];

                $dt->filters->push($dtFilter);
            }

            if ($filter['status'] !== '-' && $filter['status'] !== '' && $filter['status'] !== null) {
                $dtFilter = new DataTableFilter();
                $dtFilter->type = DataTableFilter::STR;
                $dtFilter->key = 'transactions.status';
                $dtFilter->value = $filter['status'];

                $dt->filters->push($dtFilter);
            }

            // Define query for DataTable use
            $dt->query = $this->defineTransactionQuery($filter['status']);

            $returnObj = $dt->filterData($dt);
            $dataCollection = collect();

            $transactions = $returnObj->data;

            foreach ($transactions as $tx) {
                $bankCode = preg_replace('/([a-z ]+)/', '', $tx->shop_bank_name);

                $data['transaction_id'] = $tx->transaction_id;
                $data['shop_name'] = $tx->shop_name;
                $data['ref_no'] = $tx->ref_no ?? '-';
                $data['bank'] = substr($bankCode, 0, 4) . ' ' . $tx->shop_bank_acc_no;
                $data['time'] = $tx->created_at->format('d/m/Y h:i:s A');
                $data['amount'] = number_format($tx->amount, 2);
                $data['remarks'] = $tx->ref_detail;

                if ($tx->receipt_path != null) {
                    $data['remarks'] .= '<a href="javascript:void(0);" class="a-view-receipt" data-img="' . env("RECEIPT_DOMAIN") . $tx->receipt_path . '">View Receipt</a>';
                }

                $data['status'] = Utility::transactionStatusToHtmL($tx->status);

                $dataCollection->push($data);
            }

            $jsonData = [
                "draw" => (int) $request->input('draw'),
                "recordsTotal" => (int) $returnObj->recordsFiltered,
                "recordsFiltered" => (int) $returnObj->recordsFiltered,
                "data" => $dataCollection,
            ];

            return json_encode($jsonData);

        } catch (Exception $ex) {

            return $ex->getMessage();

        }
    }

    public function statement(Request $request)
    {
        try {

            return view('merchants.reports.statement');

        } catch (Exception $ex) {

        }
    }

    private function defineStatementQuery()
    {
        $shops = $this->shopQuery()->get();

        return Shop::leftJoin('transactions', 'shops.id', 'transactions.shop_id')
            ->whereIn('shops.id', $shops->pluck('id'))
            ->where('transactions.status', Enum::TX_SUCCESS['key'])
            ->groupBy('shop_id')
            ->select(['shops.shop_name', DB::raw('SUM(transactions.amount) AS amount')]);
    }

    public function statementDT(Request $request)
    {
        try {

            // Define column names for ordering use
            $columns = [
                'shop_name',
                'amount',
            ];

            // Setup DataTable configurations
            $dt = new DataTable();
            $dt->limit = $request->length;
            $dt->start = $request->start;
            $dt->order = $columns[$request->input('order.0.column')];
            $dt->dir = $request->input('order.0.dir');

            if (!empty($request->input('search.value'))) {
                $dt->search = $request->input('search.value');
                $dt->searchables = ['shop_name'];
            }

            $dt->filters = collect();

            $filter = $request->filter;

            if (!empty($filter['startDate'])) {
                if (empty($filter['endDate'])) {
                    $filter['endDate'] = Carbon::now()->format('d/m/Y H:i:s A');
                }

                $sd = $filter['startDate'];
                $ed = $filter['endDate'];

                $dtFilter = new DataTableFilter();
                $dtFilter->type = DataTableFilter::DATE;
                $dtFilter->date_type = DataTableFilter::DATE_RANGE;
                $dtFilter->key = "transactions.created_at";
                $dtFilter->start_date = Carbon::createFromFormat('d/m/Y H:i:s A', $sd . ' 00:00:00 AM');
                $dtFilter->end_date = Carbon::createFromFormat('d/m/Y H:i:s A', $ed . ' 11:59:59 PM');

                $dt->filters->push($dtFilter);
            }

            $dt->query = $this->defineStatementQuery();

            $returnObj = $dt->filterData($dt);

            $dataCollection = collect();
            $transactions = $returnObj->data;

            foreach ($transactions as $tx) {
                $data['shop_name'] = $tx->shop_name;
                $data['amount'] = number_format($tx->amount, 2);

                $dataCollection->push($data);
            }

            $jsonData = [
                "draw" => (int) $request->input('draw'),
                "recordsTotal" => (int) $returnObj->recordsFiltered,
                "recordsFiltered" => (int) $returnObj->recordsFiltered,
                "data" => $dataCollection,
            ];

            return json_encode($jsonData);

        } catch (Exception $ex) {

            return $this->json(false, "Unable to load statements");

        }
    }
}
