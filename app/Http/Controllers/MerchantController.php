<?php

namespace App\Http\Controllers;

use App\DataTable;
use App\RoleEnum;
use App\Enum;
use App\Merchant;
use App\Shop;
use App\SubMerchantShop;
use App\Traits\ResponseTrait;
use App\Traits\ShopTrait;
use App\User;
use App\Utility;
use Auth;
use Exception;
use Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Redirect;
use DB;
use Carbon\Carbon;

class MerchantController extends Controller
{
    use ResponseTrait, ShopTrait;

    #region merchants

    public function home()
    {
        $merchants = $this->merchantQuery()->get();
        $shops = $this->shopQuery()->get();

        return view('merchants.home', compact('merchants', 'shops'));
    }

    public function destroy(Request $request)
    {
        try {

            $user = User::where('user_id', $request->user)
                ->firstOrFail();

            $shops = Shop::where('merchant_id', $user->id)
                ->get();

            if (count($shops) > 0) {

                return $this->json(false, __('dict.un_del_mer_del') . count($shops) . __('dict.shop_attac'));

            } else {
                $user->delete();
                $this->successMsg(__('dict.suc_del') . $user->name . __('dict.m_merchant'));
                return $this->json(true, '', action('MerchantController@merchants'));
            }

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_del_mer'), $ex->getMessage());

        }
    }

    public function merchants()
    {
        return view('merchants.merchants.index');
    }

    public function merchantsDT(Request $request)
    {
        try {

            // Define column names for ordering use
            $columns = [
                'users.name',
                'email',
                'phone_no',
                'processing_fee',
                'status',
            ];

            // Setup DataTable configurations
            $dt = new DataTable();
            $dt->limit = $request->length;
            $dt->start = $request->start;
            $dt->order = $columns[$request->input('order.0.column')];
            $dt->dir = $request->input('order.0.dir');

            if (!empty($request->input('search.value'))) {
                $dt->search = $request->input('search.value');
                $dt->searchables = ['users.name', 'email', 'phone_no'];
            }

            // Define query for DataTable use
            $dt->query = $this->merchantQuery();

            $returnObj = $dt->filterData($dt);
            $dataCollection = collect();

            $merchants = $returnObj->data;

            $canViewMerchantOnly = Auth::user()->hasRole(Enum::ROLE_SHOP);

            foreach ($merchants as $m) {
                $data['name'] = $this->constructMerchantName($m, $canViewMerchantOnly);
                $data['email'] = $m->email;
                $data['phone_no'] = $m->phone_no;
                $data['processing_fee'] = number_format($m->processing_fee, 2);
                $data['status'] = Utility::statusToHtml($m->status);
                $data['actions'] = Utility::merchantActions($m);

                $dataCollection->push($data);
            }

            $jsonData = [
                "draw" => (int) $request->input('draw'),
                "recordsTotal" => (int) $returnObj->recordsTotal,
                "recordsFiltered" => (int) $returnObj->recordsFiltered,
                "data" => $dataCollection,
            ];

            return json_encode($jsonData);

        } catch (Exception $ex) {

            return $ex->getMessage();

        }
    }

    public function suspendMerchant(Request $request)
    {
        try {
            $merchant = User::where('user_id', $request->merchantId)
                ->firstOrFail();

            $newStatus = $merchant->status == Enum::STATUS_ACTIVE ? Enum::STATUS_INACTIVE : Enum::STATUS_ACTIVE;

            $merchant->status = $newStatus;
            $merchant->save();

            return $this->json(true, __('dict.success_update') . $merchant->name . __('dict.m_status'));
        } catch (Exception $ex) {
            return $this->json(false, $ex->getMessage());
        }
    }

    public function viewMerchant($merchantId)
    {
        try {

            $merchant = User::where('user_id', $merchantId)
                ->firstOrFail();

            // $shops = $this->shopQuery()->get();

            if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {

                $subMerchantShops = SubMerchantShop::where('sub_merchant_user_id', Auth::id())->get()->pluck('shop_user_id');

                $shops = Shop::where('merchant_id', $merchant->id)
                    ->whereIn('shops.user_id', $subMerchantShops)
                    ->join('users', 'shops.user_id', 'users.id')
                    ->get(['users.*', 'users.user_id AS uuid', 'shops.shop_name']);

            } else if (Auth::user()->hasRole(Enum::ROLE_SHOP)) {

                $shops = Shop::where('merchant_id', $merchant->id)
                    ->where('user_id', Auth::id())
                    ->join('users', 'shops.user_id', 'users.id')
                    ->get(['users.*', 'users.user_id AS uuid', 'shops.shop_name']);

            } else {
                $shops = Shop::where('merchant_id', $merchant->id)
                    ->join('users', 'shops.user_id', 'users.id')
                    ->get(['users.*', 'users.user_id AS uuid', 'shops.shop_name']);
            }

            if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {
                $subMerchants = User::join('sub_merchants', 'users.id', 'sub_merchants.user_id')
                    ->where('sub_merchants.user_id', Auth::id())
                    ->select(['users.*'])
                    ->get();
            } else {
                $subMerchants = User::join('sub_merchants', 'users.id', 'sub_merchants.user_id')
                    ->where('sub_merchants.merchant_id', $merchant->id)
                    ->select(['users.*'])
                    ->get();
            }

            return view('merchants.merchants.view-details', compact('merchant', 'shops', 'subMerchants'));

        } catch (ModelNotFoundException $ex) {

            $this->errorMsg(__('dict.mer_not_found'));
            return redirect::back();

        } catch (Exception $ex) {

            $this->errorMsg(__('dict.un_view_mer'));
            return redirect::back();

        }
    }

    public function createMerchant()
    {
        return view('merchants.merchants.create');
    }

    public function storeMerchant(Request $request)
    {
        try {

            $user = DB::transaction(function () use ($request) {

                $user = new User();
                $user->user_id = Utility::generateUniqueId();
                $user->name = strtoupper($request->name);
                $user->email = $request->email;
                $user->phone_no = $request->phone_no;
                $user->username = $request->username;
                $user->password = bcrypt($request->password);
                $user->assignRole(Enum::ROLE_MERCHANT);
                $user->assignPermission(Enum::ROLE_MERCHANT);
                $user->save();

                // Create merchant profile
                $merchant = new Merchant();
                $merchant->user_id = $user->id;
                $merchant->processing_fee = $request->processing_fee;
                $merchant->next_invoice_date = Carbon::now()->addMonths(1);
                $merchant->save();

                return $user;
            });

            $this->successMsg(__('dict.suc_created') . $user->name . __('dict.m_merchant'));
            return redirect::action('MerchantController@viewMerchant', $user->user_id);

        } catch (Exception $ex) {

            return redirect::back();

        }
    }

    public function editMerchant($merchantUserId)
    {
        try {

            $user = User::where('user_id', $merchantUserId)
                ->firstOrFail();

            return $this->json(true, '', $user);

        } catch (Exception $ex) {

            return $this->json(false);

        }
    }

    public function updateMerchant($merchantUserId, Request $request)
    {
        try {

            $merchantUser = DB::transaction(function() use($request, $merchantUserId){
                $merchantUser = User::where('user_id', $merchantUserId)->firstOrFail();

                $merchantUser->email = $request->email;
                $merchantUser->phone_no = $request->phone_no;
                $merchantUser->save();
    
                $merchant = Merchant::where('user_id', $merchantUser->id)->firstOrFail();
                $merchant->processing_fee = $request->processing_fee;
                $merchant->save();

                return $merchantUser;
            });

            $this->successMsg(__('dict.suc_up_merchant_det'));
            return redirect::action('MerchantController@viewMerchant', $merchantUser->user_id);

        } catch (Exception $ex) {
            $this->errorMsg(__('dict.un_up_mer_det'));
            return redirect::back();

        }
    }

    public function validateDetails(Request $request)
    {
        try {

            $exist = User::where($request->key, $request->value)
                ->where('user_id', '!=', $request->user)
                ->first();

            if (!empty($exist)) {
                return $this->json(false, __('dict.email_used'));
            } else {
                return $this->json(true);
            }

        } catch (Exception $ex) {

            return $this->json(false, $ex->getMessage());

        }
    }

    public function changePassword(Request $request)
    {
        try {

            $merchantUser = User::where('user_id', $request->merchant)
                ->firstOrFail();

            if (Hash::check($request->oldPassword, $merchantUser->password)) {
                $merchantUser->password = bcrypt($request->password);
                $merchantUser->save();
                return $this->json(true, __('dict.suc_up_pw'));
            } else {
                return $this->json(false, __('dict.in_pw'));
            }

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_chg_pw'), $ex->getMessage());

        }
    }

    public function resetPassword(Request $request)
    {
        try {

            if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {

                $merchantUser = User::where('user_id', $request->merchant)
                    ->update([
                        'password' => bcrypt($request->password),
                    ]);

                return $this->json(true, __('dict.suc_res_pw'));

            } else {

                return $this->json(false, 'Err!');

            }

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_res_mer_pw'), $ex->getMessage());

        }
    }

    public function retrieveShops(Request $request)
    {
        try {

            $merchant = User::where('user_id', $request->merchantId)->firstOrFail();

            if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {

                $subMerchantShops = SubMerchantShop::where('sub_merchant_user_id', Auth::id())->get()->pluck('shop_user_id');

                $shops = Shop::where('merchant_id', $merchant->id)
                    ->whereIn('shops.user_id', $subMerchantShops)
                    ->join('users', 'shops.user_id', 'users.id')
                    ->get(['users.*', 'shops.shop_name']);

            } else if (Auth::user()->hasRole(Enum::ROLE_SHOP)) {

                $shops = Shop::where('merchant_id', $merchant->id)
                    ->where('shops.user_id', Auth::id())
                    ->join('users', 'shops.user_id', 'users.id')
                    ->get(['users.*', 'shops.shop_name']);

            } else {
                $shops = Shop::where('merchant_id', $merchant->id)
                    ->join('users', 'shops.user_id', 'users.id')
                    ->get(['users.*', 'shops.shop_name']);
            }

            return $this->json(true, '', $shops);

        } catch (Exception $ex) {

            dd($ex->getMessage());

        }
    }

    public function disableMerchant(Request $request)
    {
        try {

            $merchant = User::where('user_id', $request->merchant)
                ->firstOrFail();

            $newStatus = $merchant->status == Enum::STATUS_ACTIVE ? Enum::STATUS_INACTIVE : Enum::STATUS_ACTIVE;

            $merchant->status = $newStatus;
            $merchant->save();

            return $this->json(true, __('dict.successfully') . $newStatus . __('dict.m_shop'));

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_up_mer_stat'), $ex->getMessage());
        }
    }

    #endregion
}
