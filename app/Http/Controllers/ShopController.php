<?php

namespace App\Http\Controllers;

use App\Bank;
use App\DataTable;
use App\Enum;
use App\ReturnObj;
use App\Shop;
use App\ShopBank;
use App\Traits\ResponseTrait;
use App\Traits\ShopTrait;
use App\Transaction;
use App\User;
use App\Utility;
use Auth;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Redirect;

class ShopController extends Controller
{
    use ResponseTrait, ShopTrait;

    public function shops()
    {
        return view('merchants.shops.index');
    }

    public function shopsDT(Request $request)
    {
        try {

            // Define column names for ordering use
            $columns = [
                'users.name',
                'email',
                'phone_no',
                'callback_url',
                'response_url',
                'api_code',
                'api_secret',
                'status',
                'actions',
            ];

            // Setup DataTable configurations
            $dt = new DataTable();
            $dt->limit = $request->length;
            $dt->start = $request->start;
            $dt->order = $columns[$request->input('order.0.column')];
            $dt->dir = $request->input('order.0.dir');

            if (!empty($request->input('search.value'))) {
                $dt->search = $request->input('search.value');
                $dt->searchables = ['users.name', 'email', 'phone_no'];
            }

            // Define query for DataTable use
            $dt->query = $this->shopQuery();

            $returnObj = $dt->filterData($dt);
            $dataCollection = collect();

            $shops = $returnObj->data;

            foreach ($shops as $s) {
                $data['shop_name'] = '<a href="/shops/' . $s->uuid . '/edit">' . $s->shop_name . '</a>';
                $data['email'] = $s->email;
                $data['phone_no'] = $s->phone_no;
                $data['callback_url'] = $s->callback_url;
                $data['response_url'] = $s->response_url;
                $data['api_code'] = $s->api_code;
                $data['api_secret'] = $s->api_secret;
                $data['status'] = Utility::statusToHtml($s->status);
                $data['actions'] = Utility::getShopActions($s->uuid, $s->status);

                $dataCollection->push($data);
            }

            $jsonData = [
                "draw" => (int) $request->input('draw'),
                "recordsTotal" => (int) $returnObj->recordsTotal,
                "recordsFiltered" => (int) $returnObj->recordsFiltered,
                "data" => $dataCollection,
            ];

            return json_encode($jsonData);

        } catch (Exception $ex) {

            return $ex->getMessage();

        }
    }

    public function createShop($merchantUserId = null)
    {
        try {

            // Access page from merchant
            if (!empty($merchantUserId)) {

                $merchant = User::where('user_id', $merchantUserId)
                    ->firstOrFail();
                $merchants = null;

            } else {

                if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {
                    // Access page from shop list
                    $merchants = User::role(Enum::ROLE_MERCHANT)->get();
                    $merchant = null;

                } else {

                    $merchants = null;
                    $merchant = Auth::user();

                }
            }

            $shopUser = null;

            $banks = Bank::get();

            return view('merchants.shops.create', compact('merchant', 'merchants', 'shopUser', 'banks'));

        } catch (Exception $ex) {

            dd($ex->getMessage());

        }
    }

    public function editShop($shopId)
    {
        try {

            $merchants = User::role(Enum::ROLE_MERCHANT)->get();
            $banks = Bank::get();

            $shopUser = User::where('user_id', $shopId)
                ->with('shop')
                ->firstOrFail();

            $shopBanks = ShopBank::where('shop_id', $shopUser->shop->id)
                ->join('banks', 'banks.id', 'bank_id')
                ->select(['banks.*', 'shop_banks.*'])
                ->get();

            $merchantUser = User::where('id', $shopUser->shop->merchant_id)->firstOrFail();

            return view('merchants.shops.create', compact('merchantUser', 'merchants', 'banks', 'shopUser', 'shopBanks'));

        } catch (Exception $ex) {

            $this->errorMsg(__('dict.un_edit_shop'));
            return redirect::back();

        }
    }

    public function retrieveBank($bankId)
    {
        try {

            $shopBank = ShopBank::where('id', $bankId)->firstOrFail();

            return $this->json(true, '', $shopBank);

        } catch (Exception $ex) {

        }
    }

    public function updateBank(Request $request)
    {
        try {

            $shopBank = ShopBank::where('id', $request->bankId)->firstOrFail();
            $shopBank->bank_acc_no = $request->bank_acc_no;
            $shopBank->bank_acc_holder = $request->bank_acc_holder;

            $shopBank->min_threshold = $this->getValidThreshold($request->min_threshold);
            $shopBank->max_threshold = $this->getValidThreshold($request->max_threshold);
            $shopBank->accumulated_threshold = $this->getValidThreshold($request->accumulated_threshold);

            $shopBank->status = $request->status;
            $shopBank->save();

            $shopBank->status_elem = Utility::statusToHtml($shopBank->status);

            return $this->json(true, __('dict.suc_bank_det'), $shopBank);

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_edit_bank_det'), $ex->getMessage());

        }
    }

    // Threshold value must be higher than 0, lesser than max threshold
    private function getValidThreshold($threshold_value)
    {
        return round(min(max(0, $threshold_value), Enum::MAX_THRESHOLD), 2);
    }

    public function addBank($shopId, Request $request)
    {
        try {

            $shop = User::where('users.user_id', $shopId)
                ->join('shops', 'users.id', 'shops.user_id')
                ->select(['users.*', 'shops.id AS shop_id'])
                ->firstOrFail();

            $shopBank = ShopBank::where('shop_id', $shop->shop_id)
                ->where('bank_id', $request->bankId)
                ->first();

            if (!empty($shopBank)) {
                return $this->json(false, __('dict.bank_added'));
            } else {
                $sb = new ShopBank();
                $sb->shop_id = $shop->shop_id;
                $sb->bank_id = $request->bankId;
                $sb->bank_acc_no = $request->bankAccNo;
                $sb->bank_acc_holder = $request->bankHolder;

                $sb->min_threshold = $this->getValidThreshold($request->min_threshold);
                $sb->max_threshold = $this->getValidThreshold($request->max_threshold);
                $sb->accumulated_threshold = $this->getValidThreshold($request->accumulated_threshold);

                $sb->save();
            }

            $sb->status = Utility::statusToHtmL(Enum::STATUS_ACTIVE);
            $sb->action = $sb->shopBankActions();

            return $this->json(true, __('dict.suc_add_bank'), $sb);

        } catch (Exception $ex) {

            return $this->json(false, __('dict.bank_not_add'));

        }
    }

    public function deleteBank(Request $request)
    {
        try {
            $shopBank = ShopBank::where('id', $request->bank)->firstOrFail();

            $shopBank->delete();

            return $this->json(true, __('dict.bank_del'), $shopBank->id);

        } catch (ModelNotFoundException $ex) {
            return $this->json(false, __('dict.bank_not_found'));
        } catch (Exception $ex) {

        }
    }

    public function storeShop(Request $request)
    {
        try {

            $returnObj = DB::transaction(function () use ($request) {
                $user = new User();
                $user->user_id = Utility::generateUniqueId();
                $user->username = $request->username;
                $user->name = $request->shop_name;
                $user->email = $request->email;
                $user->phone_no = $request->phone_no;
                $user->password = bcrypt($request->password);
                $user->assignRole(Enum::ROLE_SHOP);

                // Assign shop permissions
                $user->assignPermission(Enum::ROLE_SHOP);

                $user->save();

                if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {
                    $merchantId = Auth::id();
                    $merchantUserId = Auth::user()->id;
                } else {
                    $merchant = User::where('user_id', $request->merchant)
                        ->firstOrFail();

                    $merchantId = $merchant->id;
                    $merchantUserId = $merchant->user_id;
                }

                $shop = new Shop();
                $shop->shop_name = $request->shop_name;
                $shop->user_id = $user->id;
                $shop->merchant_id = $merchantId; // Merchant user id
                $shop->api_code = Shop::generateAPICode();
                $shop->api_secret = Shop::generateAPISecret();
                $shop->save();

                $returnObj = new ReturnObj();
                $returnObj->merchantUserId = $merchantUserId;
                $returnObj->shopUserId = $user->user_id;

                return $returnObj;
            });

            $this->successMsg(__('dict.suc_shop'));
            return redirect::action('ShopController@editShop', $returnObj->shopUserId);

        } catch (Exception $ex) {

        }
    }

    public function updateBankStatus(Request $request)
    {
        try {

            $shopBank = ShopBank::findOrFail($request->shopBank);

            if ($this->authorizedShop($shopBank->shop_id)) {
                $shopBank->status = $shopBank->status == Enum::STATUS_ACTIVE ? Enum::STATUS_INACTIVE : Enum::STATUS_ACTIVE;
                $shopBank->save();
                return $this->json(true, __('dict.success_update_bank_status'));
            } else {
                return $this->json(false, __('dict.unsuccessful_update_bank_status'));
            }

        } catch (Exception $ex) {

            return $this->json(false, __('dict.unsuccessful_update_bank_status'));

        }
    }

    public function updateShop($shopId, Request $request)
    {
        try {

            $returnObj = DB::transaction(function () use ($shopId, $request) {

                $user = User::where('user_id', $shopId)
                    ->firstOrFail();

                $user->username = $request->username;
                $user->name = $request->shop_name;
                $user->email = $request->email;
                $user->phone_no = $request->phone_no;
                $user->save();

                $shop = Shop::where('user_id', $user->id)
                    ->firstOrFail();

                $shop->shop_name = $request->shop_name;
                $shop->callback_url = $request->callback_url;
                $shop->response_url = $request->response_url;
                $shop->save();

                return;
            });

            $this->successMsg(__('dict.suc_up_shop_det'));

            return redirect::back();

        } catch (Exception $ex) {

        }
    }

    public function retrieveShopBankInfo(Request $request)
    {
        try {

            $user = User::where('user_id', $request->shopId)->firstOrFail();

            $shopBank = ShopBank::join('shops', 'shops.id', 'shop_banks.shop_id')
                ->where('shops.user_id', $user->id)
                ->join('banks', 'banks.id', 'shop_banks.bank_id')
                ->select(['shop_banks.*', 'banks.abbr'])
                ->orderBy('status')
                ->get();

            return $this->json(true, '', $shopBank);

        } catch (ModelNotFoundException $ex) {

            return $this->json(false, __('dict.un_retrieve_bank'));

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_retrieve_bank'));

        }
    }

    public function retrieveShopWidgetInfo(Request $request)
    {
        try {

            $shop = Shop::join('users', 'shops.user_id', 'users.id')
                ->where('users.user_id', $request->shopId)
                ->select(['shops.id'])
                ->firstOrFail();

            $todayVolumeQuery = DB::raw('(SELECT COALESCE(SUM(amount), 0) FROM transactions WHERE status = ' . Enum::TX_SUCCESS['key'] . ' AND DATE(created_at) = CURRENT_DATE AND shop_id = ' . $shop->id . ') AS todayVolume');
            $pastMonthVolumeQuery = DB::raw('(SELECT COALESCE(SUM(amount), 0) FROM transactions WHERE status = ' . Enum::TX_SUCCESS['key'] . ' AND YEAR(created_at) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(created_at) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND shop_id = ' . $shop->id . ') AS pastMonthVolume');
            $curMonthVolumeQuery = DB::raw('(SELECT COALESCE(SUM(amount), 0) FROM transactions WHERE status = ' . Enum::TX_SUCCESS['key'] . ' AND YEAR(created_at) = YEAR(CURRENT_DATE) AND MONTH(created_at) = MONTH(CURRENT_DATE) AND shop_id = ' . $shop->id . ') AS curMonthVolume');

            $transaction = Transaction::where('shop_id', $shop->id)
                ->where('status', Enum::TX_SUCCESS['key'])
                ->select([$todayVolumeQuery, $pastMonthVolumeQuery, $curMonthVolumeQuery, DB::raw('COALESCE(SUM(amount), 0) AS totalVolume'), DB::raw('COALESCE(COUNT(id), 0) AS totalTransactions')])
                ->first();

            return $this->json(true, '', $transaction);

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_retrieve_widget'));

        }
    }

    public function disableShop(Request $request)
    {
        try {

            $shop = Shop::where('users.user_id', $request->shop)
                ->join('users', 'users.id', 'shops.user_id')
                ->select(['shops.*'])
                ->firstOrFail();

            $newStatus = $shop->status == Enum::STATUS_ACTIVE ? Enum::STATUS_INACTIVE : Enum::STATUS_ACTIVE;

            $shop->status = $newStatus;
            $shop->save();

            $successMsg = __('dict.success_update') . $shop->shop_name . __('dict.shop_status');

            $this->successMsg($successMsg);
            return $this->json(true, $successMsg);

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_dis_shop'), $ex->getMessage());

        }
    }

    public function destroy(Request $request)
    {
        try {

            $user = User::where('user_id', $request->user)
                ->firstOrFail();

            $shop = Shop::where('user_id', $user->id)
                ->delete();

            $user->delete();

            $this->successMsg(__('dict.suc_del') . $user->name . __('dict.m_shop'));
            return $this->json(true, '', action('ShopController@shops'));

        } catch (Exception $ex) {

            return $this->json(false);

        }
    }

}
