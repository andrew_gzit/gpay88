<?php

namespace App\Http\Controllers;

use App;
use App\Enum;
use App\ReturnObj;
use App\Traits\ResponseTrait;
use App\Transaction;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Lang;

class AccountController extends Controller
{

    use ResponseTrait;

    public function changeLanguage(Request $request)
    {
        try {
            Session::put('applocale', $request->locale);
            return response()->json(['success' => true, 'data' => Session::get('applocale')]);
        } catch (Expectation $ex) {
            return $this->json(false, '', $ex->getMessage());
        }
    }

    public function home()
    {
        return redirect::action('AccountController@login');
    }

    public function login()
    {
        return view('merchants.login');
    }

    public function auth(Request $request)
    {
        try {

            $user = User::where('username', $request->username)->first();

            if(!empty($user)){
                if ($user->status != Enum::STATUS_ACTIVE) {
                    $this->errorMsg(Lang::get('dict.acc_suspended') . '. <br>' . env('CONTACT_INFO')); //Your account has been suspended
                    return redirect::back();
                }
    
                if (Hash::check($request->password, $user->password)) {
                    Auth::loginUsingId($user->id);
                    return redirect::action('MerchantController@home');
                } else {
                    $this->errorMsg('Incorrect username or password entered! <br>Please try again.');
                    return redirect::back();
                }
            } else {
                $this->errorMsg('Incorrect username or password entered! <br>Please try again.');
                return redirect::back();
            }

        } catch (Exception $ex) {

            $this->errorMsg(__('dict.un_login') . ' <br>' . env('CONTACT_INFO') . $ex->getMessage());
            return redirect::back();

        }
    }

    public function logout()
    {
        try {
            Auth::logout();
            return redirect::action('AccountController@login');
        } catch (Exception $ex) {
            $this->errorMsg(__('dict.un_logout'));
            return redirect::action('AccountController@home');
        }
    }

    public function validateDetails(Request $request)
    {
        try {

            $exist = User::where($request->key, $request->value)
                ->where('user_id', '!=', $request->user)
                ->first();

            if (!empty($exist)) {
                return $this->jsonError();
            } else {
                return $this->json(true);
            }

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_val_details'));

        }
    }

    public function summaryReport(Request $request)
    {
        try {

            $shopUserId = $request->user;
            $chartData = new ReturnObj();
            $labels = [];
            $data = [];

            // Add dates till now into labels array for chart
            $firstDay = new Carbon('first day of this month');

            // Add one day to account for first day
            $daysDiff = $firstDay->diffInDays(Carbon::now()) + 1;

            for ($i = 0; $i < $daysDiff; $i++) {
                array_unshift($labels, date("d/m/Y", strtotime('-' . $i . 'day')));
            }

            $shop = User::where('users.user_id', $shopUserId)
                ->join('shops', 'users.id', 'shops.user_id')
                ->select(['shops.*'])
                ->firstOrFail();

            $transactions = Transaction::where('shop_id', $shop->id)
                ->where('status', Enum::TX_SUCCESS['key'])
                ->whereMonth('created_at', Carbon::now()->format('m'))
                ->whereYear('created_at', Carbon::now()->format('Y'))
                ->select(['amount', DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") AS created_date')])
                ->get();

            $grouped = $transactions->groupBy('created_date');

            // Sum amount of sales for the particular day
            foreach ($labels as $date) {
                if (!empty($grouped[$date])) {
                    $sum = $grouped[$date]->sum('amount');
                    array_push($data, round($sum, 2));
                } else {
                    array_push($data, 0);
                }
            }

            // Configure data for chart use
            $dataObj = new ReturnObj();
            $dataObj->label = 'Total Volume (RM)';
            $dataObj->data = $data;
            $dataObj->fill = false;
            $dataObj->borderColor = '#78efa8';

            $chartData->labels = $labels;
            $chartData->datasets = [$dataObj];

            return $this->json(true, '', $chartData);

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_report'), $ex->getMessage());

        }
    }

    public function changePassword(Request $request)
    {
        try {

            $user = User::where('user_id', $request->user)
                ->firstOrFail();

            if (Hash::check($request->oldPassword, $user->password)) {
                $user->password = bcrypt($request->password);
                $user->save();
                return $this->json(true, __('dict.suc_up_pw'));
            } else {
                return $this->json(false, __('dict.in_pw'));
            }

            return $this->json(true, __('dict.suc_up_pw'));

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_chg_pw'), $ex->getMessage());

        }
    }

    public function resetPassword(Request $request)
    {
        try {

            // Reset pw available to super admin or
            // Merchant reset pw for shop / sub merchant
            if (
                Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN) ||
                (
                    Auth::user()->hasRole(Enum::ROLE_MERCHANT) &&
                    ($request->accType == Enum::ROLE_SUBMERCHANT || $request->accType == Enum::ROLE_SHOP)
                )
            ) {

                $user = User::where('user_id', $request->user)
                    ->update([
                        'password' => bcrypt($request->password),
                    ]);

                return $this->json(true, __('dict.suc_res_pw'));

            } else {
                return $this->json(false, __('dict.unauthorized'));
            }

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_res_pw'));

        }
    }
}
