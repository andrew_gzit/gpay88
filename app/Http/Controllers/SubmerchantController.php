<?php

namespace App\Http\Controllers;

use App\Enum;
use App\ReturnObj;
use App\SubMerchant;
use App\SubMerchantShop;
use App\Traits\ResponseTrait;
use App\Traits\ShopTrait;
use App\User;
use App\Utility;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Redirect;

class SubmerchantController extends Controller
{
    use ResponseTrait, ShopTrait;

    public function create($merchantId)
    {
        $merchant_shops = $this->shopQuery()->get();

        if (!empty($merchantId)) {
            $merchantUser = User::where('user_id', $merchantId)->firstOrFail();
            $merchants = null;
        } else {
            $merchantUser = null;
            $merchants = $this->merchantQuery()->get();
        }

        return view('merchants.submerchants.create', compact('merchantUser', 'merchants', 'merchant_shops'));
    }

    public function store(Request $request)
    {
        try {

            $retObj = DB::transaction(function () use ($request) {
                $merchant = User::where('user_id', $request->merchant)->firstOrFail();

                // sm = Sub Merchant
                $smUser = new User();
                $smUser->user_id = Utility::generateUniqueId();
                $smUser->name = strtoupper($request->name);
                $smUser->username = $request->username;
                $smUser->phone_no = $request->phone_no;
                $smUser->email = $request->email;
                $smUser->password = bcrypt($request->password);
                $smUser->assignRole(Enum::ROLE_SUBMERCHANT);
                $smUser->assignPermission(Enum::ROLE_SUBMERCHANT);
                $smUser->save();

                $sm = new SubMerchant();
                $sm->user_id = $smUser->id;
                $sm->merchant_id = $merchant->id;
                $sm->save();

                // Attach submerchant to selected shops
                $subMerchantShopArr = [];

                if ($request->merchant_shop) {
                    foreach ($request->merchant_shop as $m_shop) {
                        array_push($subMerchantShopArr, [
                            'sub_merchant_user_id' => $smUser->id,
                            'shop_user_id' => $m_shop,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ]);
                    }

                    SubMerchantShop::insert($subMerchantShopArr);
                }

                $retObj = new ReturnObj();
                $retObj->smUser = $smUser;
                $retObj->merchant = $merchant;

                return $retObj;
            });

            $this->successMsg(__('dict.suc_created') . $retObj->smUser->name . __('dict.m_sub_mer'));
            return redirect::action('MerchantController@viewMerchant', $retObj->merchant->user_id);

        } catch (Exception $ex) {

            return redirect::back();

        }
    }

    public function edit($submerchantUserId)
    {

        $submerchantUser = User::where('users.user_id', $submerchantUserId)
            ->join('sub_merchants', 'users.id', 'sub_merchants.user_id')
            ->select(['users.*', 'sub_merchants.merchant_id'])
            ->firstOrFail();

        $merchantUser = User::where('id', $submerchantUser->merchant_id)->firstOrFail();

        $merchant_shops = $this->shopQuery()->get();
        $submerchant_shops = SubMerchantShop::where('sub_merchant_user_id', $submerchantUser->id)
            ->join('shops', 'shops.user_id', 'sub_merchant_shops.shop_user_id')
            ->join('users', 'users.id', 'shops.user_id')
            ->select(['users.user_id AS uuid', 'users.email', 'users.name', 'users.phone_no', 'shops.*'])
            ->get();

        if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {
            $merchants = null;
        } else {
            $merchants = $this->merchantQuery()->get();
        }

        return view('merchants.submerchants.create', compact('submerchantUser', 'merchantUser', 'merchants', 'merchant_shops', 'submerchant_shops'));
    }

    public function destroy(Request $request)
    {
        try {

            $user = User::where('user_id', $request->user)
                ->firstOrFail();

            $merchant = SubMerchant::where('sub_merchants.user_id', $user->id)
                ->join('users', 'users.id', 'sub_merchants.merchant_id')
                ->select(['users.*'])
                ->firstOrFail();

            // TODO :: Delete merchant & submerchant linking

            $user->delete();

            $this->successMsg(__('dict.suc_del_submer'));
            return $this->json(true, '', action('MerchantController@viewMerchant', $merchant->user_id));

        } catch (Exception $ex) {

            return $this->json(false, __('dict.un_del_submer'));

        }
    }

    public function update($submerchantUser, Request $request)
    {
        try {

            $submerchantUser = User::where('user_id', $submerchantUser)->firstOrFail();
            $submerchantUser->name = strtoupper($request->name);
            $submerchantUser->username = $request->username;
            $submerchantUser->email = $request->email;
            $submerchantUser->phone_no = $request->phone_no;
            $submerchantUser->save();

            // Remove submerchant shop list
            if ($request->merchant_shop) {
                SubMerchantShop::where('sub_merchant_user_id', $submerchantUser->id)
                    ->whereNotIn('shop_user_id', $request->merchant_shop)
                    ->delete();

                // Add new shop into submerchant shop list
                foreach ($request->merchant_shop as $m_shop) {
                    SubMerchantShop::updateOrCreate(
                        [
                            'sub_merchant_user_id' => $submerchantUser->id,
                            'shop_user_id' => $m_shop,
                        ],
                        [
                            'shop_user_id' => $m_shop,
                        ]);
                }

            } else {
                SubMerchantShop::where('sub_merchant_user_id', $submerchantUser->id)
                    ->delete();
            }

            $this->successMsg(__('dict.suc_up_sub_det'));
            return redirect::back();

        } catch (Exception $ex) {

            $this->errorMsg(__('dict.un_up_sub_det'), '', $ex->getMessage());
            return redirect::back();

        }
    }

    public function updateStatus(Request $request)
    {
        try {

            $user = User::where('user_id', $request->user)->firstOrFail();

            $newStatus = $user->status == Enum::STATUS_ACTIVE ? Enum::STATUS_INACTIVE : Enum::STATUS_ACTIVE;

            $user->status = $newStatus;
            $user->save();

            $successMsg = __('dict.success_update') . $user->name . __('dict.sub_mer_stat');

            $this->successMsg($successMsg);
            return $this->json(true, $successMsg);

        } catch (Exception $ex) {

            return $this->json(false, __('dict.unable_to') . $newStatus . __('dict.m_sub_merchant'), $ex->getMessage());

        }
    }
}
