<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMerchant extends Model
{
    public function merchant()
    {
        return $this->hasOne('App\User', 'id', 'merchant_id');
    }
}
