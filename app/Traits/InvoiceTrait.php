<?php

namespace App\Traits;

use Session;

trait InvoiceTrait
{

    public function calculateChargableAmount($rate, $amount)
    {
        return $amount * $rate / 100;
    }

}