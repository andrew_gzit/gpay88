<?php

namespace App\Traits;

use App\Enum;
use App\Shop;
use App\SubMerchantShop;
use App\User;
use Illuminate\Support\Facades\Auth;

trait ShopTrait
{
    public function shopQuery()
    {
        if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN) || Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {
            return User::join('shops', 'users.id', 'shops.user_id')
                ->select(['users.user_id AS uuid', 'users.email', 'users.name', 'users.phone_no', 'shops.*']);

        } else if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {
            return User::join('shops', 'users.id', 'shops.user_id')
                ->where('merchant_id', Auth::id())
                ->select(['users.user_id AS uuid', 'users.email', 'users.name', 'users.phone_no', 'shops.*']);

        } else if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {

            // Get submerchant linked shop
            $smShopsArr = SubMerchantShop::where('sub_merchant_user_id', Auth::id())->get()->pluck('shop_user_id');

            return User::join('shops', 'users.id', 'shops.user_id')
                ->whereIn('users.id', $smShopsArr)
                ->select(['users.user_id AS uuid', 'users.email', 'users.name', 'users.phone_no', 'shops.*']);

        } else if (Auth::user()->hasRole(Enum::ROLE_SHOP)) {
            return User::where('users.id', Auth::id())
                ->join('shops', 'users.id', 'shops.user_id')
                ->select(['users.user_id AS uuid', 'users.email', 'users.name', 'users.phone_no', 'shops.*']);
        }
    }

    public function merchantQuery()
    {
        if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {
            return User::role(Enum::ROLE_MERCHANT)
                ->join('merchants', 'users.id', 'merchants.user_id')
                ->select(['users.*', 'users.user_id AS uuid', 'merchants.processing_fee']);

        } else if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {
            return User::where('users.id', Auth::id())
                ->join('merchants', 'users.id', 'merchants.user_id')
                ->select(['users.*', 'users.user_id AS uuid', 'merchants.processing_fee']);

        } else if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {
            return User::where('users.id', Auth::user()->getMerchant()->id)
                ->join('merchants', 'users.id', 'merchants.user_id')
                ->select(['users.*', 'users.user_id AS uuid', 'merchants.processing_fee']);

        } else if (Auth::user()->hasRole(Enum::ROLE_SHOP)) {
            // Retrieve merchant user ID
            $shop = Shop::where('user_id', Auth::id())->firstOrFail();

            return User::where('users.id', $shop->merchant_id)
                ->join('merchants', 'users.id', 'merchants.user_id')
                ->select(['users.*', 'users.user_id AS uuid', 'merchants.processing_fee']);
        }
    }

    /**
     * Check if logged in user is authorized to access shop
     * Must be shop ID NOT shop user ID
     */
    public function authorizedShop($shopId)
    {
        if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {

            return true;

        } else if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {

            return Shop::where('merchant_id', Auth::id())
                ->where('id', $shopId)
                ->count();

        } else if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {

            return Shop::where('merchant_id', Auth::user()->getMerchant()->id)
                ->where('id', $shopId)
                ->count();

        } else if (Auth::user()->hasRole(Enum::ROLE_SHOP)) {

            return Shop::where('user_id', Auth::id())
                ->where('id', $shopId)
                ->count();

        }
    }

    public function constructMerchantName($merchant, $viewableOnly)
    {
        if ($viewableOnly) {
            return '<span>' . $merchant->name . '</span>';
        } else {
            return '<a href="/merchants/view/' . $merchant->user_id . '">' . $merchant->name . '</a>';
        }
    }

}
