<?php

namespace App\Traits;

use Session;

trait ResponseTrait
{
    public function json($success, $message = null, $data = null)
    {
        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function jsonError()
    {
        return response()->json([

        ], 400);
    }

    public function successMsg($msg)
    {
        Session::flash('css', 'success');
        Session::flash('msg', $msg);
    }

    public function errorMsg($msg)
    {
        Session::flash('css', 'error');
        Session::flash('msg', $msg);
    }
}
