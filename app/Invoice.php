<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Invoice;

class Invoice extends Model
{
    public function generateInvoiceNo()
    {
        $invoiceNo = Enum::INVOICE_PREFIX;

        do {
            $invoiceNo .= rand(100000, 999999);
            $isUsed = Invoice::where('invoice_no', $invoiceNo)->get();
        } while (count($isUsed) != 0);

        return $invoiceNo; 
    }

    public function details()
    {
        return $this->hasMany('App\InvoiceDetail', 'invoice_id', 'id');
    }
}
