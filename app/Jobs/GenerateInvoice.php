<?php

namespace App\Jobs;

use App\Enum;
use App\Invoice;
use App\InvoiceDetail;
use App\Merchant;
use App\Traits\InvoiceTrait;
use App\Transaction;
use Carbon\Carbon;
use DB;
use Exception;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, InvoiceTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {

            DB::transaction(function () {

                $now = Carbon::now();
                $lastMonth = Carbon::now()->subMonth();
                $nextPaymentDue = Carbon::now()->addMonth();

                // Get all merchants that is due today
                $merchants = Merchant::whereDate('next_invoice_date', $now)
                    ->get();

                foreach ($merchants as $m) {

                    $invoiceItems = [];

                    // Get all transaction for past month
                    $txns = Transaction::whereMonth('created_at', $lastMonth->month)
                        ->whereYear('created_at', $lastMonth->year)
                        ->where('status', Enum::TX_SUCCESS['key'])
                        ->whereIn('shop_id', $m->shops()->pluck('id'))
                        ->get();

                    $shopsTxns = $txns->groupBy('shop_id');

                    $invoice = new Invoice();
                    $invoice->invoice_no = $invoice->generateInvoiceNo();
                    $invoice->merchant_user_id = $m->user_id;
                    $invoice->total_amount = 0;
                    $invoice->rate = $m->processing_fee;
                    $invoice->status = Enum::UNPAID;
                    $invoice->issue_date = $now;
                    $invoice->payment_due = $nextPaymentDue;
                    $invoice->save();

                    // Calculate 
                    foreach ($shopsTxns as $shop_id => $txns) {
                        $invoiceDet = new InvoiceDetail();
                        $invoiceDet->invoice_id = $invoice->id;
                        $invoiceDet->shop_user_id = $shop_id;
                        $invoiceDet->num_transactions = $txns->count();
                        $invoiceDet->chargable_amount = $this->calculateChargableAmount($m->processing_fee, $txns->sum('amount'));
                        $invoiceDet->amount = $txns->sum('amount');
                        $invoiceDet->created_at = $now;
                        $invoiceDet->updated_at = $now;

                        array_push($invoiceItems, $invoiceDet->toArray());
                    }

                    InvoiceDetail::insert($invoiceItems);

                    // Update total amount
                    $invoice->total_amount = array_reduce($invoiceItems, function($carry, $item){
                        return $carry + $item['chargable_amount'];
                    });

                    $invoice->save();

                    $invoicePdf = PDF::loadView('pdf.invoice', compact('invoice'));
                    $invoicePdf->save($invoice->invoice_no . '-invoice.pdf');
                }
            });

        } catch (Exception $ex) {

            dd($ex);

        }

    }
}
