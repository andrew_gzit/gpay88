<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enum extends Model
{
    const ROLE_SUPER_ADMIN = 'Super Admin';
    const ROLE_ADMIN = 'Admin';
    const ROLE_MERCHANT = 'Merchant';
    const ROLE_SUBMERCHANT = 'Sub Merchant';
    const ROLE_SHOP = 'Shop';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = -1;
    const STATUS_SUSPENDED = -2;

    const TX_PENDING = ['key' => 0, 'value' => 'Pending'];
    const TX_SUCCESS =  ['key' => 1, 'value' => 'Success'];
    const TX_REJECTED = ['key' => 2, 'value' => 'Failed'];

    const MAX_THRESHOLD = 9999999;

    const INVOICE_PREFIX = 'GP-';

    const UNPAID = 0;
    const PAID = 1;
}
