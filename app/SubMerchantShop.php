<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMerchantShop extends Model
{
    protected $fillable = [
        'sub_merchant_user_id',
        'shop_user_id'
    ];
}
