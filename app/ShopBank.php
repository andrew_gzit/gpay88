<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Utility;

class ShopBank extends Model
{
    use SoftDeletes;

    public function shopBankActions()
    {
        if($this->status == Enum::STATUS_INACTIVE){
            $action = 'Activate';
        } else {
            $action = 'Deactivate';
        }

        return '<a href="javascript:;" data-toggle="dropdown" class="btn btn-default dropdown-toggle"></a>' .
        '<ul class="dropdown-menu" data-id="' . $this->id . '">' .
        '<li><a href="javascript:;" class="btn-edit-bank">' . __('dict.edit_bank') . '</a></li>' .
        '<li><a href="javascript:;" class="btn-delete-bank">' . __('dict.delete_bank') . '</a></li>' .
        '</ul>';
    }

    public function toggleHtml()
    {
        $curStateStr = $this->status == Enum::STATUS_ACTIVE ? __('dict.active') : __('dict.inactive');
        $state = $this->status == Enum::STATUS_ACTIVE ? 'checked' : '';
        $action = $this->status == Enum::STATUS_ACTIVE ? __('dict.deactivate') : __('dict.activate');

        // Only allow toggle for user with permission
        if(Auth::user()->can(RoleEnum::BANK_DISABLE)){
            $html = '<input data-shopbank="' . $this->id . '" class="btn-update-status" data-action="' . $action . '" type="checkbox" ' . $state . ' data-onstyle="brand" data-offstyle="warning" data-toggle="toggle" data-on="' . __('dict.active') . '" data-off="' . __('dict.inactive') . '" data-size="sm">';
        } else {
            $html = Utility::statusToHtmL($this->status);
        }

        return $html;
    }
}
