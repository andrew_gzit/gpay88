<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    public function shopUser()
    {
        return $this->hasOne('App\User', 'id', 'shop_user_id');
    }

    public function shop()
    {
        return $this->hasOne('App\Shop', 'user_id', 'shop_user_id');
    }


}
