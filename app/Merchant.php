<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    public function shops()
    {
        return $this->user->getMerchantShop;
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
