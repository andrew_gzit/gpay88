<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Shop;

class Shop extends Model
{
    use SoftDeletes;

    const PREFIX = 'GP88';

    public static function generateAPICode()
    {
        
        do {
            $code = Shop::PREFIX . Carbon::now()->format('y') . str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT);
            $exist = Shop::where('api_code', $code)->first();
        } while($exist);

        return $code;

    }

    public static function generateAPISecret()
    {

        do {
            $secret = Str::random(20);
            $exist = Shop::where('api_secret', $secret)->first();
        } while($exist);

        return $secret;

    }
}
