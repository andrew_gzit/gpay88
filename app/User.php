<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function shop()
    {
        return $this->hasOne('App\Shop', 'user_id', 'id');
    }

    public function assignPermission($role)
    {
        // Give default permissions
        switch ($role) {
            case Enum::ROLE_MERCHANT:
                $this->givePermissionTo(RoleEnum::MERCHANT_PERMISSIONS);
                break;
            case Enum::ROLE_SUBMERCHANT:
                $this->givePermissionTo(RoleEnum::SUBMERCHANT_PERMISSIONS);
                break;
            case Enum::ROLE_SHOP:
                $this->givePermissionTo(RoleEnum::SHOP_PERMISSIONS);
                break;
        }
    }

    public function getMerchantName()
    {
        // Get merchant name as shop
        if($this->hasRole(Enum::ROLE_SHOP)){
            $shop = Shop::where('user_id', $this->id)->firstOrFail();
            return User::where('id', $shop->merchant_id)->firstOrFail()->name;
        } else {
            return '-';
        }
    }

    public function merchantProfile()
    {
        return $this->hasOne('App\Merchant', 'user_id', 'id');
    }

    public function getMerchant()
    {
        if($this->hasRole(Enum::ROLE_SHOP)){

        } else if($this->hasRole(Enum::ROLE_SUBMERCHANT)) {

            $sm = SubMerchant::where('user_id', $this->id)->firstOrFail();
            return $sm->merchant;

        }
    }

    public function getMerchantShop()
    {
        return $this->hasMany('App\Shop', 'merchant_id', 'id');
    }
}
