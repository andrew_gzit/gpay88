<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleEnum extends Model
{
    const SHOP_VIEW = 'shop_view';
    const SHOP_CREATE ='shop_create';
    const SHOP_EDIT = 'shop_edit';
    const SHOP_DISABLE = 'shop_disable';
    const SHOP_DELETE = 'shop_delete';

    const BANK_VIEW = 'bank_view';
    const BANK_CREATE = 'bank_create';
    const BANK_EDIT = 'bank_edit';
    const BANK_DISABLE = 'bank_disable';
    const BANK_DELETE = 'bank_delete';

    const MERCHANT_VIEW = 'merchant_view';
    const MERCHANT_CREATE = 'merchant_create';
    const MERCHANT_EDIT = 'merchant_edit';
    const MERCHANT_DISABLE = 'merchant_disable';
    const MERCHANT_DELETE = 'merchant_delete';

    const SUBMERCHANT_VIEW = 'submerchant_view';
    const SUBMERCHANT_CREATE = 'submerchant_create';
    const SUBMERCHANT_EDIT = 'submerchant_edit';
    const SUBMERCHANT_DISABLE = 'submerchant_disable';
    const SUBMERCHANT_DELETE = 'submerchant_delete';

    const TRANSACTION_VIEW = 'transaction_view';

    // List of merchant permissions
    const MERCHANT_PERMISSIONS = [
        RoleEnum::SHOP_VIEW,
        RoleEnum::SHOP_EDIT,
        RoleEnum::SHOP_DISABLE,
        RoleEnum::SHOP_DELETE,

        RoleEnum::MERCHANT_VIEW,
        RoleEnum::MERCHANT_EDIT,

        RoleEnum::BANK_VIEW,
        RoleEnum::BANK_CREATE,
        RoleEnum::BANK_EDIT,
        RoleEnum::BANK_DISABLE,
        RoleEnum::BANK_DELETE,

        RoleEnum::TRANSACTION_VIEW,

        RoleEnum::SUBMERCHANT_VIEW,
        RoleEnum::SUBMERCHANT_CREATE,
        RoleEnum::SUBMERCHANT_EDIT,
        RoleEnum::SUBMERCHANT_DISABLE,
        RoleEnum::SUBMERCHANT_DELETE,
    ];

    // List of sub merchant permissions
    const SUBMERCHANT_PERMISSIONS = [
        RoleEnum::MERCHANT_VIEW,
        RoleEnum::SHOP_VIEW,
        RoleEnum::TRANSACTION_VIEW
    ];

    // List of shop permissions 
    const SHOP_PERMISSIONS = [
        RoleEnum::MERCHANT_VIEW,

        RoleEnum::SHOP_VIEW,
        RoleEnum::SHOP_EDIT,

        RoleEnum::BANK_VIEW,
        RoleEnum::BANK_DISABLE,

        RoleEnum::TRANSACTION_VIEW
    ];
}
