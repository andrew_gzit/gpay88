<?php

namespace App;

use App\Enum;
use App\RoleEnum;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Utility extends Model
{

    public static function generateUniqueId()
    {
        return (string) Str::orderedUuid();
    }

    public static function statusToHtmL($status)
    {
        $html = '<span class="badge %s w-100">%s</span>';
        $statusStr = '';
        $statusCss = '';

        switch ($status) {
            case Enum::STATUS_ACTIVE:
                $statusStr = __('dict.active');
                $statusCss = 'badge-brand';
                break;
            case Enum::STATUS_INACTIVE:
                $statusStr = __('dict.inactive');
                $statusCss = 'badge-warning';
                break;
            case Enum::STATUS_SUSPENDED:
                $statusStr = 'SUSPENDED';
                $statusCss = 'badge-danger';
                break;
        }

        return sprintf($html, $statusCss, $statusStr);
    }

    public static function getShopActions($shopUuid, $shopStatus)
    {
        $newStatus = $shopStatus == Enum::STATUS_ACTIVE ? __('dict.deactivate') : __('dict.activate');

        if (Auth::user()->hasRole('Super Admin') || Auth::user()->can(RoleEnum::SHOP_DISABLE)) {
            return '<a href="javascript:;" data-toggle="dropdown" class="btn btn-default dropdown-toggle"></a>' .
            '<ul class="dropdown-menu shop-action-menu" data-shop="' . $shopUuid . '">' .
            '<li><a href="javascript:;" class="btn-disable-shop" data-action=' . $newStatus . '>' . $newStatus . __("dict.shop") . '</a></li>' .
            '</ul>';
        }
    }

    public static function transactionStatusToHtmL($status)
    {
        $html = '<span class="badge %s w-100 text-uppercase">%s</span>';
        $statusStr = '';
        $statusCss = '';

        switch ($status) {
            case Enum::TX_SUCCESS['key']:
                $statusStr = Enum::TX_SUCCESS['value'];
                $statusCss = 'badge-brand';
                break;
            case Enum::TX_PENDING['key']:
                $statusStr = Enum::TX_PENDING['value'];
                $statusCss = 'badge-default';
                break;
            case Enum::TX_REJECTED['key']:
                $statusStr = Enum::TX_REJECTED['value'];
                $statusCss = 'badge-danger';
                break;
        }

        return sprintf($html, $statusCss, $statusStr);
    }

    public static function merchantActions($merchant)
    {
        $newStatus = $merchant->status == Enum::STATUS_ACTIVE ? __('dict.deactivate') : __('dict.activate');

        $actions = collect();

        if(Auth::user()->can(RoleEnum::MERCHANT_DISABLE)){
            $actions->push('<li><a href="javascript:;" data-action="' . $newStatus . '" data-id="' . $merchant->user_id . '" class="btn-update-status">' . $newStatus . __("dict.merchant") . '</a></li>');
        }

        if(count($actions) > 0){
            return '<a href="javascript:;" data-toggle="dropdown" class="btn btn-default dropdown-toggle"></a>' .
            '<ul class="dropdown-menu">' .
            $actions->join('') .
            '</ul>';
        } else {
            return '';
        }
    }

    public static function subMerchantActions($submerchantId, $status)
    {
        $newStatus = $status == Enum::STATUS_ACTIVE ? __('dict.deactivate') : __('dict.activate');

        $actions = collect();
        $actions->push('<li><a href="javascript:;" data-action="' . $newStatus . '" data-submerchant='. $submerchantId . ' class="btn-submerchant-update-status">' . $newStatus . __("dict.btn_sub_merchant") . '</a></li>');

        return '<a href="javascript:;" data-toggle="dropdown" class="btn btn-default dropdown-toggle"></a>' .
        '<ul class="dropdown-menu submerchant-action-menu" data-submerchant="' . $submerchantId . '">' .
        $actions->join('') .
        '</ul>';
    }

    public static function getMerchantFilter()
    {
        if (Auth::user()->hasRole(Enum::ROLE_SUPER_ADMIN)) {
            return User::role(Enum::ROLE_MERCHANT)->get();
        } else if (Auth::user()->hasRole(Enum::ROLE_ADMIN)) {
            return User::role(Enum::ROLE_MERCHANT)->get();
        } else if (Auth::user()->hasRole(Enum::ROLE_MERCHANT)) {
            return User::where('id', Auth::id())->get();
        } else if (Auth::user()->hasRole(Enum::ROLE_SUBMERCHANT)) {
            return User::where('id', Auth::user()->getMerchant()->id)->get();
        } else {

            // Retrieve merchant user ID
            $shop = Shop::where('user_id', Auth::id())->firstOrFail();

            return User::where('id', $shop->merchant_id);

        }
    }
}
