<?php

return [
    
    'general' => '选单',

    //Home
    'home' => '主页',
    'shop' => '商店',
    'merchant' => '商家',
    'total_volume' => '总数额',
    'total_transactions' => '总交易',
    'volume' => '数额',
    'summary_report' => '总结报告',
    'bank_status' => '银行状态',
    'log_out' => '登出',
    'today_volume' => '今天数额',

    //Merchants
    'merchants' => '商家',
    'name' => '名字',
    'email_add' => '电子邮件地址',
    'phone_num' => '电话号码',
    'status' => '状态',
    'actions' => '行动',
    'create' => '创建',
    'selected_merchant' => '选定的商人',
    'merchant_details' => '商家资料',
    'settings' => '设定',

    //Create Merchant
    'create_merchant' => '创建商家',
    'phone_no' => '电话号码',
    'username' => '用户名',
    'password' => '密码',
    'c_pw' => '确认密码',
    'save' => '保存',

    //Merchant Detail
    'detail' => ' 详情',
    'email' => '电子邮件',
    'edit_details' => '编辑详细资料',
    'reset_pw' => '重设密码',
    'chg_pw' => '更改密码',
    'sub_merchants' => '子商家',
    'shop_name' => '店铺名称',
    'delete_merchant' => '删除商家',
    'confirm_del_merchant' => '确认删除商家',
    'processing_fee' => '手续费',

    //Create Sub Merchant
    'create_sub_merchant' => '创建子商家',
    'edit_sub_merchant' => '编辑子商家',
    'sub_merchant_details' => '子商家详细资料',
    'submit' => '提交',
    'del_sub_merchant' => '删除子商家',
    'con_del_sub_merchant' => '确认删除子商家',
    'selected_submerchant' => '选定的子商家',
    'btn_sub_merchant' => '子商家',

    //Edit Merchant Details
    'edit_merchant_details' => '编辑商家详细资料',
    'update' => '更新',
    'close' => '关闭',

    //Change Password
    'old_pw' => '旧密码',
    'new_pw' => '新密码',
    'c_new_pw' => '确认新密码',

    //Shops
    'shops' => '商店',
    'shop_id' => '店铺编号',
    'callback_url' => '回调网址',
    'response_url' => '回应网址',
    'api_code' => 'API代码',
    'api_secret' => 'API秘密',
    'deactivate' => '停用 ',
    'activate' => '启用 ',
    'confirm' => '确认',
    'cancel' => '取消',
    'selected_shop' => '选定的商店',

    //Create Shop
    'create_shop' => '创建店铺',
    'shop_details' => '店铺详情',
    'shop_name' => '店铺名称',
    'del_shop' => '删除店铺',
    'c_del_shop' => '确认删除店铺',

    //Edit Shop
    'edit_shop' => '编辑店铺',
    //Integration Details
    'integration_details' => '整合细节',
    //Bank Details
    'bank_details' => '银行明细',
    'bank_name' => '银行名称',
    'bank_acc_no' => '银行帐号',
    'bank_holder_name' => '银行持有人姓名',
    'min_threshold' => '最低阈值',
    'max_threshold' => '最大阈值',
    'accumulated_threshold' => '累计阈值',
    'add_bank' => '加银行',
    'bank_abbr' => '银行简称',
    'action' => '行动',
    'edit_bank' => '编辑银行',
    'delete_bank' => '删除银行',
    'edit_bank_info' => '编辑银行信息 - ',
    'active' => '活跃',
    'inactive' => '不活跃',
    'save_changes' => '保存更改',

    //Transaction Reports
    'trans_report' => '交易',
    'reports' => '报告',
    'transactions' => '交易',
    'date' => '日期',
    'till' => '至',
    'filter' => '过滤',
    'reset' => '重启',
    'transaction_id' => '交易编号',
    'ref_no' => '参考编号',
    'ref_detail' => '参考详情',
    'time' => '时间',
    'amount' => '数量',
    'total' => '总数',
    'remarks' => '备注',
    'bank' => '银行',
    'banks' => '银行',
    'statement' => '商店对帐单',

    //Invoice
    'billing' => '账单',
    'invoices' => '开票',

    //Messages
    //Login
    'un_logout' => '无法登出！',
    'un_login' => '无法登录。',
    'acc_suspended' => '您的帐户已被暂停',
    'success_update' => '成功更新',
    //Merchant
    'm_status' => '状态',
    'suc_up_merchant_det' => '成功更新商家详细资料。',
    'suc_res_pw' => '成功重置商户密码。',
    'suc_del' => '成功删除',
    'un_res_mer_pw' => '无法重设商家密码',
    'un_up_mer_stat' => '无法更新商家状态',
    'suc_created' => '成功创建',
    'm_merchant' => '商家',
    'un_del_mer' => '无法删除商家',
    'un_del_mer_del' => '无法删除商家。删除',
    'shop_attac' => '附属于此商家的商店继续。',
    'mer_not_found' => '找不到商家！',
    'un_view_mer' => '无法查看商家！',
    'un_up_mer_det' => '无法更新商家详细信息',
    'email_used' => '电子邮件已被使用。尝试另一封电子邮件。',
    //Shop
    'm_shop' => '店铺。',
    'unauthorized' => '未经授权！',
    'un_res_pw' => '无法重设密码。',
    'un_chg_pw' => '无法更改密码.',
    'suc_up_pw' => '成功更新密码。',
    'in_pw' => '不正确的旧密码。请再试一遍。',
    'suc_shop' => '成功创建商店。',
    'suc_up_shop_det' => '已成功更新商店详细信息。',
    'un_retrieve_bank' => '无法检索所选商店的银行。',
    'un_retrieve_widget' => '无法检索小部件的详细信息。',
    'shop_status' => '店铺状态',
    'un_dis_shop' => '无法停用商店',
    'un_edit_shop' => '无法编辑商店！',
    'confirm_del_shop'=>'确定删除商店？',
    //Reports
    'un_report' => '无法检索摘要报告。',
    'un_val_details' => '无法验证详细信息',
    //Sub Merchant
    'm_sub_mer' => '子商家',
    'suc_del_submer' => '成功删除子商家',
    'un_del_submer' => '无法删除子商家',
    'suc_up_sub_det' => '成功更新了子商家详细信息',
    'un_up_sub_det' => '无法更新子商家详细信息。',
    'sub_mer_stat' => '子商家状态',
    'unable_to' => '无法',
    'm_sub_merchant' => '子商家',
    //Bank Details
    'successfully' => '成功地',
    'suc_bank_det' => '成功更新了银行详细信息。',
    'un_edit_bank_det' => '无法编辑银行详细信息。',
    'bank_added' => '所选银行已添加！',
    'suc_add_bank' => '成功添加银行',
    'bank_not_add' => '未添加银行',
    'bank_del' => '银行已成功删除。',
    'bank_not_found' => '找不到选定的银行！请再试一遍。',

    'success_update_bank_status' => '成功更新银行信息',
    'unsuccessful_update_bank_status' => '失败更新银行信息',
];