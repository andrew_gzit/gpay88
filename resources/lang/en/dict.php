<?php

return [

    'general' => 'General',

    //Home
    'home' => 'Home',
    'merchant' => 'Merchant',
    'shop' => 'Shop',
    'total_volume' => 'Total Volume',
    'total_transactions' => 'Total Transactions',
    'volume' => 'Volume',
    'summary_report' => 'Summary Report',
    'bank_status' => 'Bank Status',
    'log_out' => 'Log Out',
    'today_volume' => 'Today\'s Volume',

    //Merchants
    'merchants' => 'Merchants',
    'name' => 'Name',
    'email_add' => 'Email Address',
    'phone_num' => 'Phone Number',
    'status' => 'Status',
    'actions' => 'Actions',
    'create' => 'Create',
    'selected_merchant' => 'selected merchant',
    'merchant_details' => 'Merchant Details',
    'settings' => 'Settings',

    //Create Merchant
    'create_merchant' => 'Create Merchant',
    'phone_no' => 'Phone No.',
    'username' => 'Username',
    'password' => 'Password',
    'c_pw' => 'Confirm Password',
    'save' => 'Save',

    //Merchant Detail
    'detail' => ' Detail',
    'email' => 'Email',
    'edit_details' => 'Edit Details',
    'reset_pw' => 'Reset Password',
    'chg_pw' => 'Change Password',
    'sub_merchants' => 'Sub Merchants',
    'shop_name' => 'Shop Name',
    'delete_merchant' => 'Delete Merchant',
    'confirm_del_merchant' => 'Confirm delete merchant',
    'processing_fee' => 'Processing Fee',

    //Create Sub Merchant
    'create_sub_merchant' => 'Create Sub Merchant',
    'edit_sub_merchant' => 'Edit Sub Merchant',
    'sub_merchant_details' => 'Sub Merchant Details',
    'submit' => 'Submit',
    'del_sub_merchant' => 'Delete Sub Merchant',
    'con_del_sub_merchant' => 'Confirm delete sub merchant',
    'selected_submerchant' => 'Sub Merchant',
    'btn_sub_merchant' => ' Sub Merchant',

    //Edit Merchant Details
    'edit_merchant_details' => 'Edit Merchant Details',
    'update' => 'Update',
    'close' => 'Close',

    //Change Password
    'old_pw' => 'Old Password',
    'new_pw' => 'New Password',
    'c_new_pw' => 'Confirm New Password',

    //Shops
    'shops' => 'Shops',
    'shop_id' => 'Shop ID',
    'callback_url' => 'Callback URL',
    'response_url' => 'Response URL',
    'api_code' => 'API Code',
    'api_secret' => 'API Secret',
    'deactivate' => 'Deactivate ',
    'activate' => 'Activate ',
    'confirm' => 'Confirm',
    'cancel' => 'Cancel',
    'selected_shop' => 'selected shop',

    //Create Shop
    'create_shop' => 'Create Shop',
    'shop_details' => 'Shop Details',
    'shop_name' => 'Shop Name',
    'del_shop' => 'Delete Shop',
    'c_del_shop' => 'Confirm delete shop',

    //Edit Shop
    'edit_shop' => 'Edit Shop',
    //Integration Details
    'integration_details' => 'Integration Details',
    //Bank Details
    'bank_details' => 'Bank Details',
    'bank_name' => 'Bank Name',
    'bank_acc_no' => 'Bank Account No.',
    'bank_holder_name' => 'Bank Holder Name',
    'min_threshold' => 'Min Threshold',
    'max_threshold' => 'Max Threshold',
    'accumulated_threshold' => 'Accumulated Threshold',
    'add_bank' => 'Add Bank',
    'bank_abbr' => 'Bank Abbr.',
    'action' => 'Action',
    'edit_bank' => 'Edit Bank',
    'delete_bank' => 'Delete Bank',
    'edit_bank_info' => 'Edit Bank Info - ',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'save_changes' => 'Save Changes',
    
    //Transaction Reports
    'trans_report' => 'Transactions',
    'reports' => 'Reports',
    'transactions' => 'Transactions',
    'date' => 'Date',
    'till' => 'till',
    'filter' => 'Filter',
    'reset' => 'Reset',
    'transaction_id' => 'Transaction ID',
    'ref_no' => 'Ref. No',
    'ref_detail' => 'Ref. Detail',
    'time' => 'Time',
    'amount' => 'Amount',
    'total' => 'Total',
    'remarks' => 'Remarks',
    'bank' => 'Bank',
    'banks' => 'Banks',
    'statement' => 'Statement',

    //Invoice
    'billing' => 'Billing',
    'invoices' => 'Invoices',

    //Messages
    //Login
    'un_logout' => 'Unable to logout!',
    'un_login' => 'Unable to login.',
    'acc_suspended' => 'Your account has been suspended',
    'success_update' => 'Successfully updated ',

    //Merchant
    'm_status' => ' status',
    'suc_up_merchant_det' => 'Successfully updated merchants details.',
    'suc_res_pw' => 'Successfully reset password.',
    'suc_del' => 'Successfully deleted ',
    'un_res_mer_pw' => 'Unable to reset merchant password',
    'un_up_mer_stat' => 'Unable to update merchant status',
    'suc_created' => 'Successfully created ',
    'm_merchant' => ' merchant',
    'un_del_mer' => 'Unable to delete merchant',
    'un_del_mer_del' => 'Unable to delete merchant. Delete ',
    'shop_attac' => ' shops attached to this merchant to continue.',
    'mer_not_found' => 'Merchant not found!',
    'un_view_mer' => 'Unable to view merchant!',
    'un_up_mer_det' => 'Unable to update merchant details',
    'email_used' => 'Email has been used. Try another email.',

    //Shop
    'm_shop' => ' shop.',
    'unauthorized' => 'Unauthorized!',
    'un_res_pw' => 'Unable to reset password.',
    'un_chg_pw' => 'Unable to change password.',
    'suc_up_pw' => 'Successfully updated password.',
    'in_pw' => 'Incorrect old password. Please try again.',
    'suc_shop' => 'Successfully created shop.',
    'suc_up_shop_det' => 'Successfully updated shop details.',
    'un_retrieve_bank' => 'Unable to retrieve bank of selected shop.',
    'un_retrieve_widget' => 'Unable to retrieve details of widget.',
    'shop_status' => ' shop status',
    'un_dis_shop' => 'Unable to disable shop',
    'un_edit_shop' => 'Unable to edit shop!',
    'confirm_del_shop'=>'Confirm delete shop?',

    //Reports
    'un_report' => 'Unable to retrieve summary report.',
    'un_val_details' => 'Unable to validate details',

    //Sub Merchant
    'm_sub_mer' => ' submerchant',
    'suc_del_submer' => 'Successfully deleted sub merchant',
    'un_del_submer' => 'Unable to delete sub merchant',
    'suc_up_sub_det' => 'Successfully updated sub merchant details',
    'un_up_sub_det' => 'Unable to update sub merchant details.',
    'sub_mer_stat' => ' sub merchant status',
    'unable_to' => 'Unable to ',
    'm_sub_merchant' => ' sub merchant',

    //Bank Details
    'successfully' => 'Successfully ',
    'suc_bank_det' => 'Successfully updated bank details.',
    'un_edit_bank_det' => 'Unable to edit bank details.',
    'bank_added' => 'Selected bank has already been added!',
    'suc_add_bank' => 'Successfully added bank',
    'bank_not_add' => 'Bank not added',
    'bank_del' => 'Bank successfully deleted.',
    'bank_not_found' => 'Selected bank cannot be found! Please try again.',

    'success_update_bank_status' => 'Successfully updated bank status',
    'unsuccessful_update_bank_status' => 'Unsuccessful updated bank status',
];