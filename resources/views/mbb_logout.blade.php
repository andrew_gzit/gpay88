@extends('mbb_layout')

@section('content')
<div class="common_box white-bg-box">
    <div class='text-center'>
        <h3>You have logged out</h3>
    </div>
    <div id="loginMain" class="common_box w-loginmain">
        <div class="p-login">
            <h3 class="font-size-subhead">Thank you fo banking online with Maybank2u.com</h3>
            <blockquote class="note mx-0 rounded bg-white-05 font-size-dec">
                <strong>Note:</strong><br>
                As an added security measure, please clear your cache after each session.
            </blockquote>
        </div>
    </div>
    <p class="text-center">
        <input type="submit" name="action" value="Return to FPX" class="btn btn-primary">
    </p>
</div>
@stop
