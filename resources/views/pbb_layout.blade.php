<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PBe</title>

    <link rel="stylesheet" href="/bootstrap.css">
    <link rel="stylesheet" href="/pbb.css">
    <link rel="stylesheet" href="/pbb-responsive.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="/pbb_metronic.css">
</head>

<body>

    <div id="Layer4a">
        <div id="header">
            <!-- BEGIN HEADER -->
            <div class="header navbar navbar-inverse">
                <!-- BEGIN TOP NAVIGATION BAR -->
                <div class="header-inner">
                    <!-- BEGIN LOGO -->
                    <a class="navbar-brand" id="bank-logo-href" href="#"
                        onclick="parent.window.location='www.pbebank.com'">
                        <img src="/pb_logo.png" alt="logo" id="bank-logo" class="img-responsive">
                    </a>
                    <!-- END LOGO -->

                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN PEB LOGO -->
                        <li class="dropdown">
                            <a href="#" style="cursor:default">

                                <img alt="FPX" id="fpx-logo" src="/fpxLogo.png" width="85%">

                            </a>
                        </li>
                        <!-- END PBE LOGO -->
                    </ul>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END TOP NAVIGATION BAR -->
            </div>
            <!-- END HEADER -->
        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-center">
                <div class="margin-top-20"></div>
                <div class="row">

                    <!-- BEGIN RIGHT -->
                    <div class="col-md-6">
                        <div class="portlet">
                            <div class="row">
                                <div class="col-md-12 margin-top-10">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet" style="z-index:-1; visibility: visible;">
                                        <!-- BEGIN LOGIN FORM -->
                                        <table width="100%" class="table-iframe">
                                            <tbody>
                                                <tr>

                                                    <td width="70%" class="first-td-iframe">
                                                        @yield('content')
                                                    </td>

                                                    <td style="background-color:#f3f2f2; padding-left:15px;"
                                                        class="second-td-iframe">

                                                        <style type="text/css">
                                                            .dl-horizontal dt {
                                                                float: left;
                                                                width: 9px;
                                                                overflow: hidden;
                                                                clear: left;
                                                                text-align: right;
                                                                text-overflow: ellipsis;
                                                                white-space: nowrap;
                                                                font-weight: normal;
                                                                vertical-align: top;
                                                                background-image: "arrow.png";
                                                                line-height: 1.8;
                                                            }

                                                            .dl-horizontal dd {
                                                                margin-left: 14px;
                                                                vertical-align: top;
                                                                line-height: 1.8;
                                                            }
                                                        </style>

                                                        <div class="margin-top-5">
                                                            <b>New to PBe?</b><br>
                                                            <dl class="dl-horizontal">
                                                                <dt><img src="/arrow.png"></dt>
                                                                <dd><a href="javascript:MM_openBrWindow(faqlink5,'Help','scrollbars=yes,width=600,height=480,left=50,top=20')"
                                                                        id="first-time-login"><u>First Time Login
                                                                            Guide</u></a></dd>
                                                                <dt><img src="/arrow.png"></dt>
                                                                <dd><a href="javascript:MM_openBrWindow(faqlink6,'Help','scrollbars=yes,width=600,height=480,left=50,top=20')"
                                                                        id="PBe-online-security"><u>PBe Online
                                                                            Security</u></a></dd>
                                                                <dt><img src="/arrow.png"></dt>
                                                                <dd><a href="javascript:MM_openBrWindow(faqlink3,'Help','scrollbars=yes,width=600,height=480,left=50,top=20')"
                                                                        id="faq"><u>FAQs</u></a></dd>
                                                                <dt><img src="/arrow.png"></dt>
                                                                <dd><a href="javascript:MM_openBrWindow(faqlink7 ,'Help','scrollbars=yes,width=700,height=480,left=50,top=20')"
                                                                        id="PBe-tutorials "><u>PBe Tutorials</u></a>
                                                                </dd>
                                                            </dl>

                                                            <b>Need Help?</b><br>

                                                            <dl class="dl-horizontal">
                                                                <dt><img src="/arrow.png"></dt>
                                                                <dd><a href="javascript:MM_openBrWindow(faqlink8,'Help','scrollbars=yes,width=600,height=480,left=50,top=20')"
                                                                        id="forgot-pass"><u>Forgot Password?</u></a>
                                                                </dd>
                                                                <dt><img src="/arrow.png"></dt>
                                                                <dd><a href="javascript:MM_openBrWindow(faqlink9 ,'Help','scrollbars=yes,width=700,height=480,left=50,top=20')"
                                                                        id="user-id-deactivated"><u>User ID
                                                                            Deactivated?</u></a></dd>
                                                            </dl>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- BEGIN PORTLET -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet">
                                        <div class="security">
                                            <h3> <span id="alert-icon"></span> <span id="alert-title"></span></h3>
                                            <span id="alert-description"><a
                                                    href="https://www.pbebank.com/Personal-Banking/Promo/login_newbanner01.aspx"
                                                    target="_blank"><img
                                                        src="https://www.pbebank.com/images/securedpromo/login_newbanner01.aspx"
                                                        class="img-responsive"></a></span>
                                        </div>
                                        <div class="margin-bottom-10"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                            <div class="clearfix"></div>
                        </div><!-- END PORTLET -->
                    </div><!-- END COL 6-->

                    <!-- BEGIN LEFT -->
                    <div class="col-md-6">
                        <div class="portlet">
                            <div class="margin-top-10"></div>
                            <!-- BEGIN TILES CONTENT-->
                            <div id="tiles-banner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="container-context">
                                            <div id="myCarousel" class="carousel image-carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="active item">
                                                        <a onclick="window.open('https://www.pbebank.com/Personal-Banking/Promo/login_newbanner02.aspx', '_blank'); return false;"
                                                            href="#"><img
                                                                src="https://www.pbebank.com/images/securedpromo/login_newbanner02.aspx"
                                                                class="img-responsive" alt=""></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="footer">
            <div class="footer">
                <div class="footer-size">
                    <div class="footer-inner">
                        <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Terms-Conditions.aspx"
                            target="_blank">Terms &amp; Conditions </a> |
                        <a href="https://www.pbebank.com/Others/Privacy.aspx" target="_blank">Privacy </a> |
                        <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Security-Policy.aspx"
                            target="_blank">Security </a> |
                        <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/e-Banking-Charter.aspx"
                            target="_blank"> e-Banking Charter</a> |
                        <a href="http://www.bnm.gov.my/" target="_blank">Bank Negara Malaysia</a>
                    </div>
                    <script language="JavaScript">
                        var year = new Date().getFullYear();
      document.getElementById("copyright").innerHTML = "Copyright &copy; "+year+" Public Bank Berhad (6463-H) ALL RIGHTS RESERVED";
                    </script>
                    <div class="footer-tools">
                        <div class="col-sm-12">
                            <div id="copyright">Copyright © 2020 Public Bank Berhad (6463-H) ALL RIGHTS RESERVED</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>

</html>