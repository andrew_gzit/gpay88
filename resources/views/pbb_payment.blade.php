@extends('pbb_layout2')

@section('content')
<div class="page-center">
    <!-- BEGIN PAGE CONTENT-->
    <div class="page-content-full">
        <div class="row">
            <!-- BEGIN RIGHT -->
            <div class="col-md-12">
                <div class="portlet">
                    <img src="/pbe_logo.png">
                    <ul class="nav navbar-nav pull-right">
                        <img alt="" id="ebanking-logo" src="/fpxLogo.png" class="pull-right">
                    </ul>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-name">
                                <h4>Public Bank FPX Payment Agent</h4>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="portlet">
                            <img src="/bar_selected.jpg"><img src="/bar_line.jpg"><img src="/bar.jpg">
                        </div>
                    </div>
                    <div class="margin-top-20">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet-body form">
                                <form class="form-horizontal" name="frm1" method="POST" autocomplete="off">
                                    <div class="form-body">
                                        <div class="alert alert-danger display-none bold" id="errorMsg">
                                        </div>

                                        <p>&nbsp;</p>

                                        <span id="myPAC">
                                            <div class="row"><label class="col-md-3 control-label">PAC Serial
                                                    No</label>
                                                <div class="col-md-9">
                                                    <div class="label-inline1">KK3632548 &nbsp; <span
                                                            id="expirySeconds2">(This PAC will expire in 82
                                                            seconds)</span></div>
                                                </div>
                                            </div>
                                            <div class="row"><label class="col-md-3 control-label">PAC</label>
                                                <div class="col-md-9">
                                                    <div class="form-group">
                                                        <div class="col-md-11"><input type="text"
                                                                class="form-control placeholder-no-fix"
                                                                placeholder="Enter PAC" name="SECFA" id="pac"
                                                                maxlength="6" size="12" value="" style="z-index:99999;"
                                                                autofocus=""></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>


                                        <div class="row">
                                            <label class="col-md-3 control-label">From Account</label>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <div class="col-md-11">
                                                        <select name="FROM_ACC_NO" class="form-control"
                                                            onchange="checkHalal(this);">


                                                            <option value="6309440314- S - Savings" data-rel="">
                                                                6309440314&nbsp;(Savings)&nbsp;RM 230.00</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="inner-note-message margin-left-5">
                                                <p><b>Transaction Details :-</b></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Transaction ID</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    <script language="javascript">
                                                        dispSeq("1")
                                                    </script>
                                                    <script language="javascript">
                                                        dispLastSixNumeric("2001081727520566")
                                                    </script>2001081727520566
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Serial Number</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Seller ID</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    SE00033122
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Seller Description</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    LAZADA
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Order Number</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    RT200086287187
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Amount</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    <script language="javascript">
                                                        dispSeq("2")
                                                    </script>MYR<script language="javascript">
                                                        dispAmt("5.69")
                                                    </script>5.69
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Service Charge</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    MYR0.00
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="inner-note-message margin-left-5" style="color:blue;">
                                                <b>The information received from you will be disclosed to the payee
                                                    corporation or biller for the purpose of effecting your
                                                    payment.<br>
                                                    Please click "Accept" to proceed or click "Cancel" if you do not
                                                    wish to continue.</b>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-actions fluid">
                                                <div class="col-md-6 col-md-6">
                                                    <button type="button" class="btn red"
                                                        onclick="return cancel_payment();">Cancel</button>
                                                    <button type="button" data-toggle="modal" data-target="#basic"
                                                        onclick="javascript:loadPAC();countDownPac();return false;"
                                                        name="reqPAC" class="btn red">Request PAC Now</button>
                                                    <button type="submit" class="btn red" name="next"
                                                        onclick="javascript:doSubmit();return false;"
                                                        disabled="">Accept</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner-note">
                                <h3>Note</h3>
                                <ul>
                                    <li>A security code known as PBe Authentication Code (PAC) is used when
                                        performing high risk transactions. Click <a
                                            href="javascript:MM_openBrWindow('https://www.pbebank.com/Personal-Banking/FAQs/PBe-Online-Banking/PAC-(1).aspx','','scrollbars=yes')">here</a>
                                        for more details.</li>
                                    <li>By clicking on the "Cancel" button, you would be logged out from PBe and
                                        redirected to the merchant site.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- END PORTLET -->
            </div><!-- END COL 9-->
        </div>
        <!-- END RIGHT -->
    </div><!-- END PAGE -->
</div>

<div class="footer">
    <div class="footer-size">
        <div class="footer-inner">
            <a href="http://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Terms-Conditions.aspx"
                target="_blank">Terms &amp; Conditions</a> |
            <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Privacy.aspx"
                target="_blank">Privacy</a> |
            <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Security-Policy.aspx"
                target="_blank">Security</a> |
            <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/e-Banking-Charter.aspx"
                target="_blank"> e-Banking Charter</a> |
            <a href="http://www.bnm.gov.my" target="_blank">Bank Negara Malaysia</a>
        </div>
        <div class="footer-tools">
            <div class="col-sm-12">Copyright ©
                <script>
                    document.write(new Date().getFullYear());
                </script> Public Bank Berhad (6463-H) ALL RIGHTS RESERVED</div>
        </div>
    </div>
</div>
@stop