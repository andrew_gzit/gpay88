@extends('layouts.merchant')

@php
// conditionally define title with merchant name or edit shop
if(empty($shopUser)) {

$title = __('dict.create_shop');

if(!empty($merchant)){
$title .= ' - ' . $merchant->name;
}

} else {
$title = __('dict.edit_shop');
}
@endphp

@section('title', $title)
@section('header', $title)

@section('breadcrumb')
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="{{ action('ShopController@shops') }}">@lang('dict.shops')</a></li>
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
@stop

@section('content')

<div class="panel">


    <form id='formShop' method='POST'
        action="{{ empty($shopUser) ? action('ShopController@storeShop') : action('ShopController@updateShop', $shopUser->user_id) }}">
        @csrf

        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">@lang('dict.shop_details')</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xl-5">

                    <div class="form-group row">
                        <label for="merchant" class="col-sm-4 col-form-label">@lang('dict.merchant')</label>
                        <div class="col-sm-8">
                            @if(!empty($merchants))
                            @role('Super Admin')
                            <select name="merchant" required class='form-control form-control-lg' id="merchant">
                                @foreach($merchants AS $m)
                                <option value="{{ $m->user_id }}">{{ $m->name }}</option>
                                @endforeach
                            </select>
                            @else
                            <input type="text" class='form-control-plaintext' readonly value='{{ $merchantUser->name }}'>
                            @endrole
                            @else
                            <input type="hidden" name="merchant" value='{{ $merchant->user_id }}'>
                            <input type="text" class='form-control-plaintext' readonly value='{{ $merchant->name }}'>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="shop_name" class="col-sm-4 col-form-label">@lang('dict.shop_name')</label>
                        <div class="col-sm-8">
                            <input type="text" required minlength='6' name='shop_name' id='shop_name'
                                value='{{ !empty($shopUser) ? $shopUser->shop->shop_name : '' }}'
                                class='form-control form-control-lg'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label">@lang('dict.email_add')</label>
                        <div class="col-sm-8">
                            <input type="email" required name='email' id='email' data-parsley-validateajax='email'
                                class='form-control form-control-lg'
                                value='{{ !empty($shopUser) ? $shopUser->email : '' }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone_no" class="col-sm-4 col-form-label">@lang('dict.phone_no')</label>
                        <div class="col-sm-8">
                            <input type="text" required minlength="10" maxlength="12"
                            data-parsley-type="digits" name='phone_no' id='phone_no'
                                value='{{ !empty($shopUser) ? $shopUser->phone_no : '' }}'
                                class='form-control form-control-lg'>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="username" class="col-sm-4 col-form-label">@lang('dict.username')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='username' id='username' minlength="6" data-parsley-validateajax='username'
                                value='{{ !empty($shopUser) ? $shopUser->username : '' }}'
                                class='form-control form-control-lg'>
                        </div>
                    </div>
                    @if(empty($shopUser))
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label">@lang('dict.password')</label>
                        <div class="col-sm-8">
                            <input type="password" required name='password' minlength="6" id='password'
                                class='form-control form-control-lg'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="conf_password" class="col-sm-4 col-form-label">@lang('dict.c_pw')</label>
                        <div class="col-sm-8">
                            <input type="password" required name='conf_password' data-parsley-equalto='#password'
                                id='conf_password' class='form-control form-control-lg'>
                        </div>
                    </div>
                    @else
                    <div class="form-group row">
                        @if(!empty($shopUser))
                        @if(Auth::user()->hasRole(App\Enum::ROLE_SUPER_ADMIN) || Auth::user()->hasRole(App\Enum::ROLE_MERCHANT))
                        <button type='button' id='btnResetPassword' class='btn btn-brand ml-2'>@lang('dict.reset_pw')</button>
                        @else
                        <button type='button' id='btnChangePassword' class='btn btn-brand ml-2'>@lang('dict.chg_pw')</button>
                        @endif
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>

        @if(!empty($shopUser))

        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">@lang('dict.integration_details')</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="callback_url" class="col-sm-4 col-form-label">@lang('dict.callback_url')</label>
                        <div class="col-sm-8">
                            <input type="text" name='callback_url' id='callback_url'
                                class='form-control form-control-lg' value='{{ $shopUser->shop->callback_url }}'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="response_url" class="col-sm-4 col-form-label">@lang('dict.response_url')</label>
                        <div class="col-sm-8">
                            <input type="text" name='response_url' id='response_url'
                                class='form-control form-control-lg' value='{{ $shopUser->shop->response_url }}'>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">@lang('dict.api_code')</label>
                        <div class="col-sm-8">
                            <input type="text" id='api_code' readonly='' value='{{ $shopUser->shop->api_code }}'
                                class='form-control form-control-lg form-control-plaintext'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">@lang('dict.api_secret')</label>
                        <div class="col-sm-8">
                            <input type="text" id='api_secret' readonly='' value='{{ $shopUser->shop->api_secret }}'
                                class='form-control form-control-lg form-control-plaintext'>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endif

    </form>

    @if(!empty($shopUser))
    <div class="panel-heading ui-sortable-handle">
        <h4 class="panel-title">@lang('dict.bank_details')</h4>
    </div>
    <div class="panel-body">
        <form id='formBank'>

            <div class="row">

                @can(App\RoleEnum::BANK_CREATE)
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="bank_name" class="col-sm-4 col-form-label">@lang('dict.bank_name')</label>
                        <div class="col-sm-8">
                            <select name="bank_name" id="bank_name" class='bank-selection form-control form-control-lg'>
                                @foreach($banks AS $b)
                                <option data-abbr='{{ $b->abbr }}' data-name='{{ $b->name }}' value="{{ $b->id }}">
                                    {{ $b->abbr . ' - ' . $b->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="account_no" class="col-sm-4 col-form-label">@lang('dict.bank_acc_no')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='account_no' id='account_no'
                                class='bank-input form-control form-control-lg'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="holder_name" class="col-sm-4 col-form-label">@lang('dict.bank_holder_name')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='holder_name' id='holder_name'
                                class='bank-input form-control form-control-lg'>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="min_threshold" class="col-sm-4 col-form-label">@lang('dict.min_threshold')</label>
                        <div class="col-sm-8">
                            <input type="number" required min='1' name='min_threshold' id='min_threshold'
                                class='bank-input form-control form-control-lg'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="max_threshold" class="col-sm-4 col-form-label">@lang('dict.max_threshold')</label>
                        <div class="col-sm-8">
                            <input type="number" required min='1' name='max_threshold' id='max_threshold'
                                class='bank-input form-control form-control-lg'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="accumulated_threshold" class="col-sm-4 col-form-label">@lang('dict.accumulated_threshold')</label>
                        <div class="col-sm-8">
                            <input type="number" min='0' name='accumulated_threshold' id='accumulated_threshold'
                                class='bank-input form-control form-control-lg'>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <button id='btnAddBank' type='button' class='btn btn-brand'>@lang('dict.add_bank')</button>
                </div>
                @endcan


                <div class="col-xl-12">
                    <table class="table table-brand mt-3">
                        <thead>
                            <tr>
                                <th width='1%' class='nowrap'>@lang('dict.bank_abbr')</th>
                                <th>@lang('dict.bank_name')</th>
                                <th>@lang('dict.bank_acc_no')</th>
                                <th>@lang('dict.bank_holder_name')</th>
                                <th>@lang('dict.min_threshold')</th>
                                <th>@lang('dict.max_threshold')</th>
                                <th>@lang('dict.accumulated_threshold')</th>
                                <th>@lang('dict.status')</th>
                                @canany([App\RoleEnum::BANK_EDIT])
                                <th>@lang('dict.action')</th>
                                @endcanany
                            </tr>
                        </thead>
                        <tbody id='tbodyShopBanks'>
                            @foreach($shopBanks AS $sb)
                            <tr data-id='{{ $sb->id }}'>
                                <td>{{ $sb->abbr }}</td>
                                <td>{{ $sb->name }}</td>
                                <td>{{ $sb->bank_acc_no }}</td>
                                <td>{{ $sb->bank_acc_holder }}</td>
                                <td>RM {{ number_format($sb->min_threshold, 2) }}</td>
                                <td>RM {{ number_format($sb->max_threshold, 2) }}</td>
                                <td>RM {{ number_format($sb->accumulated_threshold, 2) }}</td>
                                <td>{!! App\Utility::statusToHtml($sb->status) !!}</td>
                                @canany([App\RoleEnum::BANK_EDIT])
                                <td>{!! $sb->shopBankActions() !!}</td>
                                @endcanany
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </form>

    </div>
    @endif

    <div class="panel-footer">
        <button type='button' id='btnSave' class='btn btn-brand'>@lang('dict.save')</button>
    </div>
</div>

@if(!empty($shopUser))
@can(App\RoleEnum::SHOP_DELETE)
<div class="clear-fix">
    <button type='button' id='btnDelete' class='btn btn-danger pull-right'>@lang('dict.del_shop')</button>
</div>
@endcan
@endif

@component('components.edit-bank-info', ['banks' => $banks])
@endcomponent

@component('components.change-password')
@endcomponent

@component('components.reset-password')
@endcomponent

@stop

@push('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" />

<script>
    $(function(){

        $("#btnSave").click(function(){
            var form = $('#formShop');
            form.parsley().validate();

			form.parsley().whenValidate({
                group: 0
            }).done(function(){
                $("#formShop").submit();
            });
        });

        Parsley.addValidator('validateajax', {
            validateString: function(value, data) {
                return $.ajax({
                    type: "POST",
                    url: '{{ action("AccountController@validateDetails") }}',
                    data: {
                        key: data,
                        value,
                        user: '{{ !empty($shopUser) ?  $shopUser->user_id : '' }}'
                    },
                    success: function(response) { 
                       return response.success;
                    },
                    error: (response) => {
                        response = false;
                    }
                });
            },
            messages: {
                en: 'Value has been used!'
            }
        }, );

        @if(!empty($shopUser))
        $("#btnDelete").click(function(){
            confirmation('', `@lang('dict.confirm_del_shop')?`)
            .then((result) => {
                if(result.value) {
                    deleteShop()
                    .then((response) => {
                        if(response.success) {
                            window.location.replace(response.data);
                        } else {
                            notifyError(response.message);
                        }
                    });
                }
            });
        });

        function deleteShop()
        {
            return $.ajax({
                type: "POST",
                url: "{{ action('ShopController@destroy') }}",
                data: {
                    user: '{{ $shopUser->user_id }}'
                }
            })
        }

        $("#bank_name").select2();

        $("#btnAddBank").click(function(){
            if(validateBankInputs('#formBank')){
                let selectedBank = $("#bank_name option:selected");

                var bank = {
                    bankAbbr: selectedBank.attr('data-abbr'),
                    bankName: selectedBank.attr('data-name'),
                    bankId: $("#bank_name").val(),
                    bankHolder: $("#holder_name").val(),
                    bankAccNo: $("#account_no").val(),
                    min_threshold: $("#min_threshold").val(),
                    max_threshold: $("#max_threshold").val(),
                    accumulated_threshold: $("#accumulated_threshold").val()
                };

                addBank(bank)
                .done((response) => {
                    if(response.success){
                        notifySuccess(response.message);
                        appendBank(bank, response.data);
                    } else {
                        notifyError(response.message);
                    }
                })
                .fail((response) => {
                    notifyError(response.message);
                });
            }
        });

        $(document).on('click', '.btn-delete-bank', function(){
            confirmDeleteBank($(this).closest('ul').attr('data-id'));
        });

        $(document).on('click', '.btn-edit-bank', function(){
            retrieveBank($(this).closest('ul').attr('data-id'))
            .done((response) => {
                toggleEditBank(response.data);
            })
            .fail((response) => {
                notifyError('Unable to retrieve bank info.');
            });
        });

        $("#min_threshold").change(function(){
            $("#max_threshold").attr('min', $(this).val());
        });

        $("#edit_min_threshold").change(function(){
            $("#edit_max_threshold").attr('min', $(this).val());
        });

        $("#btnUpdateBankInfo").click(function(){
            if(validateBankInputs('#formEditBankInfo')){
                var bankInfo = {
                    bankId: $("#edit_bank").val(),
                    bank_acc_no: $("#edit_account_no").val(),
                    bank_acc_holder: $("#edit_holder_name").val(),
                    min_threshold: $("#edit_min_threshold").val(),
                    max_threshold: $("#edit_max_threshold").val(),
                    accumulated_threshold: $("#edit_accumulated_threshold").val(),
                    status: $("#edit_status").val()
                }

                updateBank(bankInfo)
                .done((response) => {
                    if(response.success){
                        updateBankInfo(response.data);
                        notifySuccess(response.message);
                    } else {
                        notifyError(response.message);
                    }
                })
                .fail(function(){

                });
            }
        });

        @if(!empty($shopUser))
        $("#btnChangePassword").click(function(){
            toggleChangePwModal();
        });

        $("#btnChgPassword").click(function(){

            var form = $("#modalChangePassword form");
            form.parsley().validate();

			if (form.parsley().isValid()){
                var oldPw = $("#chgOldPassword").val();
                var pw = $("#chgPassword").val();

                changePassword(oldPw, pw)
                .done((response) => {
                    if(response.success){
                        notifySuccess(response.message);
                        toggleChangePwModal();
                    } else {
                        notifyError(response.message);
                    }
                });
            }
        });

        function toggleChangePwModal()
        {
            $("#modalChangePassword form").trigger("reset").parsley().reset();
            $("#modalChangePassword").modal('toggle');
        }

        @if(Auth::user()->hasRole(App\Enum::ROLE_SUPER_ADMIN) || Auth::user()->hasRole(App\Enum::ROLE_MERCHANT))
        $("#btnResetPassword").click(function(){
            toggleResetPwModal();
        });

        $("#btnResetPasswordSubmit").click(function(){
            var form = $("#modalResetPassword form");
            form.parsley().validate();

			if (form.parsley().isValid()){
				var pw = $("#resetPassword").val();

                resetPassword(pw)
                .done((response) => {
                    if(response.success){
                        notifySuccess(response.message);
                        toggleResetPwModal();
                    } else {
                        notifyError(response.message);
                    }
                })
                .fail((response) => {
                    notifyError(response.message);
                });
            } 
        });

        function toggleResetPwModal()
        {
            $("#modalResetPassword form").trigger("reset").parsley().reset();;
            $("#modalResetPassword").modal('toggle');
        }

        function resetPassword(password){
            return $.ajax({
                url: "{{ action('AccountController@resetPassword') }}",
                type: "POST",
                data: {
                    accType: '{{ App\Enum::ROLE_SHOP }}',
                    user: "{{ $shopUser->user_id }}",
                    password
                }
            })
        }
        @endif

        function changePassword(oldPassword, password){
            return $.ajax({
                url: "{{ action('AccountController@changePassword') }}",
                type: "POST",
                data: {
                    user: "{{ $shopUser->user_id }}",
                    oldPassword,
                    password
                }
            })
        }
        @endif

        function validateEditBankInputs()
        {
            // return $(".edit-bank-input").filter(function () {
            //     return $.trim($(this).val()).length == 0
            // }).length == 0;
        }

        function validateBankInputs(formTarget)
        {

            var form = $(`${formTarget}`);
            form.parsley().validate();

			if (form.parsley().isValid()){
                return true;
            }
        }

        function clearBankInput()
        {
            $("#formBank").parsley().reset();
            $(".bank-input").val('').trigger('change');
            $('.bank-selection option:eq(0)').prop('selected',true).trigger('change');
        }

        function validateInput(validateObj)
        {
            return $.ajax({
                url: "{{ action('AccountController@validateDetails') }}",
                type: "POST",
                data: validateObj
            })
        }

        function confirmDeleteBank(bank)
        {
            return Swal.fire({
                title: 'Delete Bank',
                text: 'You are deleting the selected bank.',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                focusCancel: true
                })
            .then(function(result){
                if(result.value) {
                    deleteBank(bank)
                    .done((response) => {
                        if(response.success) {
                            notifySuccess(response.message);
                            $(`tr[data-id='${bank}']`).remove();
                        } else {
                            notifyError(response.message);
                        }
                    })
                    .fail((response) => {
                        notifyError(response.message);
                    });
                }
            });
        }

        function addBank(bankObj)
        {
            return $.ajax({
                url: "{{ action('ShopController@addBank', $shopUser->user_id) }}",
                method: "POST",
                data: bankObj
            })
        }

        function appendBank(bankObj, obj)
        {
            var html = `<tr data-id='${obj.id}'>` +
            `<td>${bankObj.bankAbbr}</td>` +
            `<td>${bankObj.bankName}</td>` +
            `<td>${bankObj.bankAccNo}</td>` +
            `<td>${bankObj.bankHolder}</td>` +
            `<td>RM ${parseFloat(bankObj.min_threshold).toLocaleString('us', { minimumFractionDigits: 2})}</td>` +
            `<td>RM ${parseFloat(bankObj.max_threshold).toLocaleString('us', { minimumFractionDigits: 2})}</td>` +
            `<td>RM ${parseFloat(bankObj.accumulated_threshold).toLocaleString('us', { minimumFractionDigits: 2})}</td>` +
            `<td>${obj.status}</td>` +
            `<td>${obj.action}</td>` +
            '</tr>';

            $("#tbodyShopBanks").append(html);
            clearBankInput();
        }

        function retrieveBank(bank)
        {
            return $.ajax({
                url: `/shops/retrieveBank/${bank}`,
                method: "GET",
            });
        }

        function toggleEditBank(bankInfo)
        {
            $("#formEditBankInfo").parsley().reset();

            $("#edit_bank").val(bankInfo.id);
            $("#edit_bank_name").val(bankInfo.bank_id).trigger('change');
            $("#edit_account_no").val(bankInfo.bank_acc_no);
            $("#edit_holder_name").val(bankInfo.bank_acc_holder);
            $("#edit_min_threshold").val(bankInfo.min_threshold);
            $("#edit_max_threshold").val(bankInfo.max_threshold);
            $("#edit_accumulated_threshold").val(bankInfo.accumulated_threshold);
            $("#edit_status").val(bankInfo.status);
            
            $("#modalEditBankInfo").modal('toggle');
        }

        function updateBankInfo(bankInfo)
        {
            $("#modalEditBankInfo").modal('toggle');

            console.log('here', bankInfo);

            let tr = $(`tr[data-id='${bankInfo.id}']`);
            tr.find("td:eq(2)").text(bankInfo.bank_acc_no);
            tr.find("td:eq(3)").text(bankInfo.bank_acc_holder);
            tr.find("td:eq(4)").text(`RM ${parseFloat(bankInfo.min_threshold).toLocaleString('us', { minimumFractionDigits: 2 })}`);
            tr.find("td:eq(5)").text(`RM ${parseFloat(bankInfo.max_threshold).toLocaleString('us', { minimumFractionDigits: 2 })}`);
            tr.find("td:eq(6)").text(`RM ${parseFloat(bankInfo.accumulated_threshold).toLocaleString('us', { minimumFractionDigits: 2 })}`);
            tr.find("td:eq(7)").html(bankInfo.status_elem);
        }

        function updateBank(bankInfo)
        {
            return $.ajax({
                url: `/shops/updateBank/${bankInfo.bankId}`,
                method: "POST",
                data: bankInfo
            })
        }

        function deleteBank(bank)
        {
            return $.ajax({
                url: "{{ action('ShopController@deleteBank') }}",
                method: "POST",
                data: {
                    bank
                }
            })
        }

        @endif
    });
</script>
@endpush