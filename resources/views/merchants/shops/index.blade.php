@extends('layouts.merchant')
@section('title', __('dict.shops'))
@section('header', __('dict.shops'))

@section('breadcrumb')
@if(Auth::user()->hasRole('Merchant') || Auth::user()->hasRole('Super Admin'))
<a href="{{ action('ShopController@createShop', Auth::user()->uuid) }}" class='btn btn-brand'>@lang('dict.create') @lang('dict.shop')</a>
@endif
@stop

@section('content')
<div class="table-responsive-sm">
    <table id='tableShops' class="table table-brand">
        <thead>
            <tr>
                <th class='no-wrap'>@lang('dict.shop_id')</th>
                <th>@lang('dict.email_add')</th>
                <th>@lang('dict.phone_num')</th>
                <th>@lang('dict.callback_url')</th>
                <th>@lang('dict.response_url')</th>
                <th>@lang('dict.api_code')</th>
                <th>@lang('dict.api_secret')</th>
                <th width='1%'>@lang('dict.status')</th>
                @can(\App\RoleEnum::SHOP_DISABLE)
                <th width='1%'>@lang('dict.actions')</th>
                @endcan
            </tr>
        </thead>
    </table>
</div>
@stop

@push('page_script')
<script>
    $(function(){

        const shopColumns = [
                {
                    data: 'shop_name',
                },
                {
                    data: 'email'
                },
                {
                    data: 'phone_no'
                },
                {
                    data: 'callback_url'
                },
                {
                    data: 'response_url'
                },
                {
                    data: 'api_code'
                },
                {
                    data: 'api_secret'
                },
                {
                    data: 'status'
                }
            ];

        @can(\App\RoleEnum::SHOP_DISABLE)
        shopColumns.push({
            data: 'actions'
        });
        @endcan

        const tableShops = $("#tableShops").DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ action('ShopController@shopsDT') }}",
                datatType: "json",
                type: "POST",
                data: function(d) {
                    _token = "{{ csrf_token() }}"
                }
            },
            columns: shopColumns,
            aaSorting: [
                [0, 'desc']
            ]
        });

        $(document).on('click', '.btn-disable-shop', function(){

            var shop = $(this).closest('.shop-action-menu').attr('data-shop');
            var action = $(this).attr('data-action').toLowerCase();

            confirmation('', `@lang('dict.confirm') ${action} @lang('dict.selected_shop')?`)
            .then((result) => {
                if(result.value){
                    disableShop(shop)
                    .done((response) => {
                        if(response.success){
                            tableShops.ajax.reload();
                            notifySuccess(response.message);
                        } else {
                            notifyError(response.message);
                        }
                    })
                    .fail((response) => {
                        notifyError(response.message);
                    });
                }
            });

        });

        function disableShop(shop)
        {
            return $.ajax({
                url: "{{ action('ShopController@disableShop') }}",
                method: "POST",
                data: {
                    shop
                }
            })
        }

    });
</script>
@endpush