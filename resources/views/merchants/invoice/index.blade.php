@extends('layouts.merchant')
@section('title', __('dict.invoices'))
@section('header', __('dict.invoices'))

@section('content')
<div class="table-responsive-sm">
    <table id='tableInvoice' class="table table-brand">
        <thead>
            <tr>
                <th>No.</th>
                <th>Date</th>
                <th>Status</th>
                <th>Type</th>
                <th>Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>142590</td>
                <td>Feb 13, 2020</td>
                <td>Unpaid</td>
                <td>January 2020 Invoice</td>
                <td>903.45</td>
                <td></td>
            </tr>
            <tr>
                <td>142590</td>
                <td>Feb 13, 2020</td>
                <td>Unpaid</td>
                <td>January 2020 Invoice</td>
                <td>9303.45</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>
@stop

@push('page_script')
<script>
    $(function(){
        $("#tableInvoice").DataTable();
    })
</script>
@endpush