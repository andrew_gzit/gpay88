@extends('layouts.merchant')
@section('title', __('dict.statement'))
@section('header', __('dict.statement'))

@section('content')
<div class="row">
    <div class="col-xl-3">
        <div class="form-group">
            <label for="merchant">@lang('dict.date')</label>
            <div class="date-row">
                <input id='dateStart' type="text" class='col datepicker form-control form-control-lg'>
                <span class='ml-2 mr-2'>@lang('dict.till')</span>
                <input id='dateEnd' type="text" class='col datepicker form-control form-control-lg'>
            </div>
        </div>
    </div>
    <div class="col-xl-12 mb-4">
        <button id='btnFilter' class='btn btn-brand mr-2'>@lang('dict.filter')</button>
        <button id='btnReset' class='btn btn-brand-inverse'>@lang('dict.reset')</button>
    </div>
</div>

<table id='tableStatements' class='table table-brand'>
    <thead>
        <tr>
            <th>@lang('dict.shop_name')</th>
            <th>@lang('dict.total_volume') (RM)</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop

@push('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"
    integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
    integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />

<script src='https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js'></script>

<script>
    $(function(){
        var startDate = moment().startOf('month').format('DD/MM/YYYY');
        var endDate = moment().endOf('day');

        $("#dateStart, #dateEnd").datetimepicker({
            format: 'DD/MM/YYYY',
            maxDate: endDate,
            ignoreReadonly: true
,            icons: {
                previous: 'las la-angle-left',
            }
        });

        $("#dateStart").val(startDate);
        $("#dateEnd").val(endDate.format('DD/MM/YYYY'));

        $("#dateStart").on("dp.change", function (e) {
            $('#dateEnd').data("DateTimePicker").minDate(e.date);
        });
        $("#dateEnd").on("dp.change", function (e) {
            $('#dateStart').data("DateTimePicker").maxDate(e.date);
        });

        $("#btnFilter").click(function(){
            tableStatements.ajax.reload();
        });

        const tableStatements = $("#tableStatements").DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            dom: 'lBfrtip',
            buttons: [
                'excel', 'pdf'
            ],
            ajax: {
                url: "{{ action('ReportController@statementDT') }}",
                datatType: "json",
                type: "POST",
                
                data: function(d){
                    d.filter = filterData();
                }
            },
            columns: [
                {
                    data: 'shop_name',
                    class: 'nowrap'
                },
                {
                    data: 'amount',
                    class: 'nowrap'
                },
            ],
            aaSorting: [
                [1, 'desc']
            ],
        }); 

        function filterData()
        {
            return {
                _token: "{{ csrf_token() }}",
                startDate: $("#dateStart").val(),
                endDate: $("#dateEnd").val(),
            }
        }

        $("#btnReset").click(function(){
            $("#dateStart").data("DateTimePicker").date(startDate);
        });

    });
</script>
@endpush