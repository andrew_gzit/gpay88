@extends('layouts.merchant')
@section('title', __('dict.transactions'))
@section('header', __('dict.transactions'))

@section('breadcrumb')
<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
    aria-controls="collapseOne">
    <i class="las la-sliders-h"></i>
    Filters
</button>
@stop

@section('content')
<div id="collapseOne" class="collapse show " aria-labelledby="headingOne" data-parent="#accordion">
    <div class="row m-0 w-100">
        <div class="col-xl-4">
            <div class="form-group">
                <label for="merchant">@lang('dict.date')</label>
                <div class="date-row">
                    <input id='dateStart' type="text" class='col form-control form-control-lg'>
                    <span class='ml-2 mr-2'>@lang('dict.till')</span>
                    <input id='dateEnd' type="text" class='col form-control form-control-lg'>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-6">
            <div class="form-group ">
                <label for="merchants">@lang('dict.merchants')</label>
                <select name="merchants" class='form-control form-control-lg w-100' id="merchants">
                    <option value=""></option>
                    @foreach($merchants AS $m)
                    <option value="{{ $m->uuid }}">{{ $m->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xl-2 col-6">
            <div class="form-group ">
                <label for="shops">@lang('dict.shops')</label>
                <select name="shops" class='form-control form-control-lg' id="shops">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="col-xl-2 col-6">
            <div class="form-group ">
                <label for="shops">@lang('dict.banks')</label>
                <select name="banks" class='form-control form-control-lg' id="banks">
                    <option value=""></option>
                    <option value="-">All Banks</option>
                    @foreach($banks AS $b)
                    <option value="{{ $b->name }}">{{ $b->abbr }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xl-2 col-6">
            <div class="form-group ">
                <label for="status">@lang('dict.status')</label>
                <select name="status" class='form-control form-control-lg' id="status">
                    <option value=""></option>
                    <option value="-">All Status</option>
                    <option value="{{ App\Enum::TX_PENDING['key'] }}">{{ App\Enum::TX_PENDING['value'] }}
                    </option>
                    <option value="{{ App\Enum::TX_SUCCESS['key'] }}">{{ App\Enum::TX_SUCCESS['value'] }}
                    </option>
                    <option value="{{ App\Enum::TX_REJECTED['key'] }}">{{ App\Enum::TX_REJECTED['value'] }}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-xl-12 mb-4">
            <button id='btnFilter' class='btn btn-brand mr-2'>@lang('dict.filter')</button>
            <button id='btnReset' class='btn btn-brand-inverse'>@lang('dict.reset')</button>
        </div>
    </div>
</div>

<div class="table-responsive-sm">
    <table id='tableTransactions' class="table table-brand">
        <thead>
            <tr>
                <th width='1%'>@lang('dict.transaction_id')</th>
                <th width='1%'>@lang('dict.shop_name')</th>
                <th width='1%'>@lang('dict.ref_no')</th>
                <th width='1%'>@lang('dict.bank')</th>
                <th width='1%'>@lang('dict.time')</th>
                <th>@lang('dict.remarks')</th>
                <th width='1%'>@lang('dict.amount')</th>
                <th width='1%'>@lang('dict.status')</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th colspan="6" class='text-right'>@lang('dict.total'):</th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

@component('components.view-receipt')
@endcomponent

@stop

@push('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"
    integrity="sha256-d/edyIFneUo3SvmaFnf96hRcVBcyaOy96iMkPez1kaU=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css"
    integrity="sha256-FdatTf20PQr/rWg+cAKfl6j4/IY3oohFAJ7gVC3M34E=" crossorigin="anonymous" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"
    integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
    integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />

<script src='https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js'></script>

<script>
    $(function(){

        $("#merchants").select2({
            placeholder: 'Filter by Merchant'
        }).change(function(){
            var merchantId = $(this).val();
            retrieveShops(merchantId)
            .done((response) => {
                if(response.success){
                    var shops = response.data;
                    var html = "<option></option>";

                    shops.forEach(function(shop, index){
                        html += `<option value='${shop.user_id}'>${shop.name}</option>`;
                    });

                    $("#shops").html(html).select2({
                        placeholder: 'Filter by Shop'
                    });
                }
            })
            .fail((response) => {
                console.log(response);
            });
        });

        function retrieveShops(merchantId)
        {
            return $.ajax({
                url: '{{ action("MerchantController@retrieveShops") }}',
                method: 'POST',
                data: {
                    merchantId
                }
            });
        }

        $("#status").select2({
            placeholder: 'Filter by Status'
        });

        $("#banks").select2({
            placeholder: 'Filter by Bank'
        })

        $("#shops").select2({
            placeholder: 'Filter by Shop'
        });

        var startDate = moment().startOf('day').format('DD/MM/YYYY hh:mm:ss A');
        var endDate = moment().endOf('day');

        $("#dateStart, #dateEnd").datetimepicker({
            format: 'DD/MM/YYYY hh:mm:ss A',
            maxDate: endDate,
            icons: {
                previous: 'las la-angle-left',
            }
        });

        $("#dateStart").val(startDate);
        $("#dateEnd").val(endDate.format('DD/MM/YYYY hh:mm:ss A'));

        $("#dateStart").on("dp.change", function (e) {
            $('#dateEnd').data("DateTimePicker").minDate(e.date);
        });
        $("#dateEnd").on("dp.change", function (e) {
            $('#dateStart').data("DateTimePicker").maxDate(e.date);
        });

        var data = {};

        const tableTransactions = $("#tableTransactions").DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            dom: 'lBfrtip',
            buttons: [
                'excel', 'pdf'
            ],
            ajax: {
                url: "{{ action('ReportController@transactionsDT') }}",
                datatType: "json",
                type: "POST",
                
                data: function(d){
                    d.filter = filterData();
                }
            },
            columns: [
                {
                    data: 'transaction_id',
                    class: 'nowrap'
                },
                {
                    data: 'shop_name',
                    class: 'nowrap'
                },
                {
                    data: 'ref_no',
                    class: 'nowrap'
                },
                {
                    data: 'bank',
                    class: 'nowrap'
                },
                {
                    data: 'time',
                    class: 'nowrap'
                },
                {
                    data: 'remarks'  
                },
                {
                    data: 'amount',
                    class: 'nowrap'
                },
                {
                    data: 'status',
                    class: 'nowrap'
                },
            ],
            aaSorting: [
                [4, 'desc']
            ],
            footerCallback: function (row, data, start, end, display) {
                var api = this.api(), data;

                var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                    };
                
                total = api
                    .column(6)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
 
                pageTotal = api
                    .column(6, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                pageTotal = parseFloat(pageTotal).toLocaleString('us', { minimumFractionDigits: 2});
                total = parseFloat(total).toLocaleString('us', { minimumFractionDigits: 2});

                var html = pageTotal != total ? `RM'${pageTotal} ( RM ${total} total)` : pageTotal;
 
                $(api.column(6).footer()).html(html);
            }
        });

        function filterData()
        {
            return {
                _token: "{{ csrf_token() }}",
                startDate: $("#dateStart").val(),
                endDate: $("#dateEnd").val(),
                merchant: $("#merchants").val(),
                shop: $("#shops").val(),
                status: $("#status").val(),
                bank: $("#banks").val()
            }
        }

        $("#btnFilter").click(function(){
            tableTransactions.ajax.reload();
        });

        $("#btnReset").click(function(){
            $("#shops").empty().val('').trigger('change');
            $("#merchants").val('').trigger('change');
            $("#status").val('').trigger('change');
            $("#banks").val('').trigger('change');
            $("#dateStart").data("DateTimePicker").date(startDate);
            // $("#dateEnd").data("DateTimePicker").date(new Date());
        });

        $(document).on('click', '.a-view-receipt', function(){
            displayReceipt($(this).attr('data-img'));
        });

        function displayReceipt(receipt_path){
            $("#imgReceipt").attr('src', receipt_path);
            $("#modalViewReceipt").modal('toggle');
        }
        
    });
</script>
@endpush