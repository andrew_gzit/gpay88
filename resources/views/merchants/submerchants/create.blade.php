@extends('layouts.merchant')

@php
if(empty($submerchantUser)) {

$title = __('dict.create_sub_merchant');

if(!empty($merchant)){
$title .= ' - ' . $merchant->name;
}

} else {
$title = __('dict.edit_sub_merchant');
}
@endphp

@section('title', $title)
@section('header', $title)

@section('breadcrumb')
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="{{ action('MerchantController@merchants') }}">Merchants</a></li>
    @if(!empty($merchantUser))
    <li class="breadcrumb-item">
        <a href="{{ action('MerchantController@viewMerchant', $merchantUser->user_id) }}">{{ $merchantUser->name }}
            Detail</a>
    </li>
    @endif
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
@stop

@section('content')
<form id='formSubMerchant' method='POST'
    action="{{ empty($submerchantUser) ? action('SubmerchantController@store') : action('SubmerchantController@update', $submerchantUser->user_id) }}">
    @csrf
    <div class="panel">
        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">@lang('dict.sub_merchant_details')</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xl-5">

                    <div class="form-group row">
                        <label for="merchant" class="col-sm-4 col-form-label">@lang('dict.merchant')</label>
                        <div class="col-sm-8">
                            @if(!empty($merchantUser))
                            <input type="text" hidden value='{{ $merchantUser->user_id }}' name='merchant'>
                            <input type="text" class='form-control-plaintext' readonly
                                value='{{ $merchantUser->name }}'>
                            @else
                            <select name="merchant" id="merchant" class='form-control'>
                                @foreach($merchants AS $m)
                                <option value="{{ $m->uuid }}">{{ $m->name }}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">@lang('dict.name')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='name' id='name'
                                class='form-control form-control-lg text-uppercase'
                                value='{{ !empty($submerchantUser) ? $submerchantUser->name : '' }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label">@lang('dict.email_add')</label>
                        <div class="col-sm-8">
                            <input type="email" name='email' id='email' class='form-control form-control-lg'
                                data-parsley-validateajax='email'
                                value='{{ !empty($submerchantUser) ? $submerchantUser->email : '' }}'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone_no" class="col-sm-4 col-form-label">@lang('dict.phone_no')</label>
                        <div class="col-sm-8">
                            <input type="text" minlength="10" maxlength="12"
                            data-parsley-type="digits" name='phone_no' id='phone_no'
                                class='form-control form-control-lg'
                                value='{{ !empty($submerchantUser) ? $submerchantUser->phone_no : '' }}'>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="username" class="col-sm-4 col-form-label">@lang('dict.username')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='username' minlength='6' data-parsley-pattern='/^\S+$/g'
                                id='username' data-parsley-validateajax='username' class='form-control form-control-lg'
                                value='{{ !empty($submerchantUser) ? $submerchantUser->username : '' }}'>
                        </div>
                    </div>
                    @if(empty($submerchantUser))
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label">@lang('dict.password')</label>
                        <div class="col-sm-8">
                            <input type="password" required name='password' minlength='6' id='password'
                                class='form-control form-control-lg'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="conf_password" class="col-sm-4 col-form-label">@lang('dict.c_pw')</label>
                        <div class="col-sm-8">
                            <input type="password" required name='conf_password' data-parsley-equalto='#password'
                                id='conf_password' class='form-control form-control-lg'>
                        </div>
                    </div>
                    @else
                    @if(Auth::user()->hasRole(App\Enum::ROLE_SUPER_ADMIN) ||
                    Auth::user()->hasRole(App\Enum::ROLE_MERCHANT))
                    <button type='button' id='btnResetPassword' class='btn btn-brand'>@lang('dict.reset_pw')</button>
                    @else
                    @can(App\RoleEnum::SUBMERCHANT_EDIT)
                    <button type='button' id='btnChangePassword' class='btn btn-brand'>@lang('dict.chg_pw')</button>
                    @endcan
                    @endif
                    @endif

                    <div class="row">
                        <div class="col-xl-6">
                            <label for="">Shop List</label>
                            <select class='form-control' id="shop_multiselect"
                                size="{{ count($merchant_shops) }}" multiple="multiple">
                                @foreach($merchant_shops AS $m_shop)
                                <option value="{{ $m_shop->user_id }}">{{ $m_shop->shop_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xl-6">
                            <label for="">Selected Shop</label>
                            <select name="merchant_shop[]" id="shop_multiselect_to" class="form-control" size="8"
                                multiple="multiple">
                            
                            @if(!empty($submerchantUser))
                            @foreach($submerchant_shops AS $m_shop)
                            <option value="{{ $m_shop->user_id }}">{{ $m_shop->shop_name }}</option>
                            @endforeach
                            @endif

                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type='submit' class='btn btn-brand'>@lang('dict.save')</button>
        </div>
    </div>
</form>

@if(!empty($submerchantUser))
@can(App\RoleEnum::SUBMERCHANT_DELETE)
<button class='btn btn-danger' type='button' id='btnDelete'>@lang('dict.del_sub_merchant')</button>
@endcan
@endif

@component('components.change-password')
@endcomponent

@component('components.reset-password')
@endcomponent

@stop

@push('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/multiselect/2.2.9/js/multiselect.min.js"
    integrity="sha256-d0Be108Oo5AK7vd8u6C/Gdg9WnqF2pj8YPgaxgfxjOM=" crossorigin="anonymous"></script>

<script>
    $(function(){
        $("#formSubMerchant").parsley();

        $("#merchant").select2();

        $("#shop_multiselect").multiselect({
            search: {
                left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
                right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            }
        });

        @if(!empty($submerchantUser))
        $("#btnDelete").click(function(){
            confirmation('', `@lang('dict.con_del_sub_merchant')?`)
            .then((result) => {
                if(result.value) {
                    deleteSubMerchant()
                    .then((response) => {
                        if(response.success) {
                            window.location.replace(response.data);
                        } else {
                            notifyError(response.msg);
                        }
                    });
                }
            });
        });

        function deleteSubMerchant()
        {
            return $.ajax({
                type: "POST",
                url: "{{ action('SubmerchantController@destroy') }}",
                data: {
                    user: '{{ $submerchantUser->user_id }}'
                }
            })
        }
        @endif

        Parsley.addValidator('validateajax', {
            validateString: function(value, data) {
                return $.ajax({
                    type: "POST",
                    url: '{{ action("AccountController@validateDetails") }}',
                    data: {
                        key: data,
                        value,
                        user: '{{ !empty($submerchantUser) ? $submerchantUser->user_id : '' }}'
                    },
                    success: function(response) {
                       return response.success;
                    },
                    error: (response) => {
                        response = false;
                    }
                });
            },
            messages: {
                en: 'Value has been used!'
            }
        }, );

        @if(!empty($submerchantUser))
        $("#btnChangePassword").click(function(){
            toggleChangePwModal();
        });

        $("#btnChgPassword").click(function(){
            var form = $("#modalChangePassword form");
            form.parsley().validate();

			if (form.parsley().isValid()){
                var oldPw = $("#chgOldPassword").val();
                var pw = $("#chgPassword").val();

                changePassword(oldPw, pw)
                .done((response) => {
                    if(response.success){
                        notifySuccess(response.message);
                        toggleChangePwModal();
                    } else {
                        notifyError(response.message);
                    }
                });
            }
        });

        function toggleChangePwModal()
        {
            $("#modalChangePassword form").trigger("reset").parsley().reset();
            $("#modalChangePassword").modal('toggle');
        }

        @if(Auth::user()->hasRole(App\Enum::ROLE_SUPER_ADMIN) || Auth::user()->hasRole(App\Enum::ROLE_MERCHANT))
        $("#btnResetPassword").click(function(){
            toggleResetPwModal();
        });

        $("#btnResetPasswordSubmit").click(function(){
            var form = $("#modalResetPassword form");
            form.parsley().validate();

			if (form.parsley().isValid()){
				var pw = $("#resetPassword").val();

                resetPassword(pw)
                .done((response) => {
                    if(response.success){
                        notifySuccess(response.message);
                        toggleResetPwModal();
                    } else {
                        notifyError(response.message);
                    }
                })
                .fail((response) => {
                    notifyError(response.message);
                });
            }
        });

        function toggleResetPwModal()
        {
            $("#modalResetPassword form").trigger("reset").parsley().reset();;
            $("#modalResetPassword").modal('toggle');
        }

        function resetPassword(password){
            return $.ajax({
                url: "{{ action('AccountController@resetPassword') }}",
                type: "POST",
                data: {
                    accType: '{{ App\Enum::ROLE_SUBMERCHANT }}',
                    user: "{{ $submerchantUser->user_id }}",
                    password
                }
            })
        }
        @endif

        function changePassword(oldPassword, password){
            return $.ajax({
                url: "{{ action('AccountController@changePassword') }}",
                type: "POST",
                data: {
                    user: "{{ $submerchantUser->user_id }}",
                    oldPassword,
                    password
                }
            })
        }
        @endif
    });
</script>
@endpush