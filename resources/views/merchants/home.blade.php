@extends('layouts.merchant')
@section('title', __('dict.home'))
@section('header', __('dict.home'))

@section('content')
<div class="row">
    <div class="col-xl-12">
        <div class="mb-3">
            <div class="form-group row">
                <label for="merchantFilter" class="col-form-label col-3">@lang('dict.merchant')</label>
                <div class="col-sm-2 col-9">
                    @if(!Auth::user()->hasRole(App\Enum::ROLE_SHOP))
                    <select id="merchantFilter" class="form-control form-control-lg">
                        <option value=""></option>
                        @foreach($merchants AS $m)
                        <option value="{{ $m->user_id }}">{{ $m->name }}</option>
                        @endforeach
                    </select>
                    @else
                    <input type="text" readonly class='form-control form-control-lg form-control-plaintext'
                        value='{{ Auth::user()->getMerchantName() }}'>
                    @endif
                </div>
                <label for="shopFilter" class="col-form-label col-3">@lang('dict.shop')</label>
                <div class="col-sm-2 col-9">
                    @if(!Auth::user()->hasRole(App\Enum::ROLE_SHOP))
                    <select id="shopFilter" class="form-control form-control-lg"></select>
                    @else
                    <input type="text" readonly class='form-control form-control-lg form-control-plaintext'
                        value='{{ Auth::user()->name }}'>
                    @endif
                </div>
            </div>
        </div>

    </div>

    <div class="col-12 widget-wrapper">
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount'>RM <span id='totalVolume'>-</span></div>
                <div class='title'>@lang('dict.total_volume')</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount'><span id='totalTransactions'>-</span></div>
                <div class='title'>@lang('dict.total_transactions')</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount'>RM <span id='todayVolume'>-</span></div>
                <div class='title'>@lang('dict.today_volume')</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount'>RM <span id='curMonthVolume'>-</span></div>
                <div class='title'>{{ Carbon\Carbon::now()->format('M Y')}} @lang('dict.volume')</div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount'>RM <span id='pastMonthVolume'>-</span></div>
                <div class='title'>{{ Carbon\Carbon::now()->firstOfMonth()->subMonth()->format('M Y')}}
                    @lang('dict.volume')</div>
            </div>
        </div>
    </div>

    {{-- <div class="col-xl-3 col-sm-6 col-6">
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount' id='totalVolume'>-</div>
                <div class='title'>@lang('dict.total_volume')</div>
            </div>
        </div>
    </div>
    <div class="col-xl-1 col-sm-6 col-6">
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount' id='totalTransactions'>-</div>
                <div class='title'>@lang('dict.total_transactions')</div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-sm-6 col-6">
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount' id='todayVolume'>-</div>
                <div class='title'>@lang('dict.today_volume')</div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 col-6">
        <div class="panel">
            <div class="panel-body panel-widget">
                <div class='amount' id='curMonthVolume'>-</div>
                <div class='title'>{{ Carbon\Carbon::now()->format('M Y')}} @lang('dict.volume')
</div>
</div>
</div>
</div>
<div class="col-xl-3 col-sm-6 col-6">
    <div class="panel">
        <div class="panel-body panel-widget">
            <div class='amount' id='pastMonthVolume'>-</div>
            <div class='title'>{{ Carbon\Carbon::now()->firstOfMonth()->subMonth()->format('M Y')}}
                @lang('dict.volume')</div>
        </div>
    </div>
</div> --}}
<div class="col-xl-9">
    <div class="panel">
        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">
                @lang('dict.summary_report')
                <span id='updatedAt' class='updated-at'></span>
            </h4>
        </div>
        <div class="panel-body">
            <canvas id='summaryReport'></canvas>
        </div>
    </div>
</div>
<div class="col-xl-3">
    <div class="panel">
        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">@lang('dict.bank_status')</h4>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <select id="shopSelect" class='form-control form-control-lg'>
                    @foreach($shops AS $s)
                    <option value="{{ $s->uuid }}">{{ $s->shop_name . ' - ' . $s->name }}</option>
                    @endforeach
                </select>
            </div>
            <div id='bankInfos'></div>
        </div>
    </div>
</div>
</div>
@stop

@push('page_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"
    integrity="sha256-d/edyIFneUo3SvmaFnf96hRcVBcyaOy96iMkPez1kaU=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css"
    integrity="sha256-FdatTf20PQr/rWg+cAKfl6j4/IY3oohFAJ7gVC3M34E=" crossorigin="anonymous" />

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script>
    $(function(){

        var selectedShopId;

        // Summary Report chart
        var summaryReport = new Chart($("#summaryReport"), {
            type: 'line',
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Total Volume (RM)'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }]
                }
            }
        });

        function retrieveSummaryReport(user)
        {
            selectedShopId = user;
            return $.ajax({
                type: "POST",
                url: "{{ action('AccountController@summaryReport') }}",
                data: {
                    user
                }
            })
        }

        $("#shopSelect").select2();

        $("#merchantFilter").select2({
            placeholder: "Select a Merchant"
        }).change(function(){
            var merchantId = $(this).val();
            retrieveShops(merchantId)
            .done((response) => {
                if(response.success) {
                    var shops = response.data;
                    processShop("#shopFilter", shops);
		            $('#shopFilter').val($('#shopFilter option:eq(1)').val()).trigger('change');
                } else {
                    notifyError(response.message);
                }
            })
            .fail((response) => {
                notifyError(response.message);
            });
        });

        function retrieveShops(merchantId)
        {
            return $.ajax({
                url: '{{ action("MerchantController@retrieveShops") }}',
                method: 'POST',
                data: {
                    merchantId
                }
            });
        }

        function processShop(targetElem, shops)
        {
            var html = "<option></option>";

            shops.forEach(function(shop, index){
                html += `<option value='${shop.user_id}'>${shop.shop_name} - ${shop.name}</option>`;
            });

            $(targetElem).html(html).select2({
                placeholder: 'Select a Shop'
            });
        }

        $("#shopFilter").select2({
            placeholder: "Select a Shop"
        }).change(function(){
            var shopId = $(this).val();

            if(shopId != ''){

                retrieveSummaryReport(shopId)
                .done((response) => {
                    if(response.success){
                        summaryReport.data = response.data;
                        summaryReport.update();
                    } else {
                        notifyError(response.message);
                    }
                });

                retrieveShopWidgetInfo(shopId)
                .done(function(response){
                    insertWidgetData(response.data);
                    if(!response.success){
                        notifyError(response.message);
                    }
                })
                .fail(function(){

                });
            }

        });

        $("#shopSelect").change(function(){
            var shopId = $(this).val();

            $("#bankInfos").empty();

            if(shopId != null){
                retrieveShopBankInfo(shopId)
                .done(function(response){
                    if(response.success){
                        var bankInfos = response.data;

                        if(bankInfos.length > 0) {
                            bankInfos.forEach(function(bankInfo, index){
                                $("#bankInfos").append(bankBoxElement(bankInfo));
                            });
                        } else {
                            $("#bankInfos").append('<p>No banks available.</p>');
                        }
                    } else {
                        notifyError(response.message);
                    }
                })
                .fail(function(response){
                    notifyError(response.message);
                });
            }

        }).trigger('change');

        function retrieveShopBankInfo(shopId)
        {
            return $.ajax({
                url: "{{ action('ShopController@retrieveShopBankInfo') }}",
                method: "POST",
                data: {
                    shopId
                }
            })
        }

        function bankBoxElement(bankInfo)
        {

            if(bankInfo.status === -1){
                var badgeCss = 'badge-warning';
                var bankStatus = 'Inactive';
            } else {
                var badgeCss = 'badge-brand';
                var bankStatus = 'Active';
            }

            var bankStatus = `<span class="badge ${badgeCss}">${bankStatus}</span>`;

            return "<div class='bank-box'>" +
                `<div class='title'>${bankInfo.abbr} ${bankStatus}</div>` +
                // `<div>RM ${parseFloat(bankInfo.accumulated).toLocaleString('us', { minimumFractionDigits: 2})} / RM ${parseFloat(bankInfo.accumulated_threshold).toLocaleString('us', { minimumFractionDigits: 2})}</div>` +
                "</div>"
        }

        function retrieveShopWidgetInfo(shopId)
        {
            return $.ajax({
                url: "{{ action('ShopController@retrieveShopWidgetInfo') }}",
                method: "POST",
                data: {
                    shopId
                }
            })
        }

        const totalVolumeElem = $("#totalVolume");
        const totalTransactionsElem = $("#totalTransactions");
        const todayVolumeElem = $("#todayVolume");
        const curMonthVolumeElem = $("#curMonthVolume");
        const pastMonthVolumeElem = $("#pastMonthVolume");

        function insertWidgetData(data)
        {
            var totalVolume = '0',
            totalTransactions = '0',
            todayVolume = '0',
            curMonthVolume = '0',
            pastMonthVolume = '0';

            if(data != null){
                totalVolume = `${parseFloat(data.totalVolume).toLocaleString('us', { minimumFractionDigits: 2 })}`;
                totalTransactions = data.totalTransactions;
                todayVolume =  `${parseFloat(data.todayVolume).toLocaleString('us', { minimumFractionDigits: 2 })}`;
                curMonthVolume = `${parseFloat(data.curMonthVolume).toLocaleString('us', { minimumFractionDigits: 2 })}`;
                pastMonthVolume = `${parseFloat(data.pastMonthVolume).toLocaleString('us', { minimumFractionDigits: 2 })}`;
            }

            totalVolumeElem.text(totalVolume);
            totalTransactionsElem.text(totalTransactions);
            todayVolumeElem.text(todayVolume);
            curMonthVolumeElem.text(curMonthVolume);
            pastMonthVolumeElem.text(pastMonthVolume);
        }

        @if(Auth::user()->hasRole(App\Enum::ROLE_SHOP))
        retrieveSummaryReport("{{ Auth::user()->user_id }}")
        .done((response) => {
            if(response.success){
                summaryReport.data = response.data;
                summaryReport.update();
            } else {
                notifyError(response.message);
            }
        });

        retrieveShopWidgetInfo("{{ Auth::user()->user_id }}")
        .done(function(response){
            insertWidgetData(response.data);
            if(!response.success){
                notifyError(response.message);
            }
        });
        @endif

        @if(count($merchants) > 0)
        $('#merchantFilter').val($('#merchantFilter option:eq(1)').val()).trigger('change');
        @endif

        // Auto update every 10s
        setInterval(function(){
            autoRefresh();
            $("#updatedAt").text(`Last updated at ${moment(new Date()).format('MM/DD/YYYY h:mm:ss a')}`);
        }, 60000);

        function autoRefresh()
        {
            // Update Report
            retrieveSummaryReport(selectedShopId)
            .done((response) => {
                if(response.success){
                    var newDataset = response.data.datasets[0].data;

                    $(newDataset).each((index, newData) => {
                        let curData = summaryReport.data.datasets[0].data[index];
                        if(curData !== newData){
                            summaryReport.data.datasets[0].data[index] = newData;
                        }
                    });

                    summaryReport.update();
                }
            });

            // Update widget panels
            retrieveShopWidgetInfo(selectedShopId)
            .done(function(response){
            insertWidgetData(response.data);
            if(!response.success){
                notifyError(response.message);
            }
        });
        }

       

    });
</script>
@endpush