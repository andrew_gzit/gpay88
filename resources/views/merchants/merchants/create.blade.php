@extends('layouts.merchant')
@section('title', __('dict.create_merchant'))
@section('header', __('dict.create_merchant'))
@section('breadcrumb')

@stop

@section('content')
<form id='formMerchant' method='POST' action="{{ action('MerchantController@storeMerchant') }}">
    @csrf
    <div class="panel">

        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">@lang('dict.merchant_details')</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">@lang('dict.name')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='name' id='name'
                                class='form-control form-control-lg text-uppercase'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label">@lang('dict.email_add')</label>
                        <div class="col-sm-8">
                            <input type="email" required name='email' id='email' class='form-control form-control-lg'
                                data-parsley-validateajax='email'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone_no" class="col-sm-4 col-form-label">@lang('dict.phone_no')</label>
                        <div class="col-sm-8">
                            <input type="text" minlength="10" maxlength="12"
                            data-parsley-type="digits" name='phone_no' id='phone_no'
                                class='form-control form-control-lg'>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="username" class="col-sm-4 col-form-label">@lang('dict.username')</label>
                        <div class="col-sm-8">
                            <input type="text" required name='username' minlength='6' id='username'
                                class='form-control form-control-lg' data-parsley-validateajax='username'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label">@lang('dict.password')</label>
                        <div class="col-sm-8">
                            <input type="password" required name='password' id='password'
                                class='form-control form-control-lg' minlength='6'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="conf_password" class="col-sm-4 col-form-label">@lang('dict.c_pw')</label>
                        <div class="col-sm-8">
                            <input type="password" required name='conf_password' data-parsley-equalto='#password'
                                id='conf_password' class='form-control form-control-lg'>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">@lang('dict.settings')</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="processing_fee" class="col-sm-4 col-form-label">@lang('dict.processing_fee')</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="number" value='1.3' step='0.01' style='max-width: 5rem' max="100" min="0" required
                                    name='processing_fee' id='processing_fee' class='form-control form-control-lg'>
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <button type='submit' class='btn btn-brand'>@lang('dict.save')</button>
        </div>
    </div>
</form>
@stop

@push('page_script')
<script>
    $(function(){
        $("#formMerchant").parsley({
            errorsContainer: function(el) {
                return el.$element.closest('.col-sm-8');
            },
        });

        Parsley.addValidator('validateajax', {
            validateString: function(value, data) {
                return $.ajax({
                    type: "POST",
                    url: '{{ action("AccountController@validateDetails") }}',
                    data: {
                        key: data,
                        value,
                        user: ''
                    },
                    success: function(response) {
                       return response.success;
                    },
                    error: (response) => {
                        response = false;
                    }
                });
            },
            messages: {
                en: 'Value has been used!'
            }
        }, );
    });
</script>
@endpush