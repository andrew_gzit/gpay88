@extends('layouts.merchant')
@section('title', __('dict.merchants'))
@section('header', __('dict.merchants'))

@section('breadcrumb')
@can('App\RoleEnum::MERCHANT_CREATE')
<a href="{{ action('MerchantController@createMerchant') }}" class='btn btn-brand'>@lang('dict.create') @lang('dict.merchant')</a>
@endcan
@stop

@section('content')
<div class="table-responsive-sm">
    <table id='tableMerchants' class="table table-brand">
        <thead>
            <tr>
                <th>@lang('dict.name')</th>
                <th>@lang('dict.email_add')</th>
                <th>@lang('dict.phone_num')</th>
                <th width='1%' class='nowrap'>@lang('dict.processing_fee') (%)</th>
                <th width='1%' class='nowrap'>@lang('dict.status')</th>
                @canany([\App\RoleEnum::MERCHANT_DISABLE, \App\RoleEnum::MERCHANT_EDIT])
                <th width='1%'>@lang('dict.actions')</th>
                @endcanany
            </tr>
        </thead>
    </table>
</div>
@stop

@push('page_script')
<script>
    $(function(){

        const merchantColumns =  [
            {
                data: 'name',
            },
            {
                data: 'email'
            },
            {
                data: 'phone_no'
            },
            {
                data: 'processing_fee'
            },
            {
                data: 'status'
            }
        ];

        var columnObj = {};

        @canany([\App\RoleEnum::MERCHANT_DISABLE, \App\RoleEnum::MERCHANT_EDIT])
        merchantColumns.push({
            data: 'actions'
        });

        columnObj.orderable = false;
        columnObj.targets = 5;
        @endcanany   

        const tableMerchants = $("#tableMerchants").DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ action('MerchantController@merchantsDT') }}",
                datatType: "json",
                type: "POST",
                data: function(d) {
                    _token = "{{ csrf_token() }}"
                }
            },
            columns: merchantColumns,
            columnDefs: [
                columnObj
            ],
            aaSorting: [
                [0, 'desc']
            ]
        });

        @if(Auth::user()->can('{{ \App\RoleEnum::MERCHANT_DISABLE }}'))
        $(document).on('click', '.btn-update-status', function() {
            var action = $(this).attr('data-action').toLowerCase();

            var data = {
                merchantId: $(this).attr('data-id')
            }

            confirmation('', `@lang('dict.confirm') ${action} @lang('dict.selected_merchant')?`)
            .then((result) => {
                if(result.value) {
                    disableMerchant(data)
                        .done((data) => {
                            if(data.success){
                                notifySuccess(data.message);
                                tableMerchants.ajax.reload();
                            } else {
                                notifyError(data.message);
                            }
                        }).fail((data) => {
                            notifyError(data.message);
                        });
                }
            });
        });

        function disableMerchant(data) {
            return $.ajax({
                url: '/merchants/suspend',
                method: 'POST',
                data
            });
        }
        @endif

    });
</script>
@endpush