@extends('layouts.merchant')
@section('title', $merchant->name . __('dict.detail'))
@section('header', $merchant->name . __('dict.detail'))

@section('breadcrumb')
<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><a href="{{ action('MerchantController@merchants') }}">@lang('dict.merchants')</a></li>
    <li class="breadcrumb-item active">{{ $merchant->name . __('dict.detail') }}</li>
</ol>
@stop

@section('content')

<div class="row">
    <div class="col-xl-3">
        <div class="panel">
            <div class="panel-body">
                <table class='table table-condensed-list'>
                    <tr>
                        <th>@lang('dict.username')</th>
                        <td>{{ $merchant->username }}</td>
                    </tr>
                    <tr>
                        <th>@lang('dict.name')</th>
                        <td>{{ $merchant->name }}</td>
                    </tr>
                    <tr>
                        <th>@lang('dict.email')</th>
                        <td>{{ $merchant->email }}</td>
                    </tr>
                    <tr>
                        <th>@lang('dict.phone_no')</th>
                        <td>{{ $merchant->phone_no }}</td>
                    </tr>
                    <tr>
                        <th>@lang('dict.processing_fee')</th>
                        <td>{{ number_format($merchant->merchantProfile->processing_fee, 2) }} %</td>
                    </tr>
                    <tr>
                        <th colspan="2">
                            @can(App\RoleEnum::MERCHANT_EDIT)
                            <button class='btn btn-brand mt-2' id='btnEditMerchant'>@lang('dict.edit_details')</button>
                            @endcan
                            @hasrole(App\Enum::ROLE_SUPER_ADMIN)
                            <button class='btn btn-brand mt-2' id='btnResetPassword'>@lang('dict.reset_pw')</button>
                            @else
                            @can(App\RoleEnum::MERCHANT_EDIT)
                            <button class='btn btn-brand mt-2' id='btnChangePassword'>@lang('dict.chg_pw')</button>
                            @endcan
                            @endhasrole
                        </th>
                    </tr>
                </table>

            </div>
        </div>

        @if(Auth::user()->hasRole(App\Enum::ROLE_SUPER_ADMIN))
        <button id='btnDelete' class='btn btn-danger' type='button'>@lang('dict.delete_merchant')</button>
        @endif
    </div>
    <div class="col-xl-9">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-shops-tab" data-toggle="pill" href="#pills-shops" role="tab"
                    aria-controls="pills-shops" aria-selected="false">@lang('dict.shops')
                    {{ count($shops) > 0 ? '(' . count($shops) . ')' : '' }}</a>
            </li>

            @canany([App\RoleEnum::SUBMERCHANT_VIEW])
            <li class="nav-item">
                <a class="nav-link" id="pills-sub-merchants-tab" data-toggle="pill" href="#pills-sub-merchants"
                    role="tab" aria-controls="pills-home" aria-selected="true">@lang('dict.sub_merchants')
                    {{ count($subMerchants) > 0 ? '(' . count($subMerchants) . ')' : '' }}</a>
            </li>
            @endcanany
        </ul>

        <div class="tab-content p-0 bg-transparent" id="pills-tabContent">
            <div class="tab-pane show active" id="pills-shops" role="tabpanel">
                <div class="table-responsive-sm">
                <table class='table table-brand'>
                    <thead>
                        <tr>
                            <th>@lang('dict.shop_name')</th>
                            <th>@lang('dict.api_code')</th>
                            <th>@lang('dict.api_secret')</th>
                            <th>@lang('dict.callback_url')</th>
                            <th>@lang('dict.response_url')</th>
                            <th width='1%'>@lang('dict.status')</th>
                            <th>@lang('dict.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($shops AS $s)
                        <tr>
                            <td>
                                <a href="/shops/{{ $s->uuid }}/edit">{{ $s->shop_name }}</a>
                            </td>
                            <td>{{ $s->api_code }}</td>
                            <td>{{ $s->api_secret }}</td>
                            <td>{{ $s->callback_url }}</td>
                            <td>{{ $s->response_url }}</td>
                            <td>{!! App\Utility::statusToHtml($s->status) !!}</td>
                            <td>
                                {!! App\Utility::getShopActions($s->uuid, $s->status) !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>

                @can(App\RoleEnum::SHOP_CREATE)
                <a href="{{ action('ShopController@createShop', $merchant->user_id) }}"
                    class='btn btn-brand mb-3'>@lang('dict.create_shop')</a>
                @endcan

            </div>
            @canany([App\RoleEnum::SUBMERCHANT_VIEW])
            <div class="tab-pane" id="pills-sub-merchants" role="tabpanel">
                <table class='table table-brand'>
                    <thead>
                        <tr>
                            <th width='1%' class='nowrap'>@lang('dict.name')</th>
                            <th>@lang('dict.username')</th>
                            <th width='1%' class='nowrap'>@lang('dict.status')</th>
                            <th>@lang('dict.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subMerchants AS $sm)
                        <tr>
                            <td class='nowrap'>
                                <a href="{{ action('SubmerchantController@edit', $sm->user_id) }}">{{ $sm->name }}</a>
                            </td>
                            <td>{{ $sm->username }}</td>
                            <td width='1%'>{!! App\Utility::statusToHtml($sm->status) !!}</td>
                            <td width='1%'>{!! App\Utility::subMerchantActions($sm->user_id, $sm->status) !!}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                @can(App\RoleEnum::SUBMERCHANT_CREATE)
                <a href="{{ action('SubmerchantController@create', $merchant->user_id) }}"
                    class='btn btn-brand mb-3'>@lang('dict.create_sub_merchant')</a>
                @endcan

            </div>
            @endcanany
        </div>
    </div>
</div>

@component('components.edit-merchant', ['merchant' => $merchant])
@endcomponent

@component('components.change-password', ['url' => action('MerchantController@changePassword', $merchant->user_id)])
@endcomponent

@component('components.reset-password')
@endcomponent

@stop

@push('page_script')
<script>
    $(function(){
        $("#btnEditMerchant").click(function(){
            editMerchant("{{ $merchant->user_id }}")
            .done((response) => {
                fillMerchantInfo(response.data);
              
                $("#modalEditMerchant").modal('toggle');
            })
            .fail((response) => {

            });
        });

        $("#editEmail").change(function(){
            validateInfo($(this).val())
            .done((response) => {
                if(!response.success){
                    notifyError(response.message);
                    $("#btnUpdate").prop('disabled', true);
                } else {
                    $("#btnUpdate").prop('disabled', false);
                }
            })
            .fail((response) => {
                notifyError(response.message);
            });
        });

        $("#btnChangePassword").click(function(){
            $("#modalChangePassword").modal('toggle');
        });

        $("#btnChgPassword").click(function(){
            var oldPw = $("#chgOldPassword").val();
            var pw = $("#chgPassword").val();

            changePassword(oldPw, pw)
            .done((response) => {
                if(response.success){
                    notifySuccess(response.message);
                    $("#modalChangePassword").modal('toggle');
                } else {
                    notifyError(response.message);
                }
            });
        });

        function editMerchant(merchant){
            return $.ajax({
                url: `/merchants/edit/${merchant}`,
                type: "GET"
            })
        }

        function fillMerchantInfo(merchant){
            $("#editName").val(merchant.name);
            $("#editUsername").val(merchant.username);
            $("#editEmail").val(merchant.email);
            $("#editPhoneNo").val(merchant.phone_no);
            $("#btnUpdate").prop('disabled', false);
        }

        function validateInfo(email){
            return $.ajax({
                url: "{{ action('MerchantController@validateDetails') }}",
                type: "POST",
                data: {
                    merchant: "{{ $merchant->user_id }}",
                    email
                }
            })
        }

        function changePassword(oldPassword, password){
            return $.ajax({
                url: "{{ action('MerchantController@changePassword') }}",
                type: "POST",
                data: {
                    merchant: "{{ $merchant->user_id }}",
                    oldPassword,
                    password
                }
            })
        }

        $(document).on('click', '.btn-disable-shop', function(){
            var shop = $(this).closest('.shop-action-menu').attr('data-shop');
            var action = $(this).attr('data-action').toLowerCase();

            confirmation('', `@lang('dict.confirm') ${action} @lang('dict.selected_shop')?`)
            .then((result) => {
                if(result.value){
                    disableShop(shop)
                    .done((response) => {
                        if(response.success){
                           location.reload();
                        } else {
                            notifyError(response.message);
                        }
                    })
                    .fail((response) => {
                        notifyError(response.message);
                    });
                }
            });
        });

        function disableShop(shop){
            return $.ajax({
                url: "{{ action('ShopController@disableShop') }}",
                method: "POST",
                data: {
                    shop
                }
            })
        }

        $(document).on('click', '.btn-submerchant-update-status', function(){
            var user = $(this).closest('.submerchant-action-menu').attr('data-submerchant');
            var action = $(this).attr('data-action').toLowerCase();

            confirmation('', `@lang('dict.confirm') ${action} @lang('dict.selected_submerchant')?`)
            .then((result) => {
                if(result.value){
                    disableSubMerchant(user)
                    .done((response) => {
                        if(response.success){
                            location.reload();
                        } else {
                            notifyError(response.message);
                        }
                    })
                    .fail((response) => {
                        notifyError(response.message);
                    });
                }
            });

        });

        function disableSubMerchant(user)
        {
        return $.ajax({
            url: "{{ action('SubmerchantController@updateStatus') }}",
            method: "POST",
            data: {
                user
            }
        })
        }

        @if(Auth::user()->hasRole(App\Enum::ROLE_SUPER_ADMIN))
        $("#btnDelete").click(function(){
            confirmation('', `@lang('dict.confirm_del_merchant')?`)
            .then((result) => {
                if(result.value) {
                    deleteShop()
                    .then((response) => {
                        if(response.success) {
                            window.location.replace(response.data);
                        } else {
                            notifyError(response.message);
                        }
                    });
                }
            });
        });

        function deleteShop()
        {
            return $.ajax({
                type: "POST",
                url: "{{ action('MerchantController@destroy') }}",
                data: {
                    user: '{{ $merchant->user_id }}'
                }
            })
        }

        $("#btnResetPassword").click(function(){
            toggleResetModal();
        });

        $("#btnResetPasswordSubmit").click(function(){
            var pw = $("#resetPassword").val();

            resetPassword(pw)
            .done((response) => {
                if(response.success){
                    notifySuccess(response.message);
                    toggleResetModal();
                } else {
                    notifyError(response.message);
                }
            })
            .fail((response) => {
                notifyError(response.message);
            });
        });

        function toggleResetModal()
        {
            $("#modalResetPassword form").trigger("reset");
            $("#modalResetPassword").modal('toggle');
        }

        function resetPassword(password){
            return $.ajax({
                url: "{{ action('MerchantController@resetPassword') }}",
                type: "POST",
                data: {
                    merchant: "{{ $merchant->user_id }}",
                    password
                }
            })
        }
        @endif

    });
</script>
@endpush