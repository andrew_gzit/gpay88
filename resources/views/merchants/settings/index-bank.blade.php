@extends('layouts.merchant')
@section('title', __('dict.banks'))
@section('header', __('dict.banks'))

@section('content')
<table class="table table-brand">
    <thead>
        <tr>
            <th>@lang('dict.shop_name')</th>
            <th>@lang('dict.bank_name')</th>
            <th>@lang('dict.bank_acc_no')</th>
            <th>@lang('dict.bank_holder_name')</th>
            <th>@lang('dict.min_threshold')</th>
            <th>@lang('dict.max_threshold')</th>
            {{-- <th>@lang('dict.accumulated_threshold')</th> --}}
            <th width='1%'>@lang('dict.status')</th>
            @canany([App\RoleEnum::BANK_EDIT, App\RoleEnum::BANK_DELETE])
            {{-- <th>@lang('dict.action')</th> --}}
            @endcanany
        </tr>
    </thead>
    <tbody id='tbodyShopBanks'>
        @foreach($shopBanks AS $sb)
        <tr data-id='{{ $sb->id }}'>
            <td>{{ $sb->shop_name }}</td>
            <td>{{ $sb->abbr }}</td>
            <td>{{ $sb->bank_acc_no }}</td>
            <td>{{ $sb->bank_acc_holder }}</td>
            <td>RM {{ number_format($sb->min_threshold, 2) }}</td>
            <td>RM {{ number_format($sb->max_threshold, 2) }}</td>
            {{-- <td>RM {{ number_format($sb->accumulated_threshold, 2) }}</td> --}}
            <td>{!! $sb->toggleHtml() !!}</td>
            @canany([App\RoleEnum::BANK_EDIT, App\RoleEnum::BANK_DELETE])
            {{-- <td>{!! $sb->shopBankActions() !!}</td> --}}
            @endcanany
        </tr>
        @endforeach
    </tbody>
</table>
@stop

@push('page_script')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
    rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<script>
    $(function(){
        $(document).on('change', '.btn-update-status', function(){
            var shop = $(this).closest('.shop-action-menu').attr('data-shop');
            var action = $(this).attr('data-action').toLowerCase();

            var bank = $(this).attr('data-shopbank');

            var toggleElem = $(this);
            var prevState = $(this).is(":checked") ? 'off' : 'on';

            confirmation('', `@lang('dict.confirm') ${action} @lang('dict.selected_shop')?`)
            .then((result) => {
                if(result.value){
                    updateStatus(bank)
                    .done((response) => {
                        if(response.success){
                            notifySuccess(response.message);
                        } else {
                            notifyError(response.message);
                            toggleElem.bootstrapToggle(prevState, true);
                        }
                    })
                    .fail((response) => {
                        notifyError(response.message);
                        toggleElem.bootstrapToggle(prevState, true);
                    });
                } else {
                    toggleElem.bootstrapToggle(prevState, true);
                }
             });
        });

        function updateStatus(shopBank)
        {
            return $.ajax({
                url: "{{ action('ShopController@updateBankStatus') }}",
                type: "POST",
                data: {
                    shopBank
                }
            })
        }

    });
</script>

@endpush