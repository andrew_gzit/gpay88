@extends('layouts.merchant')
@section('title', 'Bank Settings')
@section('header', 'Bank Settings')

@section('content')
<form action="{{ action('SettingsController@storeBankSettings') }}" method='POST'>
    @csrf
    <div class="panel">
        <div class="panel-heading ui-sortable-handle">
            <h4 class="panel-title">Add Bank</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xl-5">
                    <div class="form-group row">
                        <label for="bankName" class="col-sm-4 col-form-label">Bank Name</label>
                        <div class="col-sm-8">
                            <select name="bank" id="bankName" class='form-control form-control-lg'>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bankAccNum" class="col-sm-4 col-form-label">Bank Account No.</label>
                        <div class="col-sm-8">
                            <input type="text" required name='bankAccNum' id='bankAccNum'
                                class='form-control form-control-lg text-uppercase'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bankAccNum" class="col-sm-4 col-form-label">Bank Account Holder</label>
                        <div class="col-sm-8">
                            <input type="text" required name='bankAccHolder' id='bankAccHolder'
                                class='form-control form-control-lg text-uppercase'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button class='btn btn-brand'>Save</button>
        </div>
    </div>
</form>
@stop

@push('page_script')
<script>
    $(function(){

    });
</script>
@endpush