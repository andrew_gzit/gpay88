<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') - {{ env('APP_NAME') }}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/fh-3.1.6/datatables.min.css" />
    <link rel="stylesheet" type="text/css" href="/plugins/css/parsley.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/notyf@3.1.0/notyf.min.css">

    <link href="/assets/admin/css/animate.css" rel="stylesheet" />
    <link href="/assets/admin/css/default.css" rel="stylesheet" id="theme">
    <link href="/assets/admin/css/style.min.css" rel="stylesheet">
    <link href="/assets/admin/css/style-responsive.min.css" rel="stylesheet">
    <link href="/assets/admin/css/custom.css" rel="stylesheet">
    <link href="/assets/admin/css/custom-responsive.css" rel="stylesheet">
    
</head>

<body>

    <div id="page-loader" class="fade show"><span class="spinner"></span></div>

    <div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <div id="header" class="header navbar-default">
            <!-- begin navbar-header -->
            <div class="navbar-header">
                <a href="{{ action('MerchantController@home') }}" class="navbar-brand">{{ env('APP_NAME') }}</a>

                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="navbar-nav navbar-right language-bar language-bar-mobile">
                    <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><img src='/assets/images/{{ App::getLocale() }}.svg' /></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="javascript:void(0);" data-language='en' class="change-language dropdown-item"><img src='/assets/images/en.svg' />  English</a>
                            <a href="javascript:void(0);" data-language='zh' class="change-language dropdown-item"><img src='/assets/images/zh.svg' /> 简体中文</a>
                        </div>
                    </li>
                </ul>

            </div>
            <!-- end navbar-header -->

            <!-- begin header-nav -->
            <ul class="navbar-nav navbar-right">
                {{-- <li>
                    <form class="navbar-form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter keyword" />
                            <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                        <i class="fa fa-bell"></i>
                        <span class="label">5</span>
                    </a>
                    <ul class="dropdown-menu media-list dropdown-menu-right">
                        <li class="dropdown-header">NOTIFICATIONS (5)</li>
                        <li class="media">
                            <a href="javascript:;">
                                <div class="media-left">
                                    <i class="fa fa-bug media-object bg-silver-darker"></i>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">Server Error Reports <i
                                            class="fa fa-exclamation-circle text-danger"></i></h6>
                                    <div class="text-muted f-s-11">3 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:;">
                                <div class="media-left">
                                    <img src="../assets/img/user/user-1.jpg" class="media-object" alt="" />
                                    <i class="fab fa-facebook-messenger text-primary media-object-icon"></i>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">John Smith</h6>
                                    <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                    <div class="text-muted f-s-11">25 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:;">
                                <div class="media-left">
                                    <img src="../assets/img/user/user-2.jpg" class="media-object" alt="" />
                                    <i class="fab fa-facebook-messenger text-primary media-object-icon"></i>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">Olivia</h6>
                                    <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                    <div class="text-muted f-s-11">35 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:;">
                                <div class="media-left">
                                    <i class="fa fa-plus media-object bg-silver-darker"></i>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading"> New User Registered</h6>
                                    <div class="text-muted f-s-11">1 hour ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:;">
                                <div class="media-left">
                                    <i class="fa fa-envelope media-object bg-silver-darker"></i>
                                    <i class="fab fa-google text-warning media-object-icon f-s-14"></i>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading"> New Email From John</h6>
                                    <div class="text-muted f-s-11">2 hour ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-footer text-center">
                            <a href="javascript:;">View more</a>
                        </li>
                    </ul>
                </li> --}}

                <ul class="navbar-nav navbar-right language-bar">
                    <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><img src='/assets/images/{{ App::getLocale() }}.svg' /></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="javascript:void(0);" data-language='en' class="change-language dropdown-item"><img src='/assets/images/en.svg' />  ENGLISH</a>
                            <a href="javascript:void(0);" data-language='zh' class="change-language dropdown-item"><img src='/assets/images/zh.svg' /> 简体中文</a>
                        </div>
                    </li>
                </ul>

                <li class="dropdown navbar-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- <img src="../assets/img/user/user-13.jpg" alt="" /> -->
                        <span class="d-none d-md-inline">{{ Auth::user()->username }}</span> <b class="caret"></b>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        {{-- <a href="javascript:;" class="dropdown-item">Edit Profile</a>
                        <a href="javascript:;" class="dropdown-item"><span
                                class="badge badge-danger pull-right">2</span> Inbox</a>
                        <a href="javascript:;" class="dropdown-item">Calendar</a>
                        <a href="javascript:;" class="dropdown-item">Setting</a>
                        <div class="dropdown-divider"></div> --}}
                        <a href="javascript:void(0);" class="logout-link dropdown-item">@lang('dict.log_out')</a>
                    </div>
                </li>
            </ul>
            <!-- end header navigation right -->
        </div>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <div id="sidebar" class="sidebar">
            <!-- begin sidebar scrollbar -->
            <div data-scrollbar="true" data-height="100%">
                <!-- begin sidebar user -->
                {{-- <ul class="nav">
                    <li class="nav-profile">
                        <a href="javascript:;" data-toggle="nav-profile">
                            <div class="cover with-shadow"></div>
                            <div class="image">
                                <img src="../assets/img/user/user-13.jpg" alt="" />
                            </div>
                            <div class="info">
                                <b class="caret pull-right"></b>
                                Sean Ngu
                                <small>Front end developer</small>
                            </div>
                        </a>
                    </li>
                    <li>
                        <ul class="nav nav-profile">
                            <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
                            <li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
                        </ul>
                    </li>
                </ul> --}}
                <!-- end sidebar user -->
                <!-- begin sidebar nav -->
                <ul class="nav mt-3">
                    <li class="nav-header">@lang('dict.general')</li>
                     <li class="{{ Request::is('home') ? 'active' : '' }}">
                        <a href="{{ action('MerchantController@home' )}}">
                            <i class="las la-home"></i>
                            <span>@lang('dict.home')</span>
                        </a>
                    </li>

                    <!-- <li class="{{ Request::is('merchant/reports') ? 'active' : '' }}">
                        <a href="{{ action('MerchantController@home' )}}">
                            <i class="las la-file-alt"></i>
                            <span>Add Merchant</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('merchant/reports') ? 'active' : '' }}">
                        <a href="{{ action('MerchantController@home' )}}">
                            <i class="las la-file-alt"></i>
                            <span>Add Shop</span>
                        </a>
                    </li> -->
                    @can(App\RoleEnum::MERCHANT_VIEW)
                    <li class="{{ Request::is('merchants*') ? 'active' : '' }}">
                        <a href="{{ action('MerchantController@merchants' )}}">
                            <i class="las la-user-tie"></i>
                            <span>@lang('dict.merchants')</span>
                        </a>
                    </li>
                    @endcan

                    @can(App\RoleEnum::SHOP_VIEW)
                    <li class="{{ Request::is('shops*') ? 'active' : '' }}">
                        <a href="{{ action('ShopController@shops' )}}">
                            <i class="las la-store"></i>
                            <span>@lang('dict.shops')</span>
                        </a>
                    </li>
                    @endcan

                    @can(App\RoleEnum::BANK_VIEW)
                    <li class="nav-header">@lang('dict.settings')</li>
                    <li class="{{ Request::is('settings/bank') ? 'active' : '' }}">
                        <a href="{{ action('SettingsController@indexBank' )}}">
                            <i class="las la-university"></i>
                            <span>@lang('dict.banks')</span>
                        </a>
                    </li>
                    @endcan

                    @can(App\RoleEnum::TRANSACTION_VIEW)
                    <li class="nav-header">@lang('dict.reports')</li>
                    <li class="{{ Request::is('reports/transactions*') ? 'active' : '' }}">
                        <a href="{{ action('ReportController@transactions' )}}">
                            <i class="las la-file-alt"></i>
                            <span>@lang('dict.trans_report')</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('reports/statement*') ? 'active' : '' }}">
                        <a href="{{ action('ReportController@statement' )}}">
                        <i class="las la-newspaper"></i>
                            <span>@lang('dict.statement')</span>
                        </a>
                    </li>
                    @endcan

                    <li>
                        <a class='logout-link' href="javascript:void(0);">
                        <i class="las la-sign-out-alt"></i>
                            <span>@lang('dict.log_out')</span>
                        </a>
                    </li>

                    <!-- <li class="nav-header">@lang('dict.billing')</li>
                    <li class="{{ Request::is('merchant/reports/invoices*') ? 'active' : '' }}">
                        <a href="{{ action('InvoiceController@index' )}}">
                        <i class="las la-file-invoice-dollar"></i>
                            <span>@lang('dict.invoices')</span>
                        </a>
                    </li> -->

                    <!-- <li class="{{ Request::is('merchant/settings/access-control') ? 'active' : '' }}">
                        <a href="{{ action('SettingsController@accessControl' )}}">
                            <i class="las la-user-shield"></i>
                            <span>Access Control</span>
                        </a>
                    </li> -->
                </ul>

                <p class="version">V 1.0.8</p>

            </div>
        </div>

        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <div class="pull-right">
                @yield('breadcrumb')
            </div>
            {{-- <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Page Options</a></li>
                <li class="breadcrumb-item active">Blank Page</li>
            </ol> --}}
            <!-- end breadcrumb -->
            <h1 class="page-header">
                @yield('header')
                @section('header-small')
                <small>@yield('header-small')</small>
                @stop
            </h1>

            @yield('content')

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.1/parsley.min.js"></script>

    <script src="/js/apps.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/fh-3.1.6/datatables.min.js">
    </script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script> --}}

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"
        integrity="sha256-d/edyIFneUo3SvmaFnf96hRcVBcyaOy96iMkPez1kaU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css"
        integrity="sha256-FdatTf20PQr/rWg+cAKfl6j4/IY3oohFAJ7gVC3M34E=" crossorigin="anonymous" /> --}}
    {{--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"
        integrity="sha256-tSRROoGfGWTveRpDHFiWVz+UXt+xKNe90wwGn25lpw8=" crossorigin="anonymous"></script> --}}

    <script src="https://cdn.jsdelivr.net/npm/notyf@3.1.0/notyf.min.js"></script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            App.init();

            $(".change-language").click(function(){
                const curLocale = "{{ App::getLocale() }}";
                var newLocale = $(this).attr('data-language');
                if(curLocale !== newLocale){
                    $.post(`{{ action('AccountController@changeLanguage') }}`, { locale: newLocale }).then(function(response){
                        location.reload();
                    });
                }
            });

            $('.logout-link').click(function(){
                confirmation('', 'Logout?')
                .then((result) => {
                    if(result.value){
                        window.location.replace("{{ action('AccountController@logout') }}");
                    }
                });
            });

        });

        function confirmation(title, text)
        {
            return Swal.fire({
                title: title,
                text: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "{{ __('dict.confirm') }}",
                cancelButtonText: "{{ __('dict.cancel') }}",
                focusCancel: true
                });
        }

        const notyf = new Notyf();

        function notifySuccess(msg, duration = 5000){
            notyf.success({
                message: msg,
                duration
            });
        }

        function notifyError(msg, duration = 5000){
            notyf.error({
                message: msg,
                duration
            });
        }

        @if(Session::has('msg'))
            @if(Session::get('css') == 'success')
                notifySuccess("{{ Session::get('msg') }}");
            @else
                notifyError("{{ Session::get('msg') }}");
            @endif
        @endif

    </script>

    @stack('page_script')

</body>
