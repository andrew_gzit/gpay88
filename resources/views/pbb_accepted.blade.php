@extends('pbb_layout2')

@section('content')
<div class="page-center">
    <div class="page-content-full">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <img src="/pbe_logo.png">
                    <ul class="nav navbar-nav pull-right">
                        <img alt="" id="ebanking-logo" src="/fpxLogo.png" class="pull-right">
                    </ul>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-name">
                                <h4>Public Bank FPX Payment Agent</h4>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="portlet">
                            <img src="/bar_selected.jpg"><img src="/bar_line.jpg"><img src="/bar_selected.jpg">
                        </div>
                    </div>
                    <div class="margin-top-20">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet-body form">
                                <form class="form-horizontal" name="frm1" method="POST" autocomplete="off">
                                    <div class="form-body">

                                        <p>Your transaction <b>has been accepted</b>. Please take not of the
                                            <strong>Reference Number</strong> for future correspondence.</p>

                                        <div class="row">
                                            <label class="col-md-3 control-label">Reference Number</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    2001081727520566
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">From Account</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    1234
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Date/Time</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    08-01-2020 18:09:00
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="inner-note-message margin-left-5">
                                                <p><b>Transaction Details :-</b></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Transaction ID</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    2001081727520566
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Serial Number</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    1
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Seller ID</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    SE00033122
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Seller Description</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    LAZADA
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Order Number</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    RT200086287187
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Amount</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    MYR5.69
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label">Service Charge</label>
                                            <div class="col-md-9">
                                                <div class="label-inline1">
                                                    MYR0.00
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-actions fluid">
                                                <div class="col-md-6 col-md-6">

                                                    <button type="button" class="btn red">Return to FPX</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="footer-size">
        <div class="footer-inner">
            <a href="http://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Terms-Conditions.aspx"
                target="_blank">Terms &amp; Conditions</a> |
            <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Privacy.aspx"
                target="_blank">Privacy</a> |
            <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/Security-Policy.aspx"
                target="_blank">Security</a> |
            <a href="https://www.pbebank.com/Personal-Banking/Banking/E-Channel/PBe-Online-Banking/e-Banking-Charter.aspx"
                target="_blank"> e-Banking Charter</a> |
            <a href="http://www.bnm.gov.my" target="_blank">Bank Negara Malaysia</a>
        </div>
        <div class="footer-tools">
            <div class="col-sm-12">Copyright © <script>
                    document.write(new Date().getFullYear());
                </script> Public Bank Berhad (6463-H) ALL RIGHTS RESERVED</div>
        </div>
    </div>
</div>
@stop