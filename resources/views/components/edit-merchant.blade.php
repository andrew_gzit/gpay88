<div class="modal" id='modalEditMerchant' tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content">
            <form id='formUpdateMerchant' action="{{ action('MerchantController@updateMerchant', $merchant->user_id) }}" method='POST'>
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dict.edit_merchant_details')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">@lang('dict.username')</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly='' id='editUsername' class='form-control form-control-lg form-control-plaintext'
                                        value='{{ $merchant->username }}'>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">@lang('dict.name')</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly='' id='editName' class='form-control form-control-lg form-control-plaintext'
                                        value='{{ $merchant->name }}'>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="editEmail" class="col-sm-4 col-form-label">@lang('dict.email_add')</label>
                                <div class="col-sm-8">
                                    <input type="email" required name='email' id='editEmail'
                                class='form-control form-control-lg' value='{{ $merchant->email }}'>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="editPhoneNo" class="col-sm-4 col-form-label">@lang('dict.phone_no')</label>
                                <div class="col-sm-8">
                                    <input type="text" required name='phone_no' id='editPhoneNo'
                                class='form-control form-control-lg' value='{{ $merchant->phone_no }}'>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="processing_fee" class="col-sm-4 col-form-label">@lang('dict.processing_fee')</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="number" step='0.01' style='max-width: 5rem' max="100" min="0" required
                                            name='processing_fee' id='processing_fee' value='{{ $merchant->merchantProfile->processing_fee }}' class='form-control form-control-lg'>
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-brand" id='btnUpdate'>@lang('dict.update')</button>
                    <button type="button" class="btn btn-brand-inverse" data-dismiss="modal">@lang('dict.close')</button>
                </div>
            </form>

        </div>

    </div>
</div>

@push('page_script')
<script>
    $(function(){
        $("#formUpdateMerchant").parsley({
            errorsContainer: function(el) {
                return el.$element.closest('.col-sm-8');
            },
        });
    });
</script>
@endpush