<div class="modal" id='modalChangePassword' tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <form action="{{ $url ?? '' }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">@lang('dict.chg_pw')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label for='chgOldPassword' class="col-sm-4 col-form-label">@lang('dict.old_pw')</label>
                                <div class="col-sm-8">
                                    <input type="password" required id='chgOldPassword' class='form-control form-control-lg'>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for='chgPassword' class="col-sm-4 col-form-label">@lang('dict.new_pw')</label>
                                <div class="col-sm-8">
                                    <input type="password" required minlength='6' id='chgPassword' class='form-control form-control-lg'>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for='chgConfNewPassword' class="col-sm-4 col-form-label">@lang('dict.c_new_pw')</label>
                                <div class="col-sm-8">
                                    <input type="password" required minlength='6' id='chgConfNewPassword' class='form-control form-control-lg'>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-brand" id='btnChgPassword'>Submit</button>
                    <button type="button" class="btn btn-brand-inverse" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>