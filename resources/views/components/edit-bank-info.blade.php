<div class="modal" id="modalEditBankInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content ">
            <form id='formEditBankInfo' onsubmit="return false">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">@lang('dict.edit_bank_info')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label for="edit_bank_name" class="col-sm-5 col-form-label">@lang('dict.bank_name')</label>
                                <div class="col-sm-7">
                                    <select readonly id="edit_bank_name" class='form-control form-control-lg'>
                                        @foreach($banks AS $b)
                                        <option data-abbr='{{ $b->abbr }}' data-name='{{ $b->name }}'
                                            value="{{ $b->id }}">
                                            {{ $b->abbr . ' - ' . $b->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edit_account_no" class="col-sm-5 col-form-label">@lang('dict.bank_acc_no')</label>
                                <div class="col-sm-7">
                                    <input type="text" required='' name='edit_account_no' id='edit_account_no'
                                        class='edit-bank-input form-control form-control-lg'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edit_holder_name" class="col-sm-5 col-form-label">@lang('dict.bank_holder_name')</label>
                                <div class="col-sm-7">
                                    <input type="text" required='' name='edit_holder_name' id='edit_holder_name'
                                        class='edit-bank-input form-control form-control-lg'>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <hr>
                        </div>
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label for="edit_min_threshold" class="col-sm-5 col-form-label">@lang('dict.min_threshold')</label>
                                <div class="col-sm-7">
                                    <input type="number" required min='1' name='min_threshold' id='edit_min_threshold'
                                        class='edit-bank-input form-control form-control-lg'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edit_max_threshold" class="col-sm-5 col-form-label">@lang('dict.max_threshold')</label>
                                <div class="col-sm-7">
                                    <input type="number" required name='max_threshold' id='edit_max_threshold'
                                        class='edit-bank-input form-control form-control-lg'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edit_accumulated_threshold" class="col-sm-5 col-form-label">@lang('dict.accumulated_threshold')</label>
                                <div class="col-sm-7">
                                    <input type="number" name='accumulated_threshold' id='edit_accumulated_threshold'
                                        class='edit-bank-input form-control form-control-lg'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edit_status" class="col-sm-5 col-form-label">@lang('dict.status')</label>
                                <div class="col-sm-7">
                                    <select class='form-control form-control-lg' id="edit_status">
                                        <option value="{{ App\Enum::STATUS_ACTIVE }}">@lang('dict.active')</option>
                                        <option value="{{ App\Enum::STATUS_INACTIVE }}">@lang('dict.inactive')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="text" hidden id='edit_bank'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('dict.close')</button>
                    <button type="submit" id='btnUpdateBankInfo' class="btn btn-brand">@lang('dict.save_changes')</button>
                </div>
            </form>
        </div>
    </div>
</div>