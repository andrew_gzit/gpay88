@extends('cimb_layout2')

@section('content')
<div class="e-payment-apply">
    <div class="payment-row epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-8 padding-zero all">
                <strong class="group-title">To</strong>
                <small class="payment-grey-text">Recipient info</small>
                <p><strong>ALIPAY MALAYSIA</strong></p>
            </div>
            <div class="col-md-4 text-right hidden-xs hidden-sm padding-zero all">
                <strong class="group-title">Amount</strong>
                <p class="e-payment-apply-prize">
                    <strong>
                        <span class="positive-number">
                            <span class="currency">MYR</span>
                            <span class="whole-number">
                                <span class="positive-number">5</span><span>.</span><span class="decimal-part">65</span>
                            </span>
                            <span></span>
                        </span>
                    </strong>
                </p>
            </div>
            <div class="col-md-8 line-plus padding-zero all">
                <div>
                    <small class="payment-grey-text">Transaction ID</small>
                    <p><strong>165554145</strong></p>
                </div>
            </div>
            <div>
                <div class="col-md-8 line-plus padding-zero all">
                    <small class="payment-grey-text">Payment Of </small>
                    <p>LAZADA</p>
                </div>
            </div>
        </div>
    </div>

    <div class="payment-row epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-8 padding-zero all">
                <strong class="group-title">From</strong>
                <small class="payment-grey-text">Account Holder Name</small>
                <p><strong>XXX</strong></p>
            </div>
            <div class="col-md-8 line-plus padding-zero all">
				<small class="payment-grey-text">Account</small>
				<div>
					<strong>SA ACC</strong><span> </span><span>10234546</span>
				</div>
			</div>
        </div>
    </div>

    <div class="payment-row visible-xs visible-sm epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-6 padding-zero all">
                <strong class="group-title">Amount</strong>
                <p class="e-payment-apply-prize">
                    <strong>
                        <number-cmp>
                            <!---->
                            <!----><span class="positive-number">
                                <span class="currency">MYR</span>
                                <span class="whole-number">

                                    <span class="positive-number">5</span>
                                    <!----><span>.</span>
                                    <!----><span class="decimal-part">65</span>
                                </span>

                                <span></span>
                            </span>
                            <!---->
                        </number-cmp>
                    </strong>
                </p>

            </div>
        </div>
    </div>

</div>

<div class="cimb-container">
    <div class="row button-group">
        <div class="col-xs-12 col-sm-6 pull-right text-right btn-submit">
            <a class="btn btn-link hidden-xs hidden-sm"><strong class="text">Cancel</strong></a>
            <button class="btn btn-primary" type="submit">Submit</button>
            <a class="btn btn-link visible-xs visible-sm margin-zero left"><strong class="text">Cancel</strong></a>
        </div>
    </div>
</div>
@stop