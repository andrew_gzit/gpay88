<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Maybank2u.com - Welcome</title>

    <link href='/mbb.css' type="text/css" rel='stylesheet' />
</head>

<body>

    <div id='content' class="font-family-lato font-size-base bg-gold px-content min-w-content">

        <div id="login" class="clearfix common_box_shadow max-width-xs mx-auto elevate-box">
            <div class="paddingdiv">
                <div>
                    <div class="paddingdiv">
                        <div class='pull-left'>
                            <img src="/mbb_logo.gif" oncontextmenu="return false;" onmousedown="return false;" ondrag="return false;">
                        </div>
                        <div class="clearfix">
                            <input type="submit" name="action" value="Cancel" onclick="cancel();" class="button transparent cancel font-family-lato">
                        </div>
                    </div>
                </div>

                @yield('content')
            
            </div>
        </div>

    </div>
    <div id="footer">
        <img src="/mbb_logo1.gif" alt="Maybank2u">
    </div>

</body>

</html>