<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GPAY88</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="gpay88.css">
</head>

<body>

    <div class="container">
        <div class="panel lifted">
            <div class="panel-header">
                <img src="/assets/images/logo.png" alt="GPAY88" class='logo'>
                <h1 class='title'>GPAY88</h1>
            </div>
            <div class='panel-body'>
                <h3 class='panel-body-title text-center'>Payment Details</h3>
                <div>
                    <table class='payment-details'>
                        <tr>
                            <th>Pay To</th>
                            <td>GZ Merchant</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>Wallet Reload</td>
                        </tr>
                        <tr>
                            <th>Amount</th>
                            <td>RM 10.00</td>
                        </tr>
                    </table>

                    <h3 class="sub-title">Select a bank</h3>
                    <div class='bank-list'>
                        <div class="bank selection">
                            <input type="radio" class='selection' name="bank" id="">
                            <img src="/mbb_logo.png" class='bank-logo' alt="">
                        </div>
                        <div class="bank selection">
                            <input type="radio" class='selection' name="bank" id="">
                            <img src="/cimb_logo.png" class='bank-logo' alt="">
                        </div>
                        <div class="bank selection">
                            <input type="radio" class='selection' name="bank" id="">
                            <img src="/pbb_logo.png" class='bank-logo' alt="">
                        </div>
                        <div class="bank selection">
                            <input type="radio" class='selection' name="bank" id="">
                            <img src="/hlb_logo.png" class='bank-logo' alt="">
                        </div>
                    </div>

                    <div class="button-group">
                        <button class='btn btn-brand btn-block'>Continue with payment</button>
                        <button class='btn btn-brand btn-brand-inverse btn-block'>Cancel</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <script>
        $(function(){
            $(".selection").click(function(){
                $(this).find('input').prop('checked', true);
            });
        });
    </script>

</body>

</html>