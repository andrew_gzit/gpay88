<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CIMB Clicks</title>

    <link rel="stylesheet" href="/cimb.css">
    <link rel="stylesheet" href="/cimb_common.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>

<body class="page-login windows desktop has-mobile-view">
    <div class="page-level1-container position-absolute">
        <div class="login-wrapper js-login-wrapper" style="height:100%;">
            <div class="main-content login-blink" id="main-content-login">

                <div class="banner-mob login-mob" style='margin-top: 4rem'>
                    <img alt="" class="banner" data-src="img/login-banner-1.png"
                        data-src-retina="img/login-banner-1.png" src="img/login-banner-mob1.png">
                    <div class="inner container-fluid">
                        <img alt="logo-mobile" class="logo-mob" src="/cimb.svg">

                        <h1 class="text-center tac-activation-title">TAC on App Activation</h1>
                        <!---->
                        <a class="back-btn left-btn btn-link tac-activation-cancel" href="javascript:void(0)">
                            <i class="icon icon--arrow-right"></i>
                        </a>
                        <a class="navbar-note" href="javascript:void(0)" style="display: none;" title="">
                            <span class="note-wrap">
                                <span class="notice" style="display: none;"></span>
                                <i class="icon icon--bell"></i>
                            </span>
                        </a>

                    </div>
                </div>
                <div class="scroll-wrapper scrollForm scrollbar scrollable scrollbar-inner override-scroller"
                    style="position: relative;">
                    <div class="scrollForm scrollbar scrollable scrollbar-inner override-scroller scroll-content"
                        style="height: 849px; margin-bottom: 0px; margin-right: 0px; max-height: none;">

                        <a class="logo"><img alt="logo-mobile" src="/cimb.svg" style="height:28px; width:175px;"></a>

                        @yield('content')

                        <div class="bgd-footer">&nbsp;</div>

                        <div class="tac-activation-cancel link-login text-center">
                            <a class="link-text link-size close-service" href="javascript:;">Cancel</a>
                        </div>
                    </div>
                    <div class="scroll-element scroll-x">
                        <div class="scroll-element_outer">
                            <div class="scroll-element_size"></div>
                            <div class="scroll-element_track"></div>
                            <div class="scroll-bar" style="width: 96px;"></div>
                        </div>
                    </div>
                    <div class="scroll-element scroll-y">
                        <div class="scroll-element_outer">
                            <div class="scroll-element_size"></div>
                            <div class="scroll-element_track"></div>
                            <div class="scroll-bar" style="height: 96px;"></div>
                        </div>
                    </div>
                </div>
                <div class="visible-xs">
                    <div class="mobile-footer" style="background-color:transparent !important;">&nbsp;</div>
                    <footer class="container-fluid">
                        <div class="nav-footer">
                            <span class="link-footer">All rights reserved. Copyright © 2020 <br>CIMB Bank Berhad
                                (13491-P)</span>
                        </div>
                    </footer>
                    <img class="img-footer" src="img/stripebar.gif" width="100%">
                </div>
            </div>
            <div class="carousel-banner" id="carousel-banner" style="z-index:1045 !important;">
                <span class="triangle-right"></span>
                <div class="carousel-content">

                    <div class="carousel-bg">
                        <img class="banner-application" src="img/transparent.png"
                            style="background-image: url(&quot;https://www.cimbclicks.com.my/resource.web/clicks.login.banner/wabawin-d.jpg&quot;); opacity: 0; display: block;">
                    </div>
                    <div class="carousel-bg">
                        <img class="banner-application active" src="img/transparent.png"
                            style="display: block; background-image: url(&quot;https://www.cimbclicks.com.my/resource.web/clicks.login.banner/CPL_1200x726.jpg&quot;); opacity: 1;">
                    </div>

                    <div class="owl-carousel carousel-inner swipe-banner-login noshadow owl-theme"
                        style="opacity: 1; display: block;">
                        <div class="owl-wrapper-outer">
                            <div class="owl-wrapper"
                                style="width: 5376px; left: 0px; display: block; transition: all 400ms ease 0s; transform: translate3d(-1344px, 0px, 0px);">
                                <div class="owl-item" style="width: 1344px;">
                                    <div class="item">
                                        <div class="carousel-caption noshadow banner-size">

                                            <h2 class="header-info noshadow" style="margin-top:0px">Join
                                                CIMB@WhatsApp soft launch programme &amp; stand to win
                                                LAZADA e-vouchers</h2>
                                            <p class="content-info noshadow">You're invited! Join our soft
                                                launch programme &amp; experience a new way to communicate
                                                with us.</p>

                                            <!---->
                                            <p class="action-info login-banner"><a class="btn-transparent"
                                                    target="_blank" type="button" href="../waba-win.html">Find
                                                    out more</a></p>
                                            <p class="tnc-info noshadow"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 1344px;">
                                    <div class="item">
                                        <div class="carousel-caption noshadow banner-size">

                                            <h2 class="header-info noshadow" style="margin-top:0px"><span
                                                    style="color:#000;font-size:4.8rem;">#MakeItHappen with
                                                    CIMB Cash Plus Personal Loan.</span></h2>
                                            <p class="content-info noshadow"><span style="color:#000;">Apply
                                                    online and get rates as low as 6.88%* p.a.!</span> </p>

                                            <!---->
                                            <p class="action-info login-banner"><a class="btn-transparent"
                                                    target="_blank" type="button"
                                                    href="https://cplform.cimbbank.com.my/forms/cpl/#/?cid=a1:pb_a2:loan_a3:cplgeneric_a4:010120_a5:clicks_a6:web_a7:pre_a8:login-banner">Apply
                                                    Now</a></p>
                                            <p class="tnc-info noshadow"><span style="color:#000;">*Terms
                                                    and conditions apply.</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="owl-controls clickable">
                            <div class="owl-pagination">
                                <div class="owl-page"><span class=""></span></div>
                                <div class="owl-page active"><span class=""></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hidden-xs hidden-sm login-bottom-footer">
                <footer class="footer container-fluid">
                    <div class="nav-footer">

                        <a class="link-footer" href="https://www.cimbclicks.com.my/terms-and-conditions.html"
                            target="_blank">Terms &amp; Conditions</a>
                        <a class="link-footer" href="https://www.cimbbank.com.my/en/personal/support/privacy.html"
                            target="_blank">Privacy</a>
                        <a class="link-footer" href="https://www.cimbclicks.com.my/e-banking-charter.html"
                            target="_blank">e-Banking Charter</a>

                        <br>
                        <span class="link-footer">Copyright © 2020 CIMB Bank Berhad (13491-P)</span>
                    </div>
                </footer>
            </div>
        </div>
    </div>
</body>

</html>