@extends('cimb_layout2')

@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<div class="e-payment-apply">

    <div class='epayPadding paymentSuccess'>
        <i class="fas fa-check-circle"></i>
        <div>
            <strong>Successful - Your account has been deducted</strong>
            <div>Ref 12345</div>
            <div>9 Jan 2020, 10:00:00 am</div>
        </div>
    </div>

    <div class="payment-row epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-8 padding-zero all">
                <strong class="group-title">To</strong>
                <small class="payment-grey-text">Recipient info</small>
                <p><strong>ALIPAY MALAYSIA</strong></p>
            </div>
            <div class="col-md-4 text-right hidden-xs hidden-sm padding-zero all">
                <strong class="group-title">Amount</strong>
                <p class="e-payment-apply-prize">
                    <strong>
                        <span class="positive-number">
                            <span class="currency">MYR</span>
                            <span class="whole-number">
                                <span class="positive-number">5</span><span>.</span><span class="decimal-part">65</span>
                            </span>
                            <span></span>
                        </span>
                    </strong>
                </p>
            </div>
            <div class="col-md-8 line-plus padding-zero all">
                <div>
                    <small class="payment-grey-text">Transaction ID</small>
                    <p><strong>165554145</strong></p>
                </div>
            </div>
            <div>
                <div class="col-md-8 line-plus padding-zero all">
                    <small class="payment-grey-text">Payment Of </small>
                    <p>LAZADA</p>
                </div>
            </div>
        </div>
    </div>

    <div class="payment-row epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-8 padding-zero all">
                <strong class="group-title">From</strong>
                <small class="payment-grey-text">Account Holder Name</small>
                <p><strong>XXX</strong></p>
            </div>
            <div class="col-md-8 line-plus padding-zero all">
                <small class="payment-grey-text">Account</small>
                <div>
                    <strong>SA ACC</strong><span> </span><span>10234546</span>
                </div>
            </div>
        </div>
    </div>



    <div class="payment-row visible-xs visible-sm epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-6 padding-zero all">
                <strong class="group-title">Amount</strong>
                <p class="e-payment-apply-prize">
                    <strong>
                        <number-cmp>
                            <!---->
                            <!----><span class="positive-number">
                                <span class="currency">MYR</span>
                                <span class="whole-number">

                                    <span class="positive-number">5</span>
                                    <!----><span>.</span>
                                    <!----><span class="decimal-part">65</span>
                                </span>

                                <span></span>
                            </span>
                            <!---->
                        </number-cmp>
                    </strong>
                </p>

            </div>
        </div>
    </div>

</div>

<div class="grey-box paybill-table-total">
    <div class="row">
        <div class="col-xs-6 col-md-6 paybill-row">
            <span class="paybill-label">Total</span>
        </div>
        <div class="col-xs-6 col-md-6 paybill-row text-right">
            <h3 class="paybill-label">
                <span class="positive-number">
                    <span class="currency">MYR</span>
                    <span class="whole-number">
                        <span class="positive-number">5</span><span>.</span><span class="decimal-part">65</span>
                    </span>
                    <span></span>
                </span>
            </h3>
        </div>
    </div>
</div>

<div class="cimb-container">
    <div class="row button-group">
        <div class="col-xs-12 col-sm-6 pull-right text-right btn-submit">
            <button class="btn btn-primary" type="submit">Continue with Transaction</button>
        </div>
    </div>
</div>
@stop