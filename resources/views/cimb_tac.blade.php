@extends('cimb_layout2')

@section('content')
<div class="e-payment-apply">
    <div class="payment-row epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-8 padding-zero all">
                <strong class="group-title">To</strong>
                <small class="payment-grey-text">Recipient info</small>
                <p><strong>ALIPAY MALAYSIA</strong></p>
            </div>
            <div class="col-md-4 text-right hidden-xs hidden-sm padding-zero all">
                <strong class="group-title">Amount</strong>
                <p class="e-payment-apply-prize">
                    <strong>
                        <span class="positive-number">
                            <span class="currency">MYR</span>
                            <span class="whole-number">
                                <span class="positive-number">5</span><span>.</span><span class="decimal-part">65</span>
                            </span>
                            <span></span>
                        </span>
                    </strong>
                </p>
            </div>
            <div class="col-md-8 line-plus padding-zero all">
                <div>
                    <small class="payment-grey-text">Transaction ID</small>
                    <p><strong>165554145</strong></p>
                </div>
            </div>
            <div>
                <div class="col-md-8 line-plus padding-zero all">
                    <small class="payment-grey-text">Payment Of </small>
                    <p>LAZADA</p>
                </div>
            </div>
        </div>
    </div>

    <div class="payment-row epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-8 padding-zero all">
                <strong class="group-title">From</strong>
                <small class="payment-grey-text">Account Holder Name</small>
                <p><strong>XXX</strong></p>
            </div>
            <div class="col-md-8 line-plus padding-zero all">
                <small class="payment-grey-text">Account</small>
                <div>
                    <strong>SA ACC</strong><span> </span><span>10234546</span>
                </div>
            </div>
        </div>
    </div>



    <div class="payment-row visible-xs visible-sm epayPadding dottedBottom">
        <div class="row">
            <div class="col-md-6 padding-zero all">
                <strong class="group-title">Amount</strong>
                <p class="e-payment-apply-prize">
                    <strong>
                        <number-cmp>
                            <!---->
                            <!----><span class="positive-number">
                                <span class="currency">MYR</span>
                                <span class="whole-number">

                                    <span class="positive-number">5</span>
                                    <!----><span>.</span>
                                    <!----><span class="decimal-part">65</span>
                                </span>

                                <span></span>
                            </span>
                            <!---->
                        </number-cmp>
                    </strong>
                </p>

            </div>
        </div>
    </div>

</div>

<div class="grey-box paybill-table-total">
    <div class="row">
        <div class="col-xs-6 col-md-6 paybill-row">
            <span class="paybill-label">Total</span>
        </div>
        <div class="col-xs-6 col-md-6 paybill-row text-right">
            <h3 class="paybill-label">
                <span class="positive-number">
                    <span class="currency">MYR</span>
                    <span class="whole-number">
                        <span class="positive-number">5</span><span>.</span><span class="decimal-part">65</span>
                    </span>
                    <span></span>
                </span>
            </h3>
        </div>
    </div>
</div>

<div class="paybill-container tacnumber-container margintopvoid">
    <div class="tacSMS">
        <div class="sectionContent">
            <div class="row">
                <div class="section-input col-xs-12">
                    <div class="visible-xs visible-sm">
                    </div>

                    <div class="media clearfix">
                        <div class="media-icon">
                            <img src="/mobile-tac.JPG" alt="">
                        </div>

                        <div class="media-content tac-content">

                            <div class="input-field" id='resendTAC' style='display: none'>
                                <span class="hidden-xs hidden-sm">A 6-digit TAC has been sent to your mobile. Please
                                    enter it below to proceed.</span>
                                <span class="resend-tac">
                                    <div>
                                        <span>Click</span>
                                        <a class="link-text link-text-cus" href="javascript:;" tabindex="-1">
                                            <span id="resendTAC">here</span>
                                        </a>
                                        <span>to request again</span>
                                    </div>
                                </span>
                                <br>
                            </div>

                            <div id="sendTAC">
                                <span class="">To proceed, please request and enter your TAC Number.</span>
                                <div class="input-field">
                                    <span class="resend-tac">
                                        <strong></strong><br>
                                        <a class="link-text link-text-cus" href="javascript:;" tabindex="-1">
                                            <span id="requestTAC">Request TAC</span>
                                        </a>
                                    </span><br>
                                </div>
                            </div>


                            <div class="input-field tac-input">
                                <input class="input-tac-number js-tac-sms" id="input-tac-sms" maxlength="6"
                                    name="amount" placeholder="Enter TAC" type="password" tabindex="0">
                                <hr class="bar-bottom-off">
                                <a class="btn-help" data-container="body" data-placement="bottom" data-toggle="tooltip"
                                    data-trigger="hover" href="javascript:;" init-tooltip="" tabindex="-1" title=""
                                    data-original-title="Enter 6-digit number you received via SMS to proceed">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cimb-container">
    <div class="row button-group">
        <div class="col-xs-12 col-sm-6 pull-right text-right btn-submit">
            <a class="btn btn-link hidden-xs hidden-sm"><strong class="text">Cancel</strong></a>
            <button class="btn btn-primary" id='btnConfirm' type="submit" disabled="">Confirm</button>
            <a class="btn btn-link visible-xs visible-sm margin-zero left"><strong class="text">Cancel</strong></a>
        </div>
    </div>
</div>

<script>
    $(function(){
         $("#input-tac-sms").keyup(function(){
             console.log($(this).val().length);
            if($(this).val().length == 6){
                $("#btnConfirm").prop('disabled', false);
            } else {
                $("#btnConfirm").prop('disabled', true);
            }
         });

         $("#requestTAC").click(function(){
            $("#resendTAC").show();
            $("#sendTAC").hide();
         });
    });
</script>

@stop