@extends('cimb_layout')

@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<form class="login-form pass-form ng-untouched ng-pristine ng-valid" id="form-login_step2" name="form-login_step2"
    novalidate="novalidate" style="opacity: 1;">
    <div class="form-group secure-block" id="load-account-id">
        <div class="input-field controls secure">
            <div class="controls secure">
                <img class="secure-img scr-img" id="secure-img"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUcAAACCCAMAAADixfrqAAADAFBMVEUAAAAAADMAAGYAAJkAAMwAAP8AMwAAMzMAM2YAM5kAM8wAM/8AZgAAZjMAZmYAZpkAZswAZv8AmQAAmTMAmWYAmZkAmcwAmf8AzAAAzDMAzGYAzJkAzMwAzP8A/wAA/zMA/2YA/5kA/8wA//8zAAAzADMzAGYzAJkzAMwzAP8zMwAzMzMzM2YzM5kzM8wzM/8zZgAzZjMzZmYzZpkzZswzZv8zmQAzmTMzmWYzmZkzmcwzmf8zzAAzzDMzzGYzzJkzzMwzzP8z/wAz/zMz/2Yz/5kz/8wz//9mAABmADNmAGZmAJlmAMxmAP9mMwBmMzNmM2ZmM5lmM8xmM/9mZgBmZjNmZmZmZplmZsxmZv9mmQBmmTNmmWZmmZlmmcxmmf9mzABmzDNmzGZmzJlmzMxmzP9m/wBm/zNm/2Zm/5lm/8xm//+ZAACZADOZAGaZAJmZAMyZAP+ZMwCZMzOZM2aZM5mZM8yZM/+ZZgCZZjOZZmaZZpmZZsyZZv+ZmQCZmTOZmWaZmZmZmcyZmf+ZzACZzDOZzGaZzJmZzMyZzP+Z/wCZ/zOZ/2aZ/5mZ/8yZ///MAADMADPMAGbMAJnMAMzMAP/MMwDMMzPMM2bMM5nMM8zMM//MZgDMZjPMZmbMZpnMZszMZv/MmQDMmTPMmWbMmZnMmczMmf/MzADMzDPMzGbMzJnMzMzMzP/M/wDM/zPM/2bM/5nM/8zM////AAD/ADP/AGb/AJn/AMz/AP//MwD/MzP/M2b/M5n/M8z/M///ZgD/ZjP/Zmb/Zpn/Zsz/Zv//mQD/mTP/mWb/mZn/mcz/mf//zAD/zDP/zGb/zJn/zMz/zP///wD//zP//2b//5n//8z///8SEhIYGBgeHh4kJCQqKiowMDA2NjY8PDxCQkJISEhOTk5UVFRaWlpgYGBmZmZsbGxycnJ4eHh+fn6EhISKioqQkJCWlpacnJyioqKoqKiurq60tLS6urrAwMDGxsbMzMzS0tLY2Nje3t7k5OTq6urw8PD29vb8/PwgKWLDAAAKGElEQVR42u2d22sb2RnAz1wkjS3Zlm+x48jrfdlAKSWFpdAlm26hW7rduCVs27ALqVNI2ibYgkKhhV5g3xb21YHuS18LW8KShzUt9KEP+1JoS1n6D6xXci1fdPOMpBnNnDk93zm6HI2kpLXWcpN+hsM33+Wc70SMPv3mkw7RGMG/z+BPJwReSRwjDp0wjeAYeeikdYlyJKmTlopyJKm3bkyCciSJ9fEzrI84Rh46oh/yI/Ij8iNK5EfkR+RH5EccyI/Ij8iPWB+R/5AfkR+RH3EgPyI/Ij8iP+JAfkR+RH5EfsSB/Ij8iPyIA/kR+RH5EfkRJfIj8iPyI/IjDuRH5EfkR5TIj8iPyI/Ij0/F324+l9t9EflxZIajWlb/4Hxym6PUBfr+N72AGrzIJoil/Xn9LGtQLjSonn1AdSm5ZcKcNHtiQn2L+586fvSOv3PNCWDjm2HDLZaunCmnhTozstvMaEmNsrr9vYKjxkn/+fCjeVpu+vCqE+aIOfnROqEb005gsLPltLXAqeYIoUaGsEesGTRrjOaCsrNodOKo8J8PP2rsdDcyOfS3tsPUjCn1oOgaW4/O9s0T7MNbOlxr67/7CuW6tmy0Y/K9/vEOdrppB01q0uR8R3ePqJk5463uitqY6ej+cZPr5OP1p5cfq37WYOZs12Zx/awZTdbGrh47NLhOvvT08mNwArUoZSim3NkzGtQ+qua5MgV6s/bU9h8dzmlaOK3a/r6aGT8ffhTCPpz/AXY13j5NPThYD/UwmRziD+pVp2yXv/ZGQGLSduNa2a5w2+23CPHtcnXjrV2pXyuneYl2yl993a4TU2E/Vq+eVOxq049pHVuFc2OoTyu5XgiavGb6KR6Ta61nd/w7K3bJrvJ1jda6u8LP43hOahdPqr4m9sdqxWrVpdbY+fGhDhwXG+xvHhVuuU2N6o+820fH0t7hPj73wKZb26TNgQbR3ML3/Uca9SuFh511nH8du4G2RVwnf/I4PkyIdX6v8mXbX/ti5Q7/EIV1y+p8Hsc0r1AJmXb3uMHt/sGtgH9QnhTH3n98VdSq+GD/oUtz0yuZtQWd165GQdhXV5Kt2lb9MhPy0j+nJO+R2lGzxX3BO05rneNyYOiLF+9Oc/tJ8TF8GBM184/AlytTPf6NMqO/WVy9OMdtrxZF/MW7U7LGat5RIPdTosQ78OS8em0EfjTePsVNbFP+ttbzywP9dvb6vWn+Rotp3k09ZHpCYOpEWc9ev6nHyqGQU9pl6zaXoW4Vs0IKezMueLTocn05rl+23DDU7307Lte2RVy6J18N5r2WgjvCrCn+ylVuX7B4+uDe9ZtUS0B9vWxVRJ7UcSDz6TSuFzc7+SfHzI/7wG30+cH+T0z6nLjZd74AvDe5INPkOW/qYYLM60GBwzK/A/bFs7LVSKYm2M41OwB/coHH1kqded6BTk3t0mB+lDa+rrkS9b/7JrfHliDGqUDeJQ2uISePT9XnJkmBQn4r8BYmyUFAlTxj48f7UIuMIX4Dah5cr4ta9ENp1zhfcl1bjhlxLmUc+H+8uDjBY2f+IvRGwGNPwP4nMc/keRhrDOZHWfP4vM0+/y2wp0SMBev6XntvEH/nQlLTZkV+994yv04LOxs3Pz6IcFyU87QBvCdr0DwkpYbifykple/Ghc4rpNvszjNFzfOG8GO7Zuai/kYIUv7bTGGvqfEv8nc5Scj49rWo1WPmR8Fx2hD/amZtEO9xzuM6vIJsLbMKtlD4f96elxJ6wF9KYf9Jaw2Y5w/vLwr/o6jfFXxryhixLpPXwq7/Cq41mV9eS/u4v782tjjH0Z0nxUE/MGwzoU7522hL9RvC39ENobuEUWGPt9fg85oD1iOKX4/6XdifYSp5XHkt7KGm5NcU+7j5kQgOe22Ynz0sHe3d2Mv18FybH7txbX5s6ZbQub0p7J/mc/kb+ZyoeZtD+VG8iIYR9VNR+/b4/Pxuvr2u+Lhr8WM0v2ofHz/+QtSiYLA/KH76Tt0NHrBe3lNqWw8PRnSmcGJHPhjKj77w/yzqZ73zlTySH/vzq/ax9R8rDiBDKj3I5xY3t0ODpBOx3n7grrHF7ZeU2Bvb4F/t1Y0Mkfiy2r/2gP6iXQn5urOpiD8P+xvUD4W1t7ZX+/Kr9vHxo1cAfkwtDHq2PvA5B+r/WI/yXk7w45oSuyf4sfOySJ4kGVZo9tiH9x8Bt0+ABy8ZEX9OAx68pPWtsSf4ca0vv2ofHz/GTeCtxiDecjaBA+fW+3hP8qMaK2tSR2dCnwBmBPvHT+4/8lEH7rPMqH9C8GDQvz/Jj/35Vfv4+FFLidpSGeCqiZpkDePH/n5iR/OFzvcT7zwzP6n/SDzBiZN9/piQ9UFsq3Cimv9c+JHMEOCtmtfvY5ITh/GjGiv5raN7QuevySfCvvGf9B9t4D5tqs+fEjzY6N+7yolq/vPhR8IWgbdoye8vt4Lngv+eH5kN+nsJwq5YYA+qj+dRGI27wH2L/X79t7A/z+nbt8qJav7z4UemWWmoLfcPG107cY64jIsa5Xb57En8+MsWOBwT0GfAPyfmnTiRvCzKj8Ex6DOJAf4ZE/Ldqkf3LWsiHcKPdPy/f5xJQ00Jj/cdD7bh2cV8uc7tlqhRFf4Z5B5IfvO7fJaTJaiHH+/vVT2NuQUP9IkJUWveF/HlwrsB1M1G8VDlQ7c9v7F/g+u/Tke+v6bia/m5N0AvHTg8I91pVPaJwpV/IPLxFuI/jNrH2H8Ud2S8rkF/sd7YeLni2J6nZ6/PEGbVGPT1HLvihvPfgr6ekeDxvtOQ/b5Et5DI/mPK/9ErfD6D/t/GBekzrRp/k2a/8fl62a7U73zdh54iKwWiT0i1OP8n03rlB69zfe7NTmFo+XUDUDy24W7x9X2v6mxcu+A22AxAGd8F5Pkcf+hk7h2RX147wv5CItTH/P019BdfrgWG4C7BbWRqAlq29JBKPf7Xdbsk/BytlbhMhB+dVz4QdmomZ7trv9QIWvEwL9PmT2UdkPEFo/P7H8UuONB3HKLEr7X5ssWx3fjn5W+HpJ4Z/+8f19MrS6nkTzlLkoQ1V1pNJ8BuXExPZnVturS0TqaWYob4XluXfNbLfy1+S/1tPs5976WXZpW15+0FK57VmWmmkhecDn8q6ySs+eUlo7Mf1S9ssdmVdNKAPmnCmpl/rsOXLY7txrdqt9RPy4/a+f3/M/wuYASecPD8zEgjwo94fuaUI9J/xPMzp5SS3xienxlNtvhtB8/PjCSDE/lMe5XSZ+H8jHlO9WRXpw8oPKM71TP/3eSzfH7GyJqS2bh8Fs7PnCM/4vlrHHj+Gs9fIz/i+WuUeP4az1/j+WvkRxzIj8iPyI8okR+RH5EfkR/xD/kR+RH5EfkRJfIj8iPyIw7kR+RH5EfkR5TIj8iPyI/IjziQH5EfkR9xID8iPyI/Ij/iQH5EfkR+/L8a/wbbS9qtbNgNIwAAAABJRU5ErkJggg==">
            </div>
        </div>
    </div>
    <div class="form-group note-secure">
        <div class=" form-group Login-no-margin-bottom Login-text-center LoginCheckBox ">
            <label class="secure-block-label">Do not proceed if this is not your SecureWord</label>
            <br>
            <input autofocus="" class="checkbox-custom filled-in" id="loginCheckBox" type="checkbox">
            <label class="Login-font-border secure-word-label">Yes, this is my SecureWord</label>
        </div>
    </div>
    <div class="form-group">
        <div class="input-cmp input-cmp-validate">
            <div class="inputarea-box-pass">
                <label class="label">Password</label>
                <input autocapitalize="off" autocomplete="off" autocorrect="off" id="password" maxlength="20"
                    minlength="8" name="password" placeholder="Enter Password" required="" disabled=""
                    style="color: black !important;-webkit-text-security: disc" tabindex="-1" aria-required="true"
                    type="text">
                <hr class="bottom-bar">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-field input-field-btn">
            <a class="arrow-back" href="javascript:;" tabindex="-1"><i class="fas fa-arrow-left"></i></a>

            <button class="btn-primary btn-block btn-login" type="button">
                <span class="btn-text">Login</span>
            </button>
        </div>
    </div>
</form>

<script type='text/javascript'>
    $(function(){
        $(".secure-word-label").click(function(){
            $("#loginCheckBox").prop('checked', true);
            $("#password").prop('disabled', false).focus();
        });
    });
</script>
@stop