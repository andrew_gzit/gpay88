<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @font-face {
            font-family: "Poppins";
            font-weight: 400;
            src: url("{{ ltrim(public_path('assets/fonts/NunitoSans-Regular.ttf'), '/') }}") format("truetype");
        }

        @font-face {
            font-family: "Poppins-SemiBold";
            src: url("{{ ltrim(public_path('assets/fonts/NunitoSans-Bold.ttf'), '/') }}") format("truetype");
        }
    </style>

    <link rel="stylesheet" href="{{ ltrim(public_path('assets/css/invoice.css'), '/') }}">
</head>

<body>

    <section class="section-letterhead">
        <table class='table-letterhead'>
            <tr>
                <td class='td-logo'>
                    <img class='logo' src="{{ ltrim(public_path('assets/images/logo.png'), '/') }}" alt="">
                </td>
                <td>
                    GPAY88 Enterprise (1234-X)<br>
                    6 Jalan Anson<br>
                    10400 Pulau Pinang<br>
                    Malaysia
                </td>
            </tr>
        </table>
    </section>

    <section class='section-header'>
        <table class='table table-header' cellspacing='0'>
            <tbody>
                <tr>
                    <td class='text-large text-bold'>Invoice</td>
                    <td class='text-large text-bold'>RM {{ number_format($invoice->total_amount, 2) }}</td>
                </tr>
                <tr>
                <td>Invoice No.: {{ $invoice->invoice_no }}</td>
                    <td>Due: {{ $invoice->payment_due->format('M d, Y') }}</td>
                </tr>
                <tr>
                    <td>{{ $invoice->issue_date->format('M d, Y') }}</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </section>

    <section class='section-bill'>
        <table class="table" cellspacing='0'>
            <tbody>
                <tr>
                    <td>Billed To</td>
                </tr>
                <tr>
                    <td>
                        Chan Wei Keong<br>
                        +60164375635
                    </td>
                </tr>
            </tbody>
        </table>
    </section>

    <section class='section-content'>
        <table class='table table-shop' cellspacing='0'>
            <thead>
                <tr>
                    <td>Shop</td>
                    <td>Total (RM)</td>
                    <td>Rate (%)</td>
                    <td>Chargable (RM)</td>
                </tr>
            </thead>
            <tbody>
                @foreach($invoice->details AS $det)
                <tr>
                    <td>
                        <div class='text-bold'>{{ $det->shop->shop_name }}</div>
                        Total Transactions: {{ $det->num_transactions }}
                    </td>
                    <td>{{ number_format($det->amount, 2) }}</td>
                    <td>{{ number_format($invoice->rate, 2) }}</td>
                    <td>{{ number_format($det->chargable_amount, 2) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>

    <section class='section-summary'>
        <table class="table">
            <tbody>
                <tr>
                    <td width='50%'></td>
                    <td>
                        <table class="table table-summary">
                            <tbody>
                                <tr>
                                    <td class='text-bold'>Subtotal:</td>
                                    <td>RM {{ $invoice->details->sum('chargable_amount') }}</td>
                                </tr>
                                <tr>
                                    <td class='text-bold'>Total:</td>
                                    <td>RM {{ $invoice->details->sum('chargable_amount') }}</td>
                                </tr>
                                <tr>
                                    <td class='text-bold'>Amount Due:</td>
                                    <td>RM {{ $invoice->details->sum('chargable_amount') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>

</body>

</html>