@extends('mbb_layout')

@section('content')
<div class="common_box white-bg-box">
    <div id="loginMain" class="common_box w-loginmain mt-3-rem">
        <div class="p-login">
            <h3 class="font-size-subhead">Log in to Maybank2u.com online banking</h3>
            <blockquote class="note mx-0 rounded bg-white-05 font-size-dec">
                <strong>Note:</strong><br>
                <ul class="pl-ul my-ul">
                    <li>You are in a secured site.</li>
                </ul>
            </blockquote>

            <h2 class="font-size-subhead text-center">Are these your image and security phrase?</h2>

            <table id="loginTable" class="w-100 content-table">
                <tbody>
                    <tr>
                        <td colspan="3" align="center">
                            <div class="common_box_shadow img_box_wrapper">
                                <div class='img-box'>
                                    <table class='img-table'>
                                        <tbody>
                                            <tr>
                                                <td class="py-0">
                                                    <div class="wrapper img-wrapper">
                                                        <img src="/mbb_logo.gif" class='img-security'
                                                            oncontextmenu="return false;" onmousedown="return false;"
                                                            ondrag="return false;">
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">
                            <b>Phrase : Secret</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">
                            <label id="usernameLabel"><span id="usernameErrorSymbol"></span><b>Username</b></label>:
                            Test Username
                        </td>
                    </tr>
                    <tr>
                        <form name="m1200BillPaymentForm" method="get" action="/mbb_fpx1">
                            <td width="40%" align="right" class="child-form-element child-display-block">
                                <input type="submit" name="action" value="Not me" class="btn-medium">
                            </td>
                            <td width="5%">&nbsp;</td>
                            <td width="40%" align="right" class="child-form-element child-display-block">
                                <input type="submit" name="action" value="Yes">
                            </td>
                        </form>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center font-size-dec">
                            <table id="errorTable" class="font-size-small">
                                <tbody>
                                    <tr>
                                        <td>
                                         
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table>
                <tbody>
                    <tr>
                        <td class="font-size-dec">
                            <p>
                                <span>Don't have a Maybank2U account?</span>
                                <br>
                                <a target="_blank"
                                    href="https://www.maybank2u.com.my/maybank2u/malaysia/en/personal/accounts/accounts_landing.page">Click
                                    here</a> for information on opening an account.
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div id="loginSide">
        <div class="paddingdiv">
            <div class='div-login-side'>
                <h3 class="font-size-base">Security information:</h3>
                <ul class="pl-ul my-ul font-size-dec">
                    <li><a href="http://www.maybank2u.com.my/mbb_info/m2u/public/personalDetail04.do?channelId=Personal&amp;cntTypeId=0&amp;cntKey=OB04.01&amp;programId=OB-OnlineBanking&amp;chCatId=/mbb/Personal"
                            target="_blank">Click here for security tips</a></li>
                    <li><strong>Never</strong> login via email links</li>
                    <li><strong>Never</strong> reveal your PIN and/or Password to anyone.</li>
                </ul>
                <p class="font-size-dec mb-4"><a
                        href="/mbb/m2u/common/mbbPhishingReportForm.do?BV_SessionID=@@@@0533241822.1578275579@@@@&amp;BV_EngineID=ccfcadhklfigjifcefecfledfmodfli.0">Click
                        here to notify us of any Maybank2u.com "phishing" website</a></p>
                <p class="font-size-small text-mineshaft"><strong>Forgot your Online Banking
                        password?</strong><br>Call our customer care hotline at 1-300-88-6688 or
                    603-7844 3696 if you're overseas (24 hours daily, including holidays).</p>
            </div>
        </div>
    </div>
</div>
@stop