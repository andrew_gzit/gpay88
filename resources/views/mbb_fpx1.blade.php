@extends('mbb_layout')

@section('content')
<div class="common_box white-bg-box">

    <div class='text-center'>
        <img src="/mbb_fpx.jpg" class='img-fpx'>

        <div class="text-mineshaft font-size-dec">
            <p id="timeout">Timeout in 03:00</p>
        </div>
    </div>

    <div class="common_box w-loginmain border-0">
        <div class="paddingdiv">

            <div class="text-center mb-3">
                <strong>Step 1 of 3</strong>
            </div>

            <div class="bg-gallery border-light p-2 rounded mb-4 font-size-dec">

                <form name="m9000FpxBuyerForm" method="post"> 
                    <table class="font-size-dec w-100" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td width="40%" valign="top" class="right p-1">
                                    <label id="fromAccountLabel" class=""><span id="fromAccountErrorSymbol"></span>From account: </label>
                                </td>
                                <td width="60%" valign="top" class="p-1">
                                    <strong>1234</strong>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1 right">Merchant Name:</td>
                                <td valign="top" class="p-1"><strong>LAZADA MALAYSIA</strong></td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1 right">Payment Reference:</td>
                                <td valign="top" class="p-1"><strong>163928174</strong></td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1 right">FPX Transaction ID:</td>
                                <td valign="top" class="p-1"><strong>2001061039480858</strong></td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1 right">Amount:</td>
                                <td valign="top" class="p-1"><strong>RM9.50</strong></td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1 right">Fee Amount:</td>
                                <td valign="top" class="p-1"><strong>RM0.00</strong></td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1 right">TAC:</td>
                                <td valign="top" class="p-1">
                                    <input type="password" name="tacVal" maxlength="6" size="8" autocomplete="off"
                                        value="" class="form-control focus-none mb-1 font-size-dec">
                                    <a href="/" target="_self" class="btn btn-primary d-inline-block mb-1">Request for TAC</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="p-1 font-size-dec">
                        <font color="red"><span id="tacValError"></span></font>
                        <div id="tacReqStatusSuccess">Your TAC Request is <font color="green">
                                successful </font> (06 Jan 2020 10:51:25).<br>
                            Your TAC number will be sent to your registered mobile phone number
                            016-437XXXX
                        </div>
                        <div id="tacReqStatusFail" class="error"></div>
                    </div>

                </form>
            </div>

            <p class="text-center">
                <input type="submit" name="action" value="Continue" class="btn btn-primary mr-1-rem">
                <input type="submit" name="action" value="Cancel" onclick="overlayPopUp();"
                    class="button transparent font-size-dec">
            </p>

        </div>
    </div>
</div>
@stop