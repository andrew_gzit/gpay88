<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CIMB Clicks</title>

    <link rel="stylesheet" href="/cimb_common.css">
    <link rel="stylesheet" href="/cimb_main.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>

<body>
    <div class="page-level1-container position-absolute">
        <div class="pre-login-payment no-footer-pre-login page-container">

            <div class="header content js-header hidden-sm hidden-xs">
                <div class="title-head-block">
                    <div class="header-content-wrapper container-fixed-lg">
                        <div class="title-row-1 clearfix">
                            <h3 class="clearfix" style="display: flex;">
                                <div class="col-xs-6 padding-leftright-0 names">
                                </div>
                                <div>
                                    <img class="hidden-sm hidden-xs" src="/cimb.svg" style="height: 100%;" width="150">
                                </div>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header header-mobile visible-block-xs visible-block-sm" style="z-index:999999 !important;">
                <div class="title-head-block">
                    <div class="header-content-wrapper container-fixed-lg">
                        <div class="title-row-1 clearfix">

                            <div class="select-account">
                                <div class="logo">
                                    <span class="cimb">CIMB</span>
                                    <span class="bank">BANK</span>
                                </div>
                            </div>

                            <div class="main-text-shrink text-center">
                                <div class="wrapper-text-shrink">
                                    <div class="text-label main-page-title">
                                        <span class="icon icon--login icon-left"></span>
                                        <strong>CIMB Clicks</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wrapper-container e-payment-page">
                <h3 class="title-header hidden-xs hidden-sm">ePayment</h3>
                <div class="e-payment-container fixed-mobile-scroll clearfix">
                    @yield('content')
                </div>
            </div>

            <footer class="logout-footer hidden-xs hidden-sm">
                <ul class="linked-list list-inline footer-list">
                    <li>
                        <a class="item link-footer" href="#">Terms &amp; Conditions</a>
                    </li>
                    <li>
                        <a class="item link-footer" href="#">Privacy</a>
                    </li>
                    <li>
                        <span class="item">Copyright © 2020 CIMB Bank Berhad (13491-P)</span>
                    </li>
                </ul>
            </footer>
        </div>
    </div>
</body>
</html>