@extends('mbb_layout')

@section('content')
<div class="common_box white-bg-box">
    <div class='text-center'>
        <h3>Welcome</h3>
    </div>
    <div id="loginMain" class="common_box w-loginmain">
        <div class="p-login">
            <h3 class="font-size-subhead" style="z-index: 1;position:relative;">Log in to Maybank2u.com online banking
            </h3>
            <blockquote class="note mx-0 rounded bg-white-05 font-size-dec" style="z-index: 1;position:relative;">
                <strong>Note:</strong><br>
                <ul class="pl-ul my-ul">
                    <li>You are in a secured site.</li>
                </ul>
            </blockquote>

            <form name="fpxForm" method="get" action="/mbb_security_phrase">

                <table id="loginTable" style="margin: 0 auto;z-index: 99;position:relative;" class="w-100">
                    <tbody>
                        <tr>
                            <td class="child-form-element">
                                <label id="username1Label">
                                    <span id="username1ErrorSymbol"></span><b>Username</b></label>:
                                <table class="table-input-group">
                                    <tbody>
                                        <tr>
                                            <td class="input-group-start">
                                                <input type="text" name="username1" maxlength="16"
                                                    onpaste="return false" oncopy="return false" oncut="return false"
                                                    autocomplete="off">
                                            </td>
                                            <td class="input-group-end">
                                                <input type="submit" name="action" value="Next" class="px-4">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <script type="text/javascript">
                    document.fpxForm.username1.focus();
                </script>

            </form>

            <table id="errorTable" class="font-size-small" style="z-index: 1;position:relative;">
                <tbody>
                    <tr>
                        <td>
                            <span id="serverSideError" class="error"></span>
                            <span id="username1Error" class="error"></span>
                            <span id="passwordError" class="error"></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="font-size-dec">
                <p style="z-index: 1;position:relative;">Don't have a Maybank2U account?<br><a
                        href="http://www.maybank2u.com.my/mbb_info/m2u/public/personalChannel01.do?channelId=ACC-Accounts&amp;chCatId=/mbb/Personal/ACC-Accounts"
                        target="_blank">Click here for information on opening an account</a></p>
            </div>

        </div>
    </div>

    <div id="loginSide">
        <div class="paddingdiv">
            <div style="text-align: left;margin-left: auto;margin-right: auto;">
                <h3 class="font-size-base">Security information:</h3>
                <ul class="pl-ul my-ul font-size-dec">
                    <li><a href="http://www.maybank2u.com.my/mbb_info/m2u/public/personalDetail04.do?channelId=Personal&amp;cntTypeId=0&amp;cntKey=OB04.01&amp;programId=OB-OnlineBanking&amp;chCatId=/mbb/Personal"
                            target="_blank">Click here for security tips</a></li>
                    <li><strong>Never</strong> login via email links</li>
                    <li><strong>Never</strong> reveal your PIN and/or Password to anyone.</li>
                </ul>
                <p class="font-size-dec mb-4"><a
                        href="https://www.maybank2u.com.my/mbb/m2u/common/mbbPhishingReportForm.do#1">Click
                        here to notify us of any Maybank2u.com "phishing" website</a></p>
                <p class="font-size-small text-mineshaft"><strong>Forgot your Online Banking
                        password?</strong><br>Call our customer care hotline at 1-300-88-6688 or
                    603-7844 3696 if you're overseas (24 hours daily, including holidays).</p>
            </div>
        </div>
    </div>
</div>
@stop